//--Base
#include "Actor.h"

//--Classes
#include "InventoryItem.h"
#include "InventoryPrototypes.h"
#include "InventorySubClass.h"
#include "PandemoniumRoom.h"
#include "PandemoniumLevel.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "EntityManager.h"

///===================================== Property Queries =========================================
bool Actor::GetInventoryReconstituteFlag()
{
    return mReconstituteInventory;
}
int Actor::GetInventorySize()
{
    return mInventory->GetInventorySize();
}
int Actor::GetItemCount(const char *pItemName)
{
    return mInventory->GetItemCount(pItemName);
}
InventoryItem *Actor::GetItemBySlot(int pSlot)
{
    return mInventory->GetItemL(pSlot);
}

///======================================= Manipulators ===========================================
void Actor::SetReconstituteFlag(bool pValue)
{
    mReconstituteInventory = pValue;
}
void Actor::PickUpItemByName(const char *pItemName)
{
    //--Attempts to pick up the item from the local room's inventory. What to do with it depends on
    //  what kind of item it is, following the usual rules. If the item is not found, then nothing
    //  happens. If there are multiple of the item, the first one found is picked up.
    if(!pItemName || !rCurrentRoom) return;

    //--Get the item by its name.
    InventoryItem *rItemToGet = rCurrentRoom->GetItem(pItemName);

    //--Run PickUpItemByPtr() now. It will do the rest. It will fail if the item didn't exist.
    PickUpItemByPtr(rItemToGet);
}
void Actor::PickUpItemByPtr(InventoryItem *pPtr)
{
    //--Attempts to pick up the item from the local room's inventory, using the pointer directly.
    //  Once picked up, the item follows the usual "Receive" rules. See above.
    if(!pPtr || !rCurrentRoom) return;

    //--De-register the item and re-register it.
    rCurrentRoom->LiberateItem((InventoryItem *)pPtr);
    ReceiveItem((InventoryItem *)pPtr);

    //--Picking up an item counts as a turn.
    Actor::xInstruction = INSTRUCTION_COMPLETETURN;
}
void Actor::RegisterItem(InventoryItem *pItem)
{
    mInventory->RegisterItem(pItem);
    SetReconstituteFlag(true);
}
void Actor::ReceiveItem(InventoryItem *pItem)
{
    //--Given an item, adds it to our inventory. However, the item could be a weapon or armor, in
    //  which case it is automatically equipped. If you want to just pick the item up without attempting
    //  to equip it, use RegisterItem().
    if(!pItem) return;

    //--If the item is a consumable, just register it.
    if(pItem->GetItemType() == ITEM_TYPE_CONSUMABLE)
    {
        //--Put the item in the inventory.
        RegisterItem(pItem);

        //--Print to the console.
        PrintByCode(PRINTCODE_PICKUP_ITEM, true, mLocalName, pItem->GetName(), NULL);
    }
    //--If it's an armor, attempt to equip it.
    else if(pItem->GetItemType() == ITEM_TYPE_ARMOR)
    {
        //--Equip it.
        EquipArmor(pItem);

        //--Don't print here! EquipArmor() will already have printed.
    }
    //--If it's a weapon, attempt to equip it.
    else if(pItem->GetItemType() == ITEM_TYPE_WEAPON)
    {
        //--Equip it.
        EquipWeapon(pItem);

        //--Don't print here! EquipArmor() will already have printed.
    }
}
void Actor::ReceiveClonedItem(const char *pPrototypeName)
{
    //--Receives a new copy of the item, following all the usual receiver rules. The item will be
    //  spawned here, or nothing happens if the name is invalid.
    if(!pPrototypeName) return;

    //--Get a clone. Fail if it doesn't exit.
    InventoryItem *nNewItem = InventoryPrototypes::Fetch(pPrototypeName);
    if(!nNewItem) return;

    //--Receive it.
    ReceiveItem(nNewItem);
}
void Actor::EquipWeapon(InventoryItem *pItem)
{
    //--Unequips the held weapon, if any, then equips the passed one. Takes ownership of that weapon!
    //  Passing NULL will just unequip the current weapon.
    //--If the listed item is the same as the current weapon, just unequip it.
    bool tEndOfCase = (pItem == mWeapon && pItem);
    UnequipWeapon();
    if(tEndOfCase) return;

    //--Eqip it.
    mWeapon = pItem;

    //--Print to the console.
    PrintByCode(PRINTCODE_EQUIP_ITEM, true, mLocalName, pItem->GetName(), NULL);
    SetReconstituteFlag(true);
}
void Actor::EquipArmor(InventoryItem *pItem)
{
    //--Same as EquipWeapon(), but for armor.
    bool tEndOfCase = (pItem == mArmor && pItem);
    UnequipArmor();
    if(tEndOfCase) return;

    //--Eqip it.
    mArmor = pItem;

    //--Print to the console.
    PrintByCode(PRINTCODE_EQUIP_ITEM, true, mLocalName, pItem->GetName(), NULL);
    SetReconstituteFlag(true);
}
void Actor::UnequipWeapon()
{
    //--Unequips the weapon and puts it in the inventory. If flagged to, weapons will be dropped instead
    //  of placed in the inventory.
    if(xAllowOneWeapon)
    {
        DropItem(mWeapon);
    }
    //--Otherwise, put it in the inventory.
    else
    {
        mInventory->RegisterItem(mWeapon);
    }

    if(mWeapon != NULL) SetReconstituteFlag(true);
    mWeapon = NULL;
}
void Actor::UnequipArmor()
{
    //--Same as UnequipWeapon(), but for armor. Doi.
    if(xAllowOneArmor)
    {
        DropItem(mArmor);
    }
    //--Otherwise, put it in the inventory.
    else
    {
        mInventory->RegisterItem(mArmor);
    }

    if(mArmor != NULL) SetReconstituteFlag(true);
    mArmor = NULL;
}
void Actor::DropItem(InventoryItem *pItem)
{
    //--Places the item in the currently active room. Dropping an item with no active room causes
    //  it to despawn immediately!
    //--Note: It is assumed the item, being passed in, was already removed from the inventory.
    if(!pItem) return;

    //--If the item happens to be the Armor or Weapon, remove that. This doesn't count as an
    //  unequip for printing purposes, but does the same thing.
    if(pItem == mWeapon) mWeapon = NULL;
    if(pItem == mArmor)  mArmor = NULL;

    //--No room? Delete the item immediately.
    if(!rCurrentRoom)
    {
        Memory::AddGarbage(pItem, &RootObject::DeleteThis);
        return;
    }

    //--Place the item in the room's inventory.
    rCurrentRoom->RegisterItem(pItem);
    SetReconstituteFlag(true);

    //--Print to the console.
    PrintByCode(PRINTCODE_DROP_ITEM, true, mLocalName, pItem->GetName(), NULL);
}
void Actor::DropItemBySlot(int pSlot)
{
    //--Drops the item based on the slot it occupies in the inventory.
    DropItem(mInventory->LiberateItemL(pSlot));
}
void Actor::DropItemByPtr(void *pPtr)
{
    for(int i = 0; i < mInventory->GetInventorySize(); i ++)
    {
        if(mInventory->GetItemL(i) == pPtr)
        {
            DropItemBySlot(i);
            return;
        }
    }
}

///======================================= Core Methods ===========================================
void Actor::UseItemBySlot(int pSlot)
{
    //--"Use" the item in the corresponding slot. This doubles as equipping it, if it's that kind
    //  of item.

    //--Get the item. If it's NULL, fail silently.
    InventoryItem *rItem = GetItemBySlot(pSlot);
    if(!rItem) return;

    //--Is this an item that can be equipped in the armor slot?
    if(rItem->GetItemType() == ITEM_TYPE_ARMOR)
    {
        EquipArmor(rItem);
    }
    //--Is this an item that can be equipped in the weapon slot?
    else if(rItem->GetItemType() == ITEM_TYPE_WEAPON)
    {
        EquipWeapon(rItem);
    }
    //--Is this a consumable?
    else if(rItem->GetItemType() == ITEM_TYPE_CONSUMABLE)
    {
        //--Use.
        rItem->UseItemOn(this, this);

        //--Is the item marked for consumption? Remove it.
        if(rItem->mIsConsumed)
        {
            mInventory->LiberateItemL(pSlot);
            Memory::AddGarbage(rItem, &RootObject::DeleteThis);
        }
    }
    //--Erroneous type.
    else
    {
        DebugManager::ForcePrint("UseItemBySlot: Error, Item %s has invalid type %i\n", rItem->GetName(), rItem->GetItemType());
    }

    //--Always reconstitute. Quantity may have changed.
    SetReconstituteFlag(true);
}
void Actor::UseItemByName(const char *pName)
{
    //--Finds the item and attempts to use it. This acts like an overload.
    int tSlot = mInventory->GetSlotOfItemS(pName);
    UseItemBySlot(tSlot);
}
void Actor::UseItemByPtr(void *pPtr)
{
    for(int i = 0; i < mInventory->GetInventorySize(); i ++)
    {
        if(mInventory->GetItemL(i) == pPtr)
        {
            UseItemBySlot(i);
            return;
        }
    }
}
void Actor::UseItemBySlotOn(int pSlot, uint32_t pID)
{
    //--Uses the item on the target. This can legally be us.
    InventoryItem *rItem = GetItemBySlot(pSlot);
    if(!rItem || pID == 0) return;

    //--If the ID is us, just use the item normally.
    if(pID == mUniqueID) { UseItemBySlot(pSlot); return; }

    //--Get the target entity.
    Actor *rTargetActor = (Actor *)EntityManager::Fetch()->GetEntityI(pID);
    if(!rTargetActor || rTargetActor->GetType() != POINTER_TYPE_ACTOR) return;

    //--Only consumables can be used on someone else.
    if(rItem->GetItemType() == ITEM_TYPE_CONSUMABLE)
    {
        //--Use.
        rItem->UseItemOn(this, rTargetActor);

        //--Is the item marked for consumption? Remove it.
        if(rItem->mIsConsumed)
        {
            mInventory->LiberateItemL(pSlot);
            Memory::AddGarbage(rItem, &RootObject::DeleteThis);
        }
    }
    //--Erroneous type.
    else
    {
        DebugManager::ForcePrint("UseItemBySlotOn: Error, Item %s has invalid type %i. Only consumables can be used on others.\n", rItem->GetName(), rItem->GetItemType());
    }

    //--Always reconstitute. Quantity may have changed.
    SetReconstituteFlag(true);
}
void Actor::UseItemByNameOn(const char *pName, uint32_t pID)
{
    //--Finds the item and attempts to use it on the target. This acts like an overload.
    int tSlot = mInventory->GetSlotOfItemS(pName);
    UseItemBySlotOn(tSlot, pID);
}
void Actor::DropEverything()
{
    //--Actor dumps their whole inventory on the ground in one action. Does not implicitly cost a
    //  turn, as this usually occurs for reasons beyond the Actor's control.
    SetReconstituteFlag(true);

    //--Drop items.
    while(mInventory->GetInventorySize() > 0)
    {
        //--Get.
        InventoryItem *rLiberatedItem = mInventory->LiberateItemL(0);

        //--No room? Delete it.
        if(!rCurrentRoom)
        {
            Memory::AddGarbage(rLiberatedItem, &RootObject::DeleteThis);
        }
        //--In a room, register it there.
        else
        {
            rCurrentRoom->RegisterItem(rLiberatedItem);
        }
    }

    //--Drop the Armor/Weapon.
    if(mWeapon)
    {
        //--Deletion.
        if(!rCurrentRoom)
        {
            Memory::AddGarbage(mWeapon, &RootObject::DeleteThis);
        }
        //--In a room, register it there.
        else
        {
            rCurrentRoom->RegisterItem(mWeapon);
        }
        mWeapon = NULL;
    }
    if(mArmor)
    {
        //--Deletion.
        if(!rCurrentRoom)
        {
            Memory::AddGarbage(mArmor, &RootObject::DeleteThis);
        }
        //--In a room, register it there.
        else
        {
            rCurrentRoom->RegisterItem(mArmor);
        }
        mArmor = NULL;
    }
}
void Actor::LoseItem(const char *pName)
{
    //--"Loses" one copy of the item. The item will immediately disappear, or have its quantity decreased.
    //  This goes by the name of the prototype.
    if(!pName) return;

    //--If the item name is "WEAPON" then we drop the weapon immediately.
    if(!strcasecmp(pName, "WEAPON"))
    {
        Memory::AddGarbage(mWeapon, &RootObject::DeleteThis);
        mWeapon = NULL;
        return;
    }

    //--Get the item by its name. Do nothing if it's not found.
    InventoryItem *rItem = mInventory->GetItemS(pName);
    if(!rItem) return;

    //--Liberate and delete.
    mInventory->LiberateItemP(rItem);
    Memory::AddGarbage(rItem, &RootObject::DeleteThis);

    //--Always reconstitute.
    SetReconstituteFlag(true);
}
void Actor::SwapWeaponsWith(uint32_t pID)
{
    //--Finds the requested Actor by the ID, and switches our weapon with theirs.
    Actor *rTargetActor = (Actor *)EntityManager::Fetch()->GetEntityI(pID);
    if(!rTargetActor || rTargetActor->GetType() != POINTER_TYPE_ACTOR) return;

    //--Swap. We don't need access permissions since we're both actors!
    InventoryItem *rTempPtr = mWeapon;
    mWeapon = rTargetActor->mWeapon;
    rTargetActor->mWeapon = rTempPtr;
}
