-- |[ =================================== fnIssueLoadReset() =================================== ]|
--Issue a load reset, using the provided name to determine the load counter. Load resets tell the 
-- game how many assets were loaded, and are used for progress bars.
function fnIssueLoadReset(psName)
	
	-- |[Argument Check]|
	if(psName == nil) then return end
	
    -- |[Debug]|
	--Warning if a load sequence is already underway.
	if(gsLoadSequence ~= "None") then
		io.write("Warning: A load reset " .. psName .. " was issued while " .. gsLoadSequence .. " was already in progress.\n")
	end
	
    -- |[Execution]|
	--Issue.
	local iExpectedLoads = OM_GetOption("ExpectedLoad_" .. psName)
	LI_Reset(iExpectedLoads)
	
	--Store.
	gsLoadSequence = psName
	
end