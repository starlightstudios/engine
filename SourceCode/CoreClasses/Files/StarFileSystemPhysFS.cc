//--Base
#include "StarFileSystem.h"

//--Classes
//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

///===================================== PhysFS-Only Code =========================================
#if defined _PHYSFS_FILESYSTEM_

//--Special Includes
#include "physfs.h"

//--Function
StarLinkedList *StarFileSystem::SubScanDirectory(const char *pPath, bool pScanDirectories)
{
    ///--[Documentation]
    //--Scanning subroutine. The linked-list passed back will contain FileInfo's for every single entry
    //  within the folder. If pScanDirectories is true, this will recursively be called for each
    //  sub-directory within.
    //--This is the PhysFS version. Because PhysFS uses global functions, make sure to clean up the
    //  paths before exiting.
    //--Check for NULL on failure.
    if(!pPath) return NULL;

    ///--[Common Code]
    //--Create the file info list. It does not delete its members.
    StarLinkedList *nFileInfoList = new StarLinkedList(false);

    //--0th element is always the "../" pathname.  It means "up one directory".
    //  If a flag is set, we don't create this.
    if(!mIgnoreZerothEntry && !mIsThisARecursion)
    {
        char tBuffer[STD_PATH_LEN];
        strcpy(tBuffer, pPath);
        strcat(tBuffer, "..");

        SetMemoryData(__FILE__, __LINE__);
        FileInfo *nEntry = (FileInfo *)starmemoryalloc(sizeof(FileInfo));
        nEntry->Initialize();
        nEntry->mIsDirectory = true;
        nEntry->mIsFile = false;
        nEntry->mPath = NULL;
        ResetString(nEntry->mPath, tBuffer);
        nFileInfoList->AddElement("X", nEntry, &DontDeleteThis);
    }

    ///--[Setup]
    //--Structures.
    PHYSFS_Stat tStatStruct;

    //--Store the absolute path of the directory. Ignore this if this is a recursion.
    if(!mIsThisARecursion)
    {
        //--Temporary buffer.
        char tBuffer[512];
        strcpy(tBuffer, pPath);

        //--Remove '\' and replace with '/'
        for(unsigned int i = 0; i < strlen(tBuffer); i ++)
        {
            if(tBuffer[i] == '\\') tBuffer[i] = '/';
        }

        //--This string needs to have a '/' added onto the end since it's guaranteed to be a
        //  directory.  Add that.
        strcat(tBuffer, "/");

        //--Save the final copy.
        ResetString(mLastParsedDirectory, tBuffer);
    }

    ///--[Iteration]
    //--Mount the path so it appears in the search paths.
    PHYSFS_mount(pPath, "/", 1);
    char **tFileList = PHYSFS_enumerateFiles("");

    //--Read all the entries, and store the filenames.
    char **rFileIterator = tFileList;
    while(*rFileIterator)
    {
        ///--[Directory Check]
        //--Check flags.
        PHYSFS_stat(*rFileIterator, &tStatStruct);
        //fprintf(stderr, "%s is dir: %i\n", *rFileIterator, (tStatStruct.filetype == PHYSFS_FILETYPE_DIRECTORY));

        //--If this is a directory, and we want to recurse, do that now.
        if(tStatStruct.filetype == PHYSFS_FILETYPE_DIRECTORY && pScanDirectories)
        {
            //--Unmount the path to prevent clogging. PhysFS calls are global so the recursion
            //  can't leave them on the global path.
            PHYSFS_unmount(pPath);

            //--Set this flag to false, otherwise we will have duplicate ../ entries.
            mIsThisARecursion = true;

            //--Build it.
            char *tBuf = InitializeString("%s/%s", pPath, *rFileIterator);
            StarLinkedList *tSubscanList = SubScanDirectory(tBuf, pScanDirectories);
            free(tBuf);

            //--Copy all the entries down onto our list.
            FileInfo *rScanInfo = (FileInfo *)tSubscanList->PushIterator();
            while(rScanInfo)
            {
                //--Name
                char *tName = tSubscanList->GetIteratorName();
                nFileInfoList->AddElement(tName, rScanInfo, &DontDeleteThis);

                rScanInfo = (FileInfo *)tSubscanList->AutoIterate();
            }

            //--Re-mount the path, otherwise the file info will not resolve correctly.
            PHYSFS_mount(pPath, "/", 1);

            //--Clean
            delete tSubscanList;
            mIsThisARecursion = false;
        }

        ///--[Store Info]
        //--Is this a directory, when we are ignoring directories? If so, don't create a heap
        //  copy, and don't store it.
        if(tStatStruct.filetype == PHYSFS_FILETYPE_DIRECTORY && mIgnoreFolders)
        {

        }
        //--Normal behavior.
        else
        {
            //--Buffer.
            char tBuffer[512];
            strcpy(tBuffer, pPath);
            strcat(tBuffer, "/");
            strcat(tBuffer, *rFileIterator);

            //--Remove '\' and replace with '/'
            for(unsigned int i = 0; i < strlen(tBuffer); i ++)
            {
                if(tBuffer[i] == '\\') tBuffer[i] = '/';
            }

            //--Tack on a '/' on the end, to indicate this is a directory.
            if(tStatStruct.filetype == PHYSFS_FILETYPE_DIRECTORY) strcat(tBuffer, "/");

            //--Create a heap copy.
            SetMemoryData(__FILE__, __LINE__);
            FileInfo *nEntry = (FileInfo *)starmemoryalloc(sizeof(FileInfo));
            nEntry->Initialize();
            nEntry->mIsDirectory = false;
            nEntry->mIsFile = false;
            nEntry->mPath = NULL;
            nEntry->mLastAccessed = tStatStruct.accesstime;
            ResetString(nEntry->mPath, tBuffer);

            //--Store it.
            nFileInfoList->AddElement("X", nEntry, &DontDeleteThis);
            if(tStatStruct.filetype == PHYSFS_FILETYPE_REGULAR)   nEntry->mIsFile = true;
            if(tStatStruct.filetype == PHYSFS_FILETYPE_DIRECTORY) nEntry->mIsDirectory = true;
        }

        //--Iterate up.
        rFileIterator ++;
    }

    ///--[Finish Up]
    //--Clean up.
    PHYSFS_freeList(tFileList);
    PHYSFS_unmount(pPath);

    //--Pass back the results.
    return nFileInfoList;
}

#endif
