//--Base
#include "MonoUIVendor.h"
#include "AdvUIVendorStructures.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "AdvUIVendorStructures.h"
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================= Common Render ==========================================
//--Contains functions that are used to render in multiple different modes.

///======================================== Standalones ===========================================
void MonoUIVendor::RenderModeTabs(float pColorAlpha)
{
    ///--[Documentation]
    //--At the top of the selection area is a list of Buy/Sell/Buyback etc based on what modes are
    //  available in this shop. This function renders the current tab based on what mode the UI
    //  is currently in.

    //--Constants.
    float cXBuy     = 398.0f;
    float cXSell    = 528.0f;
    float cXBuyback = 686.0f;
    float cYCommon  = 103.0f;

    ///--[Special Case]
    //--Special: In buy mode, if other modes are disabled, then only draw the buy tab.
    if(mIsBuyMode && mIsBuyOnly)
    {
        //--Render buy tab.
        MonoImages.rTab_BuyActive->Draw();
        MonoImages.rFrame_Listing->Draw();

        //--Render text.
        SetColor("Title", pColorAlpha);
        AdvImages.rFont_Heading->DrawText(cXBuy, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextBuy);
        SetColor("Clear", pColorAlpha);
        return;
    }

    ///--[Tabs]
    //--Normal:
    if(!mIsBuyOnly)
    {
        //--Normal mode. Render active tabs.
        if(mIsBuyMode)     MonoImages.rTab_BuyActive->Draw();
        if(mIsSellMode)    MonoImages.rTab_SellActive->Draw();
        if(mIsBuybackMode) MonoImages.rTab_BuybackActive->Draw();

        //--Render inactive tabs.
        if(!mIsBuyMode)     MonoImages.rTab_Buy->Draw();
        if(!mIsSellMode)    MonoImages.rTab_Sell->Draw();
        if(!mIsBuybackMode) MonoImages.rTab_Buyback->Draw();
    }
    //--Buy only:
    else
    {
        if(mIsBuyMode)  MonoImages.rTab_BuyActive->Draw();
        if(!mIsBuyMode) MonoImages.rTab_Buy->Draw();
    }

    //--Backing.
    MonoImages.rFrame_Listing->Draw();

    ///--[Text]
    //--Render active text on the title color.
    if(!mIsBuyOnly)
    {
        SetColor("Title", pColorAlpha);
        if(mIsBuyMode)     AdvImages.rFont_Heading->DrawText(cXBuy,     cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextBuy);
        if(mIsSellMode)    AdvImages.rFont_Heading->DrawText(cXSell,    cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSell);
        if(mIsBuybackMode) AdvImages.rFont_Heading->DrawText(cXBuyback, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextBuyback);

        //--Inactive text on the greyed color.
        SetColor("Greyed", pColorAlpha);
        if(!mIsBuyMode)     AdvImages.rFont_Heading->DrawText(cXBuy,     cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextBuy);
        if(!mIsSellMode)    AdvImages.rFont_Heading->DrawText(cXSell,    cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextSell);
        if(!mIsBuybackMode) AdvImages.rFont_Heading->DrawText(cXBuyback, cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextBuyback);
    }
    //--Buy only:
    else
    {
        if(mIsBuyMode)  AdvImages.rFont_Heading->DrawText(cXBuy,     cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextBuy);
        if(!mIsBuyMode) AdvImages.rFont_Heading->DrawText(cXBuy,     cYCommon, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextBuy);
    }

    //--Clean.
    SetColor("Clear", pColorAlpha);
}
void MonoUIVendor::RenderModeTitles(float pColorAlpha)
{
    ///--[Documentation]
    //--At the top of most vendor frames is a set of titles. Name/Price/etc. This changes slightly
    //  between modes.

    ///--[Constants]
    float cXName = 330.0f;
    float cXPrice = 655.0f;
    float cXOwned = 785.0f;
    float cXSlots = 952.0f;
    float cYCommon = 190.0f;

    ///--[Render Titles]
    SetColor("Title", pColorAlpha);
    AdvImages.rFont_Heading->DrawText(cXName, cYCommon, 0, 1.0f,  xrStringData->str.mTextName);
    AdvImages.rFont_Heading->DrawText(cXPrice, cYCommon, 0, 1.0f, xrStringData->str.mTextPrice);
    AdvImages.rFont_Heading->DrawText(cXSlots, cYCommon, 0, 1.0f, xrStringData->str.mTextSlots);

    //--The "Owned" title does not appear in sell mode.
    if(!mIsSellMode) AdvImages.rFont_Heading->DrawText(cXOwned, cYCommon, 0, 1.0f, xrStringData->str.mTextOwned);

    //--Clean.
    SetColor("Clear", pColorAlpha);
}
void MonoUIVendor::RenderCommonLft(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    AutoPieceOpen(DIR_LEFT, pVisAlpha);

    ///--[Render]
    //--Comparison characters.
    RenderComparisonCharacters(mCompareCharacterOld, mCompareCharacterCur, mCompareSwapTimer, cxAdvCharSwapTicks, ACE_UI_INDEX_VENDOR);

    //--Swap arrows. Only renders if more than one comparison character exists.
    if(mShowSwapArrows)
    {
        //--Arrow offset.
        float cArrowOscillate = sinf((mArrowTimer / (float)cxAdvOscTicks) * 3.1415926f * 2.0f) * cxAdvOscDist;

        //--Render.
        MonoImages.rOverlay_ArrowCharRgt->Draw(cArrowOscillate *  1.0f, 0.0f);
        MonoImages.rOverlay_ArrowCharLft->Draw(cArrowOscillate * -1.0f, 0.0f);
    }

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderCommonTop(float pVisAlpha)
{
    ///--[Position]
    //--Get render positions and color alpha.
    float cColorAlpha = AutoPieceOpen(DIR_LEFT, pVisAlpha);

    ///--[Render]
    //--Header.
    MonoImages.rOverlay_TitleShade->Draw();

    //--Header Text.
    SetColor("Heading", cColorAlpha);
    AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 16.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mShopName);
    SetColor("Clear", cColorAlpha);

    ///--[Clean]
    //--Undo the translation/color mixing.
    AutoPieceClose();
}
void MonoUIVendor::RenderCommonPlatina(float pColorAlpha)
{
    ///--[Documentation]
    //--On several UI parts the platina display appears in the top right corner. This routine handles
    //  rendering that. The rest of the side's routine is usually a comparison pane which changes by mode.
    //--The Monoceros version changes the positions and colors slightly, and uses a translatable string.
    AdvImages.rFrame_Platina->Draw();

    //--Title.
    SetColor("Heading", pColorAlpha);
    AdvImages.rFont_Heading->DrawText(1029.0f, 20.0f, 0, 1.0f, xrStringData->str.mTextPlatina);
    SetColor("Clear", pColorAlpha);

    //--Actual value:
    SetColor("Paragraph", pColorAlpha);
    AdvImages.rFont_Heading->DrawTextArgs(1343.0f, 20.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", AdventureInventory::Fetch()->GetPlatina());
    SetColor("Clear", pColorAlpha);
}

///======================================= Details Mode ===========================================
void MonoUIVendor::RenderItemDetails(int pTimer, int pTimerMax, AdventureItem *pItem)
{
    ///--[Documentation]
    //--Standard description handler. Descriptions appear at the bottom of the screen using a specific
    //  image and render an item's advanced description lines.
    //--It is legal for NULL to be passed as the item. This will render a blank box.
    if(pTimer < 1 || pTimerMax < 1) return;

    //--Percentage.
    float cScrollPercent = EasingFunction::QuadraticInOut(pTimer, pTimerMax);

    //--Offsets.
    float cBotOffset = VIRTUAL_CANVAS_Y * (1.0f - cScrollPercent);

    ///--[Darkening]
    //--Darken everything behind this UI.
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cScrollPercent * 0.7f));

    ///--[Description Frame and Header]
    //--Slides in from the bottom of the screen.
    glTranslatef(0.0f, cBotOffset * 1.0f, 0.0f);

    //--Frame.
    AdvImages.rFrame_Description->Draw();

    ///--[Blank Box Handler]
    //--If the item is NULL, stop.
    if(!pItem)
    {
        glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);
        return;
    }

    //--Header, name.
    MonoImages.rOverlay_DescriptionName->Draw();
    AdvImages.rFont_Heading->DrawText(195.0f, 496.0f, 0, 1.0f, pItem->GetDisplayName());

    ///--[Text Lines]
    //--Setup.
    float cXPosition = 129.0f;
    float cYPosition = 563.0f;
    float cYHeight   =  22.0f;

    //--Render advanced description lines.
    for(int i = 0; i < ADITEM_DESCRIPTION_MAX; i ++)
    {
        //--Retrieve.
        StarlightString *rDescriptionLine = pItem->GetAdvancedDescription(i);
        if(!rDescriptionLine) continue;

        //--Render.
        rDescriptionLine->DrawText(cXPosition, cYPosition + (cYHeight * (float)i), SUGARFONT_NOCOLOR, 1.0f, MonoImages.rFont_Description);
    }

    ///--[Clean]
    StarlightColor::ClearMixer();

    ///--[Finish Up]
    //--Clean.
    glTranslatef(0.0f, cBotOffset * -1.0f, 0.0f);
}

///====================================== Index Rendering =========================================
void MonoUIVendor::RenderItemPack(ShopInventoryPack *pPack, int pSlot, bool pIsOdd, float pAlpha)
{
    ///--[Documentation]
    //--For buy/sell/buyback, renders the information of an item in a standard format. This shows its names,
    //  value, how many the player owns, and gems if any.
    if(!pPack || !pPack->mItem) return;

    //--X positions.
    float cIconX  = 356.0f;
    float cNameX  = 381.0f;
    float cCostX  = 658.0f;
    float cOwnedX = 789.0f;
    float cGemX   = 905.0f;
    float cGemW   =  24.0f;

    //--Ownership.
    float cPortraitX = 814.0f;
    float cPortraitH =  26.0f;

    //--Compute Y position.
    float cSlotY = 240.0f;
    float cSlotH =  56.0f;
    float cYPos = cSlotY + (cSlotH * pSlot);

    //--Y Offsets.
    float cTxtY      = cYPos + 2.0f;
    float cGemY      = cYPos + 11.0f;
    float cPortraitY = cYPos + 10.0f;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Icon.
    StarBitmap *rIcon = pPack->mItem->GetIconImage();
    if(rIcon) rIcon->Draw(cIconX, cTxtY + 12.0);

    //--Name, no quantity.
    SetColor("Paragraph", pAlpha);
    if(!pPack->mItem->IsStackable() || pPack->mQuantity < 2)
        AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cTxtY, cxAdvMaxNameWid, 0, pPack->mItem->GetDisplayName());
    else
        AdvImages.rFont_Mainline->DrawTextFixedArgs(cNameX, cTxtY, cxAdvMaxNameWid, 0, "%sx%i", pPack->mItem->GetDisplayName(), pPack->mQuantity);

    //--Cost. If zero, does not print.
    int tCost = pPack->mPriceOverride;
    if(tCost == -1) tCost = pPack->mItem->GetValue();
    if(tCost > 0)
    {
        AdvImages.rFont_Mainline->DrawTextArgs(cCostX, cTxtY, 0, 1.0f, "%i", tCost);
    }

    //--How many the player owns. Refers to the stack size. Duplicates that are unique, like gems, don't show this.
    if(pPack->mOwnedDisplay > -1)
    {
        //--How many are owned. Must fit a specific area.
        SetColor("Paragraph", pAlpha);
        AdvImages.rFont_Mainline->DrawTextFixedArgs(cOwnedX, cTxtY, 21.0f, 0, "%i", pPack->mOwnedDisplay);
        SetColor("Clear", pAlpha);

        //--Who owns it.
        float tXPos = cPortraitX;
        for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
        {
            //--If the character doesn't own it, skip.
            if(pPack->mOwnedBy[i] == false) continue;

            //--Get the matching character.
            AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(i);
            if(!rCharacter) continue;

            //--Get their face icon.
            TwoDimensionReal tFaceDim;
            StarBitmap *rFaceImg = rCharacter->GetFaceProperties(tFaceDim);
            if(!rFaceImg) continue;

            //--Swap vertically to handle OpenGL.
            tFaceDim.mTop = 1.0f - (tFaceDim.mTop);
            tFaceDim.mBot = 1.0f - (tFaceDim.mBot);

            //--Render it.
            rFaceImg->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(tFaceDim.mLft, tFaceDim.mTop); glVertex2f(tXPos,              cPortraitY);
                glTexCoord2f(tFaceDim.mRgt, tFaceDim.mTop); glVertex2f(tXPos + cPortraitH, cPortraitY);
                glTexCoord2f(tFaceDim.mRgt, tFaceDim.mBot); glVertex2f(tXPos + cPortraitH, cPortraitY + cPortraitH);
                glTexCoord2f(tFaceDim.mLft, tFaceDim.mBot); glVertex2f(tXPos,              cPortraitY + cPortraitH);
            glEnd();

            //--Add to the position.
            tXPos = tXPos + cPortraitH;
        }
    }

    //--Render the gem slots.
    for(int i = 0; i < pPack->mItem->GetGemSlots(); i ++)
    {
        //--If there's a gem in the slot, render it under the frame.
        AdventureItem *rGemInSlot = pPack->mItem->GetGemInSlot(i);
        if(rGemInSlot)
        {
            StarBitmap *rItemIcon = rGemInSlot->GetIconImage();
            if(rItemIcon) rItemIcon->Draw(cGemX + (cGemW * i), cGemY);
        }

        //--Frame.
        AdvImages.rOverlay_GemSlot->Draw(cGemX + (cGemW * i), cGemY);
    }
}
void MonoUIVendor::RenderItem(AdventureItem *pItem, int pSlot, bool pIsOdd, bool pIsSellPrice, float pAlpha)
{
    ///--[Documentation]
    //--Same as above, but takes the item itself instead of an item pack. This precludes rendering
    //  how many the player owns, as this is used for the selling interface and each item is sold individually.
    if(!pItem) return;

    //--X positions.
    float cIconX  = 356.0f;
    float cNameX  = 381.0f;
    float cCostX  = 658.0f;
    float cGemX   = 905.0f;
    float cGemW   =  24.0f;

    //--Compute Y position.
    float cSlotY = 240.0f;
    float cSlotH =  56.0f;
    float cYPos = cSlotY + (cSlotH * pSlot);

    //--Y Offsets.
    float cTxtY      = cYPos + 2.0f;
    float cGemY      = cYPos + 11.0f;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Icon.
    StarBitmap *rIcon = pItem->GetIconImage();
    if(rIcon) rIcon->Draw(cIconX, cTxtY + 12.0);

    //--Name + Quantity.
    int tQuantity = pItem->GetStackSize();
    SetColor("Paragraph", pAlpha);
    if(pItem->IsStackable() && tQuantity > 1)
    {
        AdvImages.rFont_Mainline->DrawTextFixedArgs(cNameX, cTxtY, cxAdvMaxNameWid, 0, "%sx%i", pItem->GetDisplayName(), tQuantity);
    }
    //--Don't render a quantity.
    else
    {
        AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cTxtY, cxAdvMaxNameWid, 0, pItem->GetDisplayName());
    }

    //--Cost. If zero, does not print.
    int tCost = pItem->GetValue();
    if(pIsSellPrice) tCost = tCost / 2;
    if(tCost > 0)
    {
        AdvImages.rFont_Mainline->DrawTextArgs(cCostX, cTxtY, 0, 1.0f, "%i", tCost);
    }

    //--Clean.
    SetColor("Clear", pAlpha);

    //--Render the gem slots.
    for(int i = 0; i < pItem->GetGemSlots(); i ++)
    {
        //--If there's a gem in the slot, render it under the frame.
        AdventureItem *rGemInSlot = pItem->GetGemInSlot(i);
        if(rGemInSlot)
        {
            StarBitmap *rItemIcon = rGemInSlot->GetIconImage();
            if(rItemIcon) rItemIcon->Draw(cGemX + (cGemW * i), cGemY);
        }

        //--Frame.
        AdvImages.rOverlay_GemSlot->Draw(cGemX + (cGemW * i), cGemY);
    }
}

///--[Gems]
void MonoUIVendor::RenderGem(AdventureItem *pItem, const char *pOwner, int pSlot, bool pIsOdd, float pAlpha)
{
    ///--[Documentation and Setup]
    //--Given a gem, renders its information. This is just an icon, owner icon if applicable, and name.
    //  Similar to the base call, but updated for the new layout/fonts.
    if(!pItem) return;

    //--X positions.
    float cIconX     = 156.0f;
    float cPortraitX = 181.0f;
    float cNameX     = 206.0f;
    float cTierX     = 493.0f;
    //float cPlatinaR  = 637.0f;

    //--Sizes.
    float cPortraitH =  26.0f;

    //--Compute Y position.
    float cSlotY = 240.0f;
    float cSlotH =  43.0f;
    float cYPos = cSlotY + (cSlotH * pSlot);

    //--Y Offsets.
    float cTxtY      = cYPos +  3.0f;
    float cIconY     = cYPos + 11.0f;
    float cPortraitY = cYPos + 10.0f;

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    StarlightColor cGemDarken;
    StarlightColor cGemIncompatible;
    cGemDarken.SetRGBAF(0.20f, 0.20f, 0.20f, pAlpha);
    cGemIncompatible.SetRGBAF(1.0f, 0.0f, 0.0f, pAlpha);

    ///--[Color Check]
    //--Get color flag.
    int tTotalTier = -1;
    uint8_t tCurCol = 0x01;
    uint8_t tColorFlag = pItem->GetGemColors();

    //--Position constants.
    float cGemX = 668.0f;
    float cGemY = cTxtY + 10.0f;
    float cGemW =  41.0f;

    //--Loop.
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        //--If this color is present:
        if(tColorFlag & tCurCol)
        {
            tTotalTier ++;
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        }
        else
        {
            cGemDarken.SetAsMixer();
        }

        //--Position. Render at 1.5x scale.
        AdvImages.rGemIcons[i]->Draw(cGemX + (cGemW * i), cGemY);

        //--Next.
        tCurCol = tCurCol* 2;
    }
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    ///--[Rendering]
    //--Icon.
    StarBitmap *rItemIcon = pItem->GetIconImage();
    if(rItemIcon)
    {
        rItemIcon->Draw(cIconX, cIconY);
    }

    //--Owner. If the string is NULL or "Null", nobody owns it.
    if(pOwner && strcasecmp(pOwner, "Null"))
    {
        //--Get the matching character.
        AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberS(pOwner);
        if(!rCharacter) return;

        //--Get their face icon.
        TwoDimensionReal tFaceDim;
        StarBitmap *rFaceImg = rCharacter->GetFaceProperties(tFaceDim);
        if(!rFaceImg) return;

        //--Swap vertically to handle OpenGL.
        tFaceDim.mTop = 1.0f - (tFaceDim.mTop);
        tFaceDim.mBot = 1.0f - (tFaceDim.mBot);

        //--Render it.
        float tXPos = cPortraitX;
        rFaceImg->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(tFaceDim.mLft, tFaceDim.mTop); glVertex2f(tXPos,              cPortraitY);
            glTexCoord2f(tFaceDim.mRgt, tFaceDim.mTop); glVertex2f(tXPos + cPortraitH, cPortraitY);
            glTexCoord2f(tFaceDim.mRgt, tFaceDim.mBot); glVertex2f(tXPos + cPortraitH, cPortraitY + cPortraitH);
            glTexCoord2f(tFaceDim.mLft, tFaceDim.mBot); glVertex2f(tXPos,              cPortraitY + cPortraitH);
        glEnd();
    }

    //--Name.
    AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cTxtY, cxAdvMaxNameWid, 0, pItem->GetDisplayName());

    //--Tier.
    char *tRenderString = Subdivide::ReplaceTagsWithStrings(xrStringData->str.mTextTierGem, 1, "[iTier]", tTotalTier);
    AdvImages.rFont_Mainline->DrawTextArgs(cTierX, cTxtY, 0, 1.0f, tRenderString);
    free(tRenderString);

    ///--[Clean]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
}
void MonoUIVendor::RenderGemMerge(AdventureItem *pItem, const char *pOwner, int pSlot, bool pIsOdd, float pAlpha)
{
    ///--[Documentation and Setup
    //--Given a gem, renders its information. This version is used in the gem merge UI. It shows some additional
    //  information, like the gem's sub-components and tier.
    if(!pItem || !mMergeGem) return;

    //--X positions.
    float cIconX     = 156.0f;
    float cPortraitX = 181.0f;
    float cNameX     = 206.0f;
    float cTierX     = 493.0f;
    //float cPlatinaR  = 637.0f;

    //--Sizes.
    float cPortraitH =  26.0f;

    //--Compute Y position.
    float cSlotY = 240.0f;
    float cSlotH =  43.0f;
    float cYPos = cSlotY + (cSlotH * pSlot);

    //--Y Offsets.
    float cTxtY      = cYPos +  3.0f;
    float cIconY     = cYPos + 11.0f;
    float cPortraitY = cYPos + 10.0f;

    //--Colors.
    StarlightColor cGemDarken;
    cGemDarken.SetRGBAF(0.20f, 0.20f, 0.20f, pAlpha);
    StarlightColor cGemIncompatible;
    cGemIncompatible.SetRGBAF(1.0f, 0.0f, 0.0f, pAlpha);

    //--Mixer
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Flag, stores whether the gems can be merged.
    bool tCanBeMerged = mMergeGem->CanBeMerged(pItem);

    ///--[Rendering]
    //--Icon.
    StarBitmap *rItemIcon = pItem->GetIconImage();
    if(rItemIcon)
    {
        rItemIcon->Draw(cIconX, cIconY);
    }

    //--Owner. If the string is NULL or "Null", nobody owns it.
    if(pOwner && strcasecmp(pOwner, "Null"))
    {
        //--Get the matching character.
        AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberS(pOwner);
        if(!rCharacter) return;

        //--Get their face icon.
        TwoDimensionReal tFaceDim;
        StarBitmap *rFaceImg = rCharacter->GetFaceProperties(tFaceDim);
        if(!rFaceImg) return;

        //--Swap vertically to handle OpenGL.
        tFaceDim.mTop = 1.0f - (tFaceDim.mTop);
        tFaceDim.mBot = 1.0f - (tFaceDim.mBot);

        //--Render it.
        float tXPos = cPortraitX;
        rFaceImg->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(tFaceDim.mLft, tFaceDim.mTop); glVertex2f(tXPos,              cPortraitY);
            glTexCoord2f(tFaceDim.mRgt, tFaceDim.mTop); glVertex2f(tXPos + cPortraitH, cPortraitY);
            glTexCoord2f(tFaceDim.mRgt, tFaceDim.mBot); glVertex2f(tXPos + cPortraitH, cPortraitY + cPortraitH);
            glTexCoord2f(tFaceDim.mLft, tFaceDim.mBot); glVertex2f(tXPos,              cPortraitY + cPortraitH);
        glEnd();
    }

    //--Name.
    if(!tCanBeMerged) cGemIncompatible.SetAsMixer();
    AdvImages.rFont_Mainline->DrawTextFixed(cNameX, cTxtY, cxAdvMaxNameWid, 0, pItem->GetDisplayName());
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Get color flag.
    int tTotalTier = -1;
    uint8_t tCurCol = 0x01;
    uint8_t tColorFlag = pItem->GetGemColors();

    //--Position constants.
    float cGemX = 668.0f;
    float cGemY = cTxtY + 10.0f;
    float cGemW =  41.0f;

    //--Loop.
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        //--If this color is present:
        if(tColorFlag & tCurCol)
        {
            tTotalTier ++;
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
        }
        else
        {
            cGemDarken.SetAsMixer();
        }

        //--Position. Render at 1.5x scale.
        AdvImages.rGemIcons[i]->Draw(cGemX + (cGemW * i), cGemY);

        //--Next.
        tCurCol = tCurCol* 2;
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Gem Tier. Affects costs.
    if(tCanBeMerged)
    {
        char *tRenderString = Subdivide::ReplaceTagsWithStrings(xrStringData->str.mTextTierGem, 1, "[iTier]", tTotalTier);
        AdvImages.rFont_Mainline->DrawTextArgs(cTierX, cTxtY, 0, 1.0f, tRenderString);
        free(tRenderString);
    }
    else
    {
        cGemIncompatible.SetAsMixer();
        AdvImages.rFont_Mainline->DrawText(cTierX, cTxtY, 0, 1.0f, xrStringData->str.mTextIncompatible);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }

    //--Platina cost to merge.
    tTotalTier = tTotalTier + mMergeGem->GetRank();
    if(tCanBeMerged)
    {
        //--Text.
        float cPlatinaR = 637.0f;
        int tMergeCost = AdventureInventory::ComputeMonocerosGemUpgradePlatinaCost(tTotalTier);
        AdvImages.rFont_Mainline->DrawTextArgs(cPlatinaR, cTxtY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tMergeCost);

        //--Platina Symbol.
        StarBitmap *rPlatinaSymbol = (StarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Platina");
        if(rPlatinaSymbol) rPlatinaSymbol->Draw(cPlatinaR + 2.0f, cTxtY + 12.0f);
    }
}

///--[Unlock Package]
void MonoUIVendor::RenderUnlockPack(ShopInventoryPack *pPack, int pSlot, bool pIsOdd, float pAlpha, float pScrollTimer)
{
    ///--[Documentation]
    //--For Unlock mode, similar to the buy layout but renders item requirements instead of price/gems/etc.
    //  This version renders a counter/"ready" instead of the full ingredient list.
    if(!pPack || !pPack->mItem) return;

    ///--[Setup]
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    ///--[Constants]
    //--X positions.
    float cIconX  = 356.0f;

    //--Compute Y position.
    float cSlotY = 240.0f;
    float cSlotH =  56.0f;
    float cYPos = cSlotY + (cSlotH * pSlot);

    //--Y Offsets.
    float cTxtY = cYPos + 2.0f;

    //--Spacing
    //float cNameSpacing = 15.0f;

    //--Colors.
    StarlightColor cStandardText = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, pAlpha);
    StarlightColor cActiveText   = StarlightColor::MapRGBAF(0.0f, 1.0f, 0.0f, pAlpha);

    ///--[Standards]
    //--Stencil setup.
    glDisable(GL_STENCIL_TEST);

    //--Color.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);

    //--Icon.
    StarBitmap *rIcon = pPack->mItem->GetIconImage();
    if(rIcon) rIcon->Draw(cIconX, cTxtY + 12.0);

    //--Name.
    if(!pPack->mDisplayName)
        AdvImages.rFont_Mainline->DrawTextFixed(cxMono_UnlNameX, cTxtY, csMono_UnlMaxNameWid, 0, pPack->mItem->GetDisplayName());
    else
        AdvImages.rFont_Mainline->DrawTextFixed(cxMono_UnlNameX, cTxtY, csMono_UnlMaxNameWid, 0, pPack->mDisplayName);

    ///--[Unlock Information]
    //--Compute the unlocking data.
    int tTotalMaterials = 0;
    int tHaveMaterials = 0;
    ShopUnlockPack *rPackage = (ShopUnlockPack *)pPack->mUnlockRequirements->PushIterator();
    while(rPackage)
    {
        //--Increase the required amount.
        tTotalMaterials += rPackage->mNumberNeeded;

        //--Increase the held amount. It caps by the required amount.
        int tInInventory = rInventory->GetCountOf(rPackage->mItemName);
        if(tInInventory >= rPackage->mNumberNeeded)
        {
            tHaveMaterials += rPackage->mNumberNeeded;
        }
        else
        {
            tHaveMaterials += tInInventory;
        }

        //--Next.
        rPackage = (ShopUnlockPack *)pPack->mUnlockRequirements->AutoIterate();
    }

    //--If the amount required and the amount held match, then we can craft the item. Mark it as "ready".
    if(tHaveMaterials == tTotalMaterials)
    {
        cActiveText.SetAsMixerAlpha(pAlpha);
        AdvImages.rFont_Mainline->DrawText(cxMono_UnlMaterialsX, cTxtY, 0, 1.0f, xrStringData->str.mTextReady);
    }
    //--Display how many we have versus how many are needed.
    else
    {
        cStandardText.SetAsMixerAlpha(pAlpha);
        AdvImages.rFont_Mainline->DrawTextArgs(cxMono_UnlMaterialsX, cTxtY, 0, 1.0f, "%i/%i", tHaveMaterials, tTotalMaterials);
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}

///--[Comparison Pane]
//--Used by Buy/Sell etc. Not used for gem merging.
void MonoUIVendor::RenderComparisonPane(AdventureItem *pComparisonItem, float pAlpha)
{
    ///--[ ==================== Documentation ===================== ]
    //--Renders the properties of whatever item is selected if it were to be equipped by the comparison
    //  character. If there is no comparison character, doesn't render anything except the header.
    //--If the item is a gem, doesn't render the header, letting the subroutine do that.
    if(mCompareCharacterCur == -1)
    {
        //--If the item selected is a gem, then this routine will render something.
        if(pComparisonItem && pComparisonItem->IsGem())
        {
            if(RenderComparisonPaneGem(pComparisonItem, pAlpha)) return;
        }
    }

    //--Make sure the item exists. If it does not, stop here.
    if(!pComparisonItem || mCompareCharacterCur == -1) return;

    ///--[ =================== Common Rendering =================== ]
    ///--[Backing]
    AdvImages.rFrame_Comparison->Draw();

    ///--[Header]
    //--Title of Stats Area. Renders even if there is no item for properties.
    SetColor("Title", pAlpha);
    AdvImages.rFont_Heading->DrawText(1188.0f, 170.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextProperties);
    SetColor("Clear", pAlpha);

    ///--[Setup]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCompareCharacterCur);

    //--Setup variables.
    float cIconX         = 1050.0f;
    float cIconY         =  282.0f;
    float cIconH         =   36.0f;
    float cPropertyBaseX = cIconX         +  71.0f;
    float cPropertyBaseR = cPropertyBaseX +  47.0f;
    float cResistX       = cIconX         +  41.0f;
    float cResistR       = cResistX       +  62.0f;
    float cDamageX       = cIconX         + 100.0f;
    float cDamageR       = cDamageX       +  80.0f;
    float cPropertyBaseY = cIconY         -   4.0f;

    //--Alt-weapon. If the comparison slot is a weapon-alt, then the stats in the comparison window need to subtract
    //  the main weapon and add in the alt-weapon, as if it was the main weapon.
    EquipmentSlotPack *rPackage = rActiveEntity->GetEquipmentSlotPackageS(mCompareSlot);
    if(!rPackage) return;

    ///--[ ====================== Gem Slots ======================= ]
    //--Renders above the property listing since it's not in the statistic index.
    MonoImages.rOverlay_GemSlotIcon->Draw(cIconX, cIconY + (cIconH * -1));
    int tOldGemSlots = 0;
    int tNewGemSlots = 0;

    //--Get old value.
    AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
    if(rCurrentItem) tOldGemSlots = rCurrentItem->GetGemSlots();

    //--Get new value.
    tNewGemSlots = pComparisonItem->GetGemSlots();

    ///--[Render Relative]
    //--Only show the difference, positive or negative, and not the actual value. There is no base value for
    //  an entity to have in terms of gem slots.

    //--Value is lower:
    if(tNewGemSlots < tOldGemSlots)
    {
        StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
        AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * -1), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (tNewGemSlots - tOldGemSlots));
    }
    //--Value is higher:
    else if(tNewGemSlots > tOldGemSlots)
    {
        StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
        AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * -1), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i", (tNewGemSlots - tOldGemSlots));
    }

    //--Clean.
    SetColor("Clear", pAlpha);

    ///--[ ============== Iterate Across Properties =============== ]
    for(int i = 0; i < MONOUIVEN_ICONS_TOTAL; i ++)
    {
        ///--[Basics]
        //--Icon.
        AdvImages.rPropertyIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

        //--Statistic / Resistance.
        int tEntityValue = rActiveEntity->GetStatistic(mStatisticIndexes[i]);
        if(mShowRelativeValuesOnly) tEntityValue = 0;

        //--Damage Bonus. May not always be used.
        int tEntityBonusTags = rActiveEntity->GetTagCount(mBonusTagNames[i]);
        int tEntityMalusTags = rActiveEntity->GetTagCount(mMalusTagNames[i]);
        int tEntityDamageBonus = 100 + tEntityBonusTags - tEntityMalusTags;

        ///--[Alt Weapon Check]
        //--Check if this is an alt-weapon slot. If so, we need to modify the statistics.
        //  Note: Alternate weapons are not used in Monoceros, the code is only here for possible mods.
        if(rPackage->mIsWeaponAlternate)
        {
            //--Get the weapon in the slot.
            int tWeaponSlot = rActiveEntity->GetSlotForWeaponDamage();
            EquipmentSlotPack *rWeaponPackage = rActiveEntity->GetEquipmentSlotPackageI(tWeaponSlot);

            //--If there's a weapon in the main slot:
            if(rWeaponPackage->mEquippedItem)
            {
                //--Statistic / Resistance
                tEntityValue = tEntityValue - rWeaponPackage->mEquippedItem->GetStatisticNoGem(mStatisticIndexes[i]);

                //--Damage Bonus
                int tItemBonusTags = rWeaponPackage->mEquippedItem->GetTagCount(mBonusTagNames[i]);
                int tItemMalusTags = rWeaponPackage->mEquippedItem->GetTagCount(mMalusTagNames[i]);
                tEntityDamageBonus = tEntityDamageBonus - tItemBonusTags + tItemMalusTags;
            }

            //--Add the value of the alternate weapon. This will give the player the stats they'd have if they swapped in battle.
            AdventureItem *rReplaceWeapon = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
            if(rReplaceWeapon)
            {
                //--Statistic / Resistance
                tEntityValue = tEntityValue + rReplaceWeapon->GetStatisticNoGem(mStatisticIndexes[i]);

                //--Damage Bonus
                int tItemBonusTags = rReplaceWeapon->GetTagCount(mBonusTagNames[i]);
                int tItemMalusTags = rReplaceWeapon->GetTagCount(mMalusTagNames[i]);
                tEntityDamageBonus = tEntityDamageBonus + tItemBonusTags - tItemMalusTags;
            }
        }

        ///--[ ==== Stat Comparison - Base Property ===== ]
        //--This is a simple property with only one value to render.
        if(i < MONOUIVEN_RESISTANCE_BEGIN)
        {
            ///--[Normal]
            //--Setup.
            int tOldStatValue = 0;
            int tNewStatValue = 0;

            //--Stat value of the equipped item.
            AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
            if(rCurrentItem) tOldStatValue = rCurrentItem->GetStatisticNoGem(mStatisticIndexes[i]);

            //--Stat value of the replacement.
            tNewStatValue = pComparisonItem->GetStatisticNoGem(mStatisticIndexes[i]);

            ///--[Render Relative]
            //--Only show the difference, positive or negative, and not the actual value.
            if(mShowRelativeValuesOnly)
            {
                //--Value is lower:
                if(tNewStatValue < tOldStatValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (tNewStatValue - tOldStatValue));
                }
                //--Value is higher:
                else if(tNewStatValue > tOldStatValue)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i", (tNewStatValue - tOldStatValue));
                }
                //--Value is equal. Nothing renders.
                else
                {

                }
            }
            ///--[Render Absolute]
            //--Shows the character's stat value and then what it would change to.
            else
            {
                //--Base value.
                AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue);

                //--If the value in this slot is different from the old value, then display that.
                if(tOldStatValue != tNewStatValue)
                {
                    //--Value is lower:
                    if(tNewStatValue < tOldStatValue)
                    {
                        StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                    }
                    //--Higher:
                    else
                    {
                        StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                    }

                    //--Render.
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX + 5, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                    AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseR,     cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue + (tNewStatValue - tOldStatValue));
                }
            }

            //--Clean.
            SetColor("Clear", pAlpha);
        }
        ///--[ = Stat Comparison - Resistance and Damage = ]
        //--Resistance and Damage Bonus are side by side.
        else
        {
            ///--[Get Values]
            //--Equipped item values.
            AdventureItem *rCurrentItem = rActiveEntity->GetEquipmentBySlotS(mCompareSlot);
            int tOldResistance = 0;
            int tOldBonusTags  = 0;
            int tOldMalusTags  = 0;
            if(rCurrentItem)
            {
                tOldResistance = rCurrentItem->GetStatisticNoGem(mStatisticIndexes[i]);
                tOldBonusTags  = rCurrentItem->GetTagCount(mBonusTagNames[i]);
                tOldMalusTags  = rCurrentItem->GetTagCount(mMalusTagNames[i]);
            }

            //--Replacement values.
            int tNewResistance = pComparisonItem->GetStatisticNoGem(mStatisticIndexes[i]);
            int tNewBonusTags  = pComparisonItem->GetTagCount(mBonusTagNames[i]);
            int tNewMalusTags  = pComparisonItem->GetTagCount(mMalusTagNames[i]);

            //--Compute Damage
            int tOldDamageValue = 100 + tOldBonusTags - tOldMalusTags;
            if(tOldDamageValue < 0) tOldDamageValue = 0;
            int tNewDamageValue = 100 + tNewBonusTags - tNewMalusTags;
            if(tNewDamageValue < 0) tNewDamageValue = 0;

            ///--[Absolute Values]
            //--Only show the difference, positive or negative, and not the actual value.
            if(mShowRelativeValuesOnly)
            {
                ///--[Resistance]
                //--Value is lower:
                if(tNewResistance < tOldResistance)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cResistX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (tNewResistance - tOldResistance));
                }
                //--Higher:
                else if(tNewResistance > tOldResistance)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cResistX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i", (tNewResistance - tOldResistance));
                }

                ///--[Damage Bonus]
                //--Value is lower:
                if(tNewDamageValue < tOldDamageValue)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cDamageX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", (tNewDamageValue - tOldDamageValue));
                }
                //--Higher:
                else if(tNewDamageValue > tOldDamageValue)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                    AdvImages.rFont_Statistic->DrawTextArgs(cDamageX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i%%", (tNewDamageValue - tOldDamageValue));
                }
            }
            ///--[Relative Values]
            //--Show the entity's value and then what it would change to.
            else
            {
                ///--[Resistance]
                //--If the resistance is Immune, render that and don't do a comparison.
                if(tEntityValue >= 1000)
                {
                    AdvImages.rFont_Statistic->DrawText(cResistX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, xrStringData->str.mTextImmune);
                }
                else
                {
                    //--Render the base value.
                    AdvImages.rFont_Statistic->DrawTextArgs(cResistX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue);

                    //--If the value in this slot is different from the old value, then display that.
                    if(tOldResistance != tNewResistance)
                    {
                        //--Value is lower:
                        if(tNewResistance < tOldResistance)
                        {
                            StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                        }
                        //--Higher:
                        else
                        {
                            StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                        }

                        //--Render.
                        AdvImages.rFont_Statistic->DrawTextArgs(cResistX + 5, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                        AdvImages.rFont_Statistic->DrawTextArgs(cResistR,     cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tEntityValue + (tNewResistance - tOldResistance));

                        //--Clean.
                        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
                    }
                }

                ///--[Damage Bonus]
                //--Base value.
                AdvImages.rFont_Statistic->DrawTextArgs(cDamageX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tEntityDamageBonus);

                //--If the value in this slot is different from the old value, then display that.
                if(tOldDamageValue != tNewDamageValue)
                {
                    //--Value is lower:
                    if(tNewDamageValue < tOldDamageValue)
                    {
                        StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                    }
                    //--Higher:
                    else
                    {
                        StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                    }

                    //--Render.
                    AdvImages.rFont_Statistic->DrawTextArgs(cDamageX + 5, cPropertyBaseY + (cIconH * i) - 2.0f, 0, 1.0f, "->");
                    AdvImages.rFont_Statistic->DrawTextArgs(cDamageR,     cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i%%", tEntityDamageBonus + (tNewDamageValue - tOldDamageValue));
                }
            }

            //--Clean.
            SetColor("Clear", pAlpha);
        }
    }

    ///--[Comparison Item]
    //--Show what item is being compared to.
    if(rCurrentItem)
    {
        //--Base.
        AdvImages.rFont_Statistic->DrawText(1104.0f, 642.0f, 0, 1.0f, xrStringData->str.mTextComparingTo);

        //--Icon.
        StarBitmap *rItemIcon = rCurrentItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(1114.0f, 665.0f);

        //--Name.
        AdvImages.rFont_Statistic->DrawText(1137.0f, 664.0f, 0, 1.0f, rCurrentItem->GetDisplayName());
    }
    else
    {
        //--Base.
        AdvImages.rFont_Statistic->DrawText(1104.0f, 642.0f, 0, 1.0f, xrStringData->str.mTextComparingTo);
        SetColor("Greyed", pAlpha);
        AdvImages.rFont_Statistic->DrawTextArgs(1137.0f, 664.0f, 0, 1.0f, "[%s]", mCompareSlot);
        SetColor("Clear", pAlpha);
    }
}
bool MonoUIVendor::RenderComparisonPaneGem(AdventureItem *pGem, float pAlpha)
{
    ///--[Documentation]
    //--Renders the comparison pane item, except in gem format. Gems don't compare to anything. Does not render
    //  a header if the item does not exist.
    if(!pGem || !pGem->IsGem()) return false;

    ///--[Backing]
    //MonoImages.rFrame_GemProperties->Draw();

    ///--[Header]
    //--Title of Stats Area. Renders even if there is no item for properties.
    SetColor("Title", pAlpha);
    AdvImages.rFont_Heading->DrawText(1216.0f, 214.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextGemStats);
    SetColor("Clear", pAlpha);

    ///--[Setup]
    //--Fast-access pointers.
    //AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    //AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mCompareCharacterCur);

    ///--[Setup]
    //--Setup variables.
    float cIconX         = 1105.0f;
    float cIconY         =  282.0f;
    float cIconH         =   36.0f;
    float cPropertyBaseX = cIconX         +  71.0f;
    float cResistX       = cIconX         +  71.0f;
    float cDamageX       = cIconX         + 200.0f;
    float cPropertyBaseY = cIconY         -   4.0f;

    ///--[Iterate Across Properties]
    //--Render.
    for(int i = 0; i < MONOUIVEN_ICONS_TOTAL; i ++)
    {
        ///--[Basics]
        //--Icon.
        AdvImages.rPropertyIcons[i]->Draw(cIconX, cIconY + (cIconH * i));

        //--Property. Can be either a statistic or a resistance.
        int tStatistic = pGem->GetStatistic(mStatisticIndexes[i]);

        //--Damage Bonus. May not always be used.
        int tGemBonusTags = pGem->GetTagCount(mBonusTagNames[i]);
        int tGemMalusTags = pGem->GetTagCount(mMalusTagNames[i]);
        int tGemDamageBonus = tGemBonusTags - tGemMalusTags;

        ///--[Stat Comparison - Base Property]
        //--This is a simple property with only one value to render.
        if(i < MONOUIVEN_RESISTANCE_BEGIN)
        {
            //--Do not display zeroes.
            //if(tStatistic == 0) continue;

            //--Stat Penalty:
            if(tStatistic < 0)
            {
                StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
            }
            //--Stat Bonus:
            else if(tStatistic > 0)
            {
                StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
            }
            //--Zero.
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0, pAlpha);
            }

            //--Render.
            AdvImages.rFont_Statistic->DrawTextArgs(cPropertyBaseX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i", tStatistic);
        }
        ///--[Stat Comparison - Resistance and Damage]
        //--Resistance and Damage Bonus are side by side.
        else
        {
            ///--[Resistance]
            //--Skip zeroes.
            //if(tStatistic != 0)
            {
                //--Stat Penalty:
                if(tStatistic < 0)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                }
                //--Stat Bonus:
                else if(tStatistic > 0)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                }
                //--Zero.
                else
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 1.0, pAlpha);
                }

                //--Render.
                AdvImages.rFont_Statistic->DrawTextArgs(cResistX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i", tStatistic);
            }

            ///--[Damage Bonus]
            //--Skip zeroes.
            //if(tGemDamageBonus != 0)
            {
                //--Stat Penalty:
                if(tGemDamageBonus < 0)
                {
                    StarlightColor::SetMixer(0.80f, 0.20f, 0.10f, pAlpha);
                }
                //--Stat Bonus:
                else if(tGemDamageBonus > 0)
                {
                    StarlightColor::SetMixer(0.60f, 0.90f, 0.50f, pAlpha);
                }
                //--Zero.
                else
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 1.0, pAlpha);
                }

                //--Render.
                AdvImages.rFont_Statistic->DrawTextArgs(cDamageX, cPropertyBaseY + (cIconH * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%+i%%", tGemDamageBonus);
            }
        }

        //--Clean.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pAlpha);
    }

    //--Finish up. Returns true to indicate we handled rendering.
    return true;
}
