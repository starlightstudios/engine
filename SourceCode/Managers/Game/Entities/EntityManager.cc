//--Base
#include "EntityManager.h"

//--Classes
#include "Actor.h"
#include "AdventureLevel.h"
#include "TilemapActor.h"
#include "PlayerPony.h"
#include "RootEntity.h"
#include "StarCamera2D.h"

//--CoreClasses
#include "StarLinkedList.h"
#include "StarLoadInterrupt.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"
#include "Startup.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "CameraManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "NetworkManager.h"
#include "OptionsManager.h"

///--[Debug]
//#define EM_PURGE_DEBUG
#ifdef EM_PURGE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
EntityManager::EntityManager()
{
    //--[EntityManager]
    //--System
    mPlayer = new PlayerPony();

    //--Storage
    mLastCreatedID = 0;
    mEntityList = new StarLinkedList(true);
    mEntityList->AddElement("Player", mPlayer, &EntityManager::PreserveThis);

    //--Public Variables
    mHasPendingClear = false;
    mDisplayEntities = false;

    //--Special Lists
    mSpecialListList = new StarLinkedList(true);

    //--Turn Controlling
    mTurnCount = 0;
    rActingEntity = NULL;
    rLastPlayerEntity = NULL;
    mTurnControlScript = NULL;

    //--Resetting
    mIsFirstResetCall = true;
    mCleanerScript = NULL;
}
EntityManager::~EntityManager()
{
    delete mPlayer;
    delete mEntityList;
    delete mSpecialListList;
    free(mTurnControlScript);
    free(mCleanerScript);
}
void EntityManager::PreserveThis(void *pPtr)
{
    //--Dummy function.  When clearing the EM, if this function is found, the entity will not be
    //  purged, but instead left on the list.
}

//--Particle special handling
int EntityManager::xRunningRenderCounter = 0;
ParticleRenderPack *EntityManager::xParticleRenderList = NULL;

///===================================== Property Queries =========================================
RootEntity *EntityManager::GetEntityI(uint32_t pSearchID)
{
    //--Locates an Entity by its UniqueID.  NULL if the ID is not found.
    RootEntity *rEntry = (RootEntity *)mEntityList->PushIterator();
    while(rEntry)
    {
        //--Check
        if(rEntry->GetID() == pSearchID)
        {
            mEntityList->PopIterator();
            return rEntry;
        }

        //--Iterate
        rEntry = (RootEntity *)mEntityList->AutoIterate();
    }
    return NULL;
}
RootEntity *EntityManager::GetEntity(const char *pName)
{
    //--Special case: Returns the entity atop the activity stack.  NULL is a valid return, and
    //  the object may not necessarily be a RootObject type.
    if(pName && !strcasecmp(pName, "This"))
    {
        return (RootEntity *)DataLibrary::Fetch()->rActiveObject;
    }

    //--Special case: Return the entity currently controlled by the player. This can be NULL!
    if(pName && !strcasecmp(pName, "PlayerEntity"))
    {
        //--AdventureLevel must exist.
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(!rActiveLevel) return NULL;

        //--Return the entity.
        TilemapActor *rPlayerActor = rActiveLevel->LocatePlayerActor();
        return rPlayerActor;
    }

    return (RootEntity *)mEntityList->GetElementByName(pName);
}
RootEntity *EntityManager::GetEntityPartialS(const char *pPartialName)
{
    return (RootEntity *)mEntityList->GetElementByPartialName(pPartialName);
}
bool EntityManager::IsClipped(int pX, int pY)
{
    //--Scans all entities and checks collisions of the point against them.  Breaks upon first
    //  true result (doesn't matter which entity collided).
    //--If your program does not have clipped entities, leave this segment commented out.
    /*RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
    while(rEntity)
    {
        //--Re-enable this if it's possible for entities to be clipped.
        if(rEntity->IsClipped(pX, pY))
        {
            mEntityList->PopIterator();
            return true;
        }

        //--Iterate
        rEntity = (RootEntity *)mEntityList->AutoIterate();
    }*/

    return false;
}
int EntityManager::GetLastReggedID()
{
    return mLastCreatedID;
}

///======================================= Manipulators ===========================================
void EntityManager::RenamePointer(const char *pName, void *pPtr)
{
    //--Renames the pointer, if it is found.
    if(!pName || !pPtr) return;

    void *rCheckPtr = mEntityList->SetToHeadAndReturn();
    while(rCheckPtr)
    {
        if(rCheckPtr == pPtr)
        {
            StarLinkedListEntry *rWholeEntry = mEntityList->GetRandomPointerWholeEntry();
            ResetString(rWholeEntry->mName, pName);
            return;
        }

        rCheckPtr = mEntityList->IncrementAndGetRandomPointerEntry();
    }
}
void EntityManager::RegisterPointer(const char *pName, void *pPtr)
{
    //--Registers a pointer with DontDeleteThis as its deletion function.  This
    //  makes it effectively a reference pointer.
    if(!pName || !pPtr) return;
    mEntityList->AddElement(pName, pPtr, &DontDeleteThis);

    mLastCreatedID = ((RootEntity *)pPtr)->GetID();
}
void EntityManager::RegisterPointer(const char *pName, void *pPtr, DeletionFunctionPtr pDelFunc)
{
    //--Registers an entity with a specified deletion function.
    if(!pName || !pPtr) return;
    mEntityList->AddElement(pName, pPtr, pDelFunc);

    mLastCreatedID = ((RootEntity *)pPtr)->GetID();
}
void EntityManager::RemovePointer(const char *pName)
{
    //--Removes a specified pointer.
    if(!pName) return;
    mEntityList->RemoveElementS(pName);
}
void EntityManager::RemovePointerUC(uint32_t pUniqueID)
{
    //--Removes a pointer by specifying its unique ID instead of a name.
    if(pUniqueID == 0) return;

    RootObject *rEntry = (RootObject *)mEntityList->SetToHeadAndReturn();
    while(rEntry)
    {
        //--Check
        if(rEntry->GetID() == pUniqueID)
        {
            mEntityList->RemoveRandomPointerEntry();
            return;
        }

        //--Iterate
        rEntry = (RootObject *)mEntityList->IncrementAndGetRandomPointerEntry();
    }
}
void EntityManager::LiberatePointerP(void *pPtr)
{
    //--Removes the pointer from the EntityList, but does not delete it regardless of what the flag
    //  was at time of registration.
    mEntityList->SetDeallocation(false);
    mEntityList->RemoveElementP(pPtr);
    mEntityList->SetDeallocation(true);
}
void EntityManager::ClearAll()
{
    //--Clears all entities in the EM.  Any entity using PreserveThis as its deletion function
    //  will be placed on the list after the clear.
    //--Preserve this will not delete the object.  It can exist on the entity list safely
    //  during the clear.
    StarLinkedList *tPreserveList = new StarLinkedList(false);

    void *rEntry = mEntityList->SetToHeadAndReturn();
    while(rEntry)
    {
        //--Full Entry has the deletion function.
        StarLinkedListEntry *rFullEntry = mEntityList->GetRandomPointerWholeEntry();
        if(rFullEntry->rDeletionFunc == &EntityManager::PreserveThis)
        {
            tPreserveList->AddElement(rFullEntry->mName, rFullEntry->rData);
        }

        //--Iterate
        rEntry = mEntityList->IncrementAndGetRandomPointerEntry();
    }

    //--Clear the list.
    mEntityList->ClearList();

    //--Replace all preserved entities.
    rEntry = tPreserveList->PushIterator();
    while(rEntry)
    {
        //--Re-add it to the EntityList.
        mEntityList->AddElement(tPreserveList->GetIteratorName(), rEntry, &EntityManager::PreserveThis);

        //--Iterate
        rEntry = tPreserveList->AutoIterate();
    }

    //--Clean up.
    delete tPreserveList;
}
void EntityManager::SetCleanerScript(const char *pPath)
{
    ResetString(mCleanerScript, pPath);
}

///======================================= Core Methods ===========================================
void EntityManager::Reset(const char *pResetType)
{
    //--Resets the EntityManager based on the type provided.

    //--The Full Reset is only called at the end of a tick. It wipes the EM.
    if(!strcasecmp(pResetType, "Full Game Reset"))
    {
        //--On the first reset call...
        if(mIsFirstResetCall)
        {
            //--Run the cleaner script.
            if(mCleanerScript) LuaManager::Fetch()->ExecuteLuaFile(mCleanerScript);

            //--Player resets.
            mPlayer->Reset(pResetType);

            //--Clear up all entities.
            ClearAll();

            //--Reset the turn controllers.
            mTurnCount = 0;
            rActingEntity = NULL;
            rLastPlayerEntity = NULL;
            free(mTurnControlScript);
            mTurnControlScript = NULL;

            //--Set this flag.
            mIsFirstResetCall = false;
        }
        //--On the second reset call...
        else
        {
            //--Flags.
            mIsFirstResetCall = true;
            Global::Shared()->gLoadInterrupt->Reset(274);

            //--Rerun the flexible boot in Startup.cc.
            ProgramFlexibleBoot();
        }
    }
    //--For safety, the EntityManager will not finish the clear until the end of the tick. If an
    //  entity on the EM attempts to clear the EM, there won't be a crash.
    else
    {
        mHasPendingClear = true;
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void EntityManager::PurgeDestructingEntities()
{
    //--Subroutine which purges the Entities list of all entities which are marked for self-destruction.
    //  Uses the random-access pointer.
    bool tRemovedAnyEntities = false;
    RootEntity *rEntity = (RootEntity *)mEntityList->SetToHeadAndReturn();
    while(rEntity)
    {
        if(rEntity->mSelfDestruct)
        {
            tRemovedAnyEntities = true;
            DebugPush(true, "Entity %s %p is self-destructing.\n", rEntity->GetName(), rEntity);

            rEntity->RespondToSelfDestruct();
            DebugPrint("Finished destruction, removing from list.\n");
            mEntityList->RemoveRandomPointerEntry();
            DebugPop("Finished destruction.\n");
        }

        rEntity = (RootEntity *)mEntityList->IncrementAndGetRandomPointerEntry();
    }

    //--Debug:
    if(tRemovedAnyEntities)
    {
        //--Double-check that the last acting entity actually still exists!
        DebugPush(true, "Post-purge handlers.\n");
        DebugPrint("Checking acting entity %p.\n", rActingEntity);
        if(!mEntityList->IsElementOnList(rActingEntity)) rActingEntity = NULL;
        DebugPrint("Checking player entity %p.\n", rLastPlayerEntity);
        if(!mEntityList->IsElementOnList(rLastPlayerEntity)) rLastPlayerEntity = NULL;
        DebugPop("Finished post purge.\n");
    }
}
void EntityManager::Update()
{
    //--Update function.  Called every tick, tells all entities to update.
    DebugManager::PushPrint("Debug_EntityManager", "[Entity Manager] Update Begin\n");
    DebugManager::Print("Size of list in Entity Manager %i\n", mEntityList->GetListSize());

    //--Update all the entities.
    RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
    while(rEntity)
    {
        //DebugManager::Print("Updating %p ", rEntity);
        //DebugManager::Print("%i %s %d- ", rEntity->GetType(), mEntityList->GetRandomPointerName(), rEntity->GetID());
        rEntity->Update();
        //DebugManager::Print("Done\n");
        rEntity = (RootEntity *)mEntityList->AutoIterate();
    }

    //--Any entity which needs to purge itself *before* the projectile update can do so here.
    PurgeDestructingEntities();

    DebugManager::PopPrint("[Entity Manager] Update Complete\n");
}
void EntityManager::PostUpdate()
{
    //--Executed at the end of a tick, resets flags in entities that have a post-update routine.
    //  The routine is overridable and defaults to doing nothing.
    RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
    while(rEntity)
    {
        rEntity->PostUpdate();
        rEntity = (RootEntity *)mEntityList->AutoIterate();
    }
}
void EntityManager::UpdatePaused(uint8_t pPauseFlags)
{
    //--Update that executes when the program is paused for any reason. The default Pure Engine
    //  does nothing during a pause.
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void EntityManager::Render()
{
    //--Rendering entry point. All entities have their 2D activities called.
    DebugManager::PushPrint(true, "[Entity Manager] Render Begin\n");

    //--Setup
    DisplayManager::StdModelPush();

    //--Position to the Camera.
    StarCamera2D *rActiveCamera = CameraManager::FetchActiveCamera2D();
    rActiveCamera->Translate2D();

    //--Render all Entities that are not on special lists.
    RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
    while(rEntity)
    {
        DebugManager::Print("Rendering %p %i %s - ", rEntity, rEntity->GetType(), mEntityList->GetIteratorName());
        rEntity->Render();
        DebugManager::Print("Done\n");
        rEntity = (RootEntity *)mEntityList->AutoIterate();
    }
    DebugManager::PopPrint("[Entity Manager] Render Complete\n");

    //--If you want to render entities on the special list, do so here:
    //  SPACE

    //--Clean
    DisplayManager::StdModelPop();
}
void EntityManager::AddToRenderingList(StarLinkedList *pRenderingList)
{
    //--Adds all the entities to the deferred-rendering list.
    RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
    while(rEntity)
    {
        rEntity->AddToRenderList(pRenderingList);
        rEntity = (RootEntity *)mEntityList->AutoIterate();
    }
}

///====================================== Pointer Routing =========================================
PlayerPony *EntityManager::GetPlayer()
{
    return mPlayer;
}
StarLinkedList *EntityManager::GetEntityList()
{
    return mEntityList;
}
StarLinkedList *EntityManager::GetListContaining(int pType)
{
    //--Returns a list which contains all entities matching the given type. The list has deallocation
    //  turned off, and must be deleted when you are done with it.
    StarLinkedList *nList = new StarLinkedList(false);

    RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
    while(rEntity)
    {
        if(rEntity->GetType() == pType)
        {
            nList->AddElement("X", rEntity);
        }

        rEntity = (RootEntity *)mEntityList->AutoIterate();
    }

    return nList;
}
StarLinkedList *EntityManager::GetListContaining(int pTypeBegin, int pTypeEnd)
{
    //--Similar to above, but uses a type range.
    StarLinkedList *nList = new StarLinkedList(false);

    RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
    while(rEntity)
    {
        if(rEntity->GetType() >= pTypeBegin && rEntity->GetType() <= pTypeEnd) nList->AddElement("X", rEntity);

        rEntity = (RootEntity *)mEntityList->AutoIterate();
    }

    return nList;
}

///===================================== Static Functions =========================================
EntityManager *EntityManager::Fetch()
{
    return Global::Shared()->gEntityManager;
}

///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
