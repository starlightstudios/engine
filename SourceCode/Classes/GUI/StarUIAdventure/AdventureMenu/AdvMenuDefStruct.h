///========================= AdventureMenu Definitions and Structures =============================
//--This file contains headers and defintions that are in common use across the Adventure UI.
#pragma once

///--[Includes]
#include "StarBitmap.h"

///======================================== Definitions ===========================================
///--[Menu Mode Codes]
//--These describe what UI object is current handling the update for various modes, allowing the objects
//  to switch who is controlling them. The special code AM_MODE_EXITNOW allows the UI to close without
//  going back to the base menu.

//--Base Modes
#define AM_MODE_EXITNOW -2
#define AM_MODE_NOEXIT -1
#define AM_MODE_NONE 0
#define AM_MODE_BASE 0
#define AM_MODE_ITEMS 1
#define AM_MODE_EQUIP 2
#define AM_MODE_STATUS 3
#define AM_MODE_SKILLS 4
#define AM_MODE_MAP 5
#define AM_MODE_OPTIONS 6
#define AM_MODE_QUIT 7
#define AM_MODE_DOCTOR 8
#define AM_MODE_JOURNAL 9
#define AM_MODE_TOTAL 10

//--Campfire Menu Modes
#define AM_CAMPFIRE_MODE_BASE 0
#define AM_CAMPFIRE_MODE_CHAT 1
#define AM_CAMPFIRE_MODE_FILESELECT 2
#define AM_CAMPFIRE_MODE_COSTUMES 3
#define AM_CAMPFIRE_MODE_PASSWORD 4
#define AM_CAMPFIRE_MODE_RELIVE 5
#define AM_CAMPFIRE_MODE_FORMS 6
#define AM_CAMPFIRE_MODE_WARP 7
#define AM_CAMPFIRE_MODE_WARP_EXIT_NOW 8
#define AM_CAMPFIRE_MODE_REST_NOW 9

///======================================== Structures ============================================
///--[Alignment Package]
//--Used for grid rendering. Contains standardized alignment variables such as offsets and render
//  percentages. AdvUIGrid objects require this in their datatypes.
typedef struct AdvMenuStandardAlignments
{
    //--Members
    StarBitmap *rImage;
    float mOffsetX;
    float mOffsetY;
    float mImageX;
    float mImageY;
    float mImageW;
    float mImageH;

    //--Functions
    void Initialize()
    {
        rImage = NULL;
        mOffsetX = 0.0f;
        mOffsetY = 0.0f;
        mImageX = 0.0f;
        mImageY = 0.0f;
        mImageW = 1.0f;
        mImageH = 1.0f;
    }
}AdvMenuStandardAlignments;
