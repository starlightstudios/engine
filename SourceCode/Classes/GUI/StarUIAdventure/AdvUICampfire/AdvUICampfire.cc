//--Base
#include "AdvUICampfire.h"

//--Classes
#include "AdventureLevel.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
AdvUICampfire::AdvUICampfire()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_STARUIPIECE;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "ACMP");

    ///--[Visiblity]
    mVisibilityTimerMax = cxAdvVisTicks;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ======= AdvUIGrid ======== ]
    ///--[System]
    cGridNameColor.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[Cursor]
    ///--[Positioning]
    mGridRenderCenterX  = (VIRTUAL_CANVAS_X * 0.50f);
    mGridRenderCenterY  = (VIRTUAL_CANVAS_Y * 0.35f) + 50.0f;
    mGridRenderSpacingX = (VIRTUAL_CANVAS_X * 0.10f);
    mGridRenderSpacingY = (VIRTUAL_CANVAS_X * 0.10f);

    ///--[Grid Storage]
    ///--[Rendering]
    ///--[ ===== AdvUICampfire ====== ]
    ///--[Variables]
    mIsBenchMode = false;
    mHasBuiltGrid = false;
    mDontShowHighlight = true;

    //--Cursor
    mHighlightPos.Initialize();
    mHighlightSize.Initialize();

    //--Strings
    mShowHelpString = new StarlightString();

    ///--[Images]
    memset(&AdvImages, 0, sizeof(AdvImages));

    ///--[ ================ Construction ================ ]
    //--Allocate Strings
    AllocateHelpStrings(cxAdvCampfireHelpStrings);

    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("AdvImages", &AdvImages, sizeof(AdvImages));
}
AdvUICampfire::~AdvUICampfire()
{
    delete mShowHelpString;
}
void AdvUICampfire::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    ResolveSeriesInto("Adventure Campfire UI", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",     &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    ///--[AdvUIGrid]
    //--Extra handler, the name font needs to be set and checked.
    rFont_Name = AdvImages.rFont_Mainline;
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void AdvUICampfire::SetCursorByBackwardsCode(int pMode)
{
}
void AdvUICampfire::SetCursor(int pCursor)
{
    SetCursorByCode(pCursor);
}
void AdvUICampfire::TakeForeground()
{
    ///--[Variables]
    //--Flip this flag on so the UI doesn't immediately close, as the key to open it from the AdventureLevel
    //  is the same as the close key.
    SetCursorByCode(AM_CAMPFIRE_REST);

    ///--[Images]
    //--Because the icons for the campfire entries can change depending on circumstances, icon lookups are
    //  rebuilt each time the object takes the foreground.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    AdvImages.rIcons[AM_CAMPFIRE_REST]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Rest");
    AdvImages.rIcons[AM_CAMPFIRE_CHAT]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Chat");
    AdvImages.rIcons[AM_CAMPFIRE_SAVE]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Save");
    AdvImages.rIcons[AM_CAMPFIRE_COSTUMES] = (StarBitmap *)rDataLibrary->GetEntry(AdventureMenu::xCostumeIconPath);
    AdvImages.rIcons[AM_CAMPFIRE_FORM]     = (StarBitmap *)rDataLibrary->GetEntry(AdventureMenu::xTransformIconPath);
    AdvImages.rIcons[AM_CAMPFIRE_WARP]     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Warp");
    AdvImages.rIcons[AM_CAMPFIRE_RELIVE]   = (StarBitmap *)rDataLibrary->GetEntry(AdventureMenu::xReliveIconPath);
    AdvImages.rIcons[AM_CAMPFIRE_PASSWORD] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenuIcon/Password");
    mImagesReady = VerifyImages();

    ///--[Construction]
    //--On the first build, setup the menu grid.
    if(!mHasBuiltGrid)
    {
        ///--[Basic Setup]
        //--Construct the grid and set its positions.
        mHasBuiltGrid = true;
        AllocateGrid(5, 3); //Three lines are allocated to keep the center point same as the base UI. They are not used.
        ProcessGrid();

        ///--[Components]
        //--Populate the grid.
        QuicksetGridEntry(0, 0, AM_CAMPFIRE_COSTUMES, "Costumes", AdvImages.rIcons[AM_CAMPFIRE_COSTUMES]);
        QuicksetGridEntry(1, 0, AM_CAMPFIRE_FORM,     "Forms",    AdvImages.rIcons[AM_CAMPFIRE_FORM]);
        QuicksetGridEntry(2, 0, AM_CAMPFIRE_REST,     "Rest",     AdvImages.rIcons[AM_CAMPFIRE_REST]);
        QuicksetGridEntry(3, 0, AM_CAMPFIRE_CHAT,     "Chat",     AdvImages.rIcons[AM_CAMPFIRE_CHAT]);
        QuicksetGridEntry(4, 0, AM_CAMPFIRE_RELIVE,   "Relive",   AdvImages.rIcons[AM_CAMPFIRE_RELIVE]);
        QuicksetGridEntry(1, 1, AM_CAMPFIRE_WARP,     "Warp",     AdvImages.rIcons[AM_CAMPFIRE_WARP]);
        QuicksetGridEntry(2, 1, AM_CAMPFIRE_SAVE,     "Save",     AdvImages.rIcons[AM_CAMPFIRE_SAVE]);
        QuicksetGridEntry(3, 1, AM_CAMPFIRE_PASSWORD, "Password", AdvImages.rIcons[AM_CAMPFIRE_PASSWORD]);

        //--Reset cursor to rest.
        SetCursorByCode(AM_CAMPFIRE_REST);
    }

    //--Cursor.
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    ///--[Strings]
    //--"Show Help" string
    mShowHelpString->SetString("[IMG0] Show Help");
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();
}
void AdvUICampfire::QuicksetGridEntry(int pX, int pY, int pCode, const char *pName, StarBitmap *pImagePtr)
{
    ///--[Documentation]
    //--Same as the base function, but manually forces the sizes to 96x96.

    ///--[Resolve]
    //--Get and check the package. If it doesn't exist, print a warning.
    GridPackage *rPackage = GetPackageAt(pX, pY);
    if(!rPackage)
    {
        fprintf(stderr, "AdvUIGrid:QuicksetGridEntry() - Warning. Attempted to set package %i %i to %s, but package was not found.\n", pX, pY, pName);
        return;
    }

    ///--[Set]
    //--Basic variables.
    rPackage->mCode = pCode;
    strcpy(rPackage->mLocalName, pName);
    rPackage->mAlignments.Initialize();
    rPackage->mAlignments.rImage = pImagePtr;

    //--The image sizes default to the image's width and height, assuming it exists.
    if(pImagePtr)
    {
        rPackage->mAlignments.mImageW = 96.0f;
        rPackage->mAlignments.mImageH = 96.0f;
    }
}
void AdvUICampfire::SetBenchMode(bool pFlag)
{
    mIsBenchMode = pFlag;
}

///======================================= Core Methods ===========================================
void AdvUICampfire::RefreshMenuHelp()
{
    ///--[Documentation]
    //--Called whenever the help menu is called, refreshes the strings in case the controls changed.
    int i = 0;

    ///--[Set Lines]
    //--Common lines.
    SetHelpString(i, "The campfire menu allows you to rest your party, which will respawn any enemies you");
    SetHelpString(i, "have defeated and restores your health and doctor bag. It also allows you to use");
    SetHelpString(i, "the submenus to change your character's jobs and costumes, chat with your party");
    SetHelpString(i, "members, warp to any campfire you've visited, and view certain scenes you've");
    SetHelpString(i, "seen before.");
    SetHelpString(i, "");
    SetHelpString(i, "If you are resting at a bench, you will not be able to warp. All other functions are");
    SetHelpString(i, "the same.");
    SetHelpString(i, "");
    SetHelpString(i, "Press [IMG0] or [IMG1] to exit this help menu.", 2, "F1", "Cancel");

    ///--[Image Crossreference]
    //--Order all strings to crossreference AdvImages.
    for(int p = 0; p < mHelpStringsMax; p ++)
    {
        mHelpMenuStrings[p]->CrossreferenceImages();
    }
}
void AdvUICampfire::RecomputeCursorPositions()
{
    RecomputeGridCursorPosition(mHighlightPos, mHighlightSize, 1.0f);
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool AdvUICampfire::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ===== Documentation and Setup ====== ]
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mVisibilityTimer < mVisibilityTimerMax)
    {
        mVisibilityTimer ++;
        RecomputeCursorPositions();
        mHighlightPos.Complete();
        mHighlightSize.Complete();
    }

    //--Standard Timers.
    StandardTimer(mHelpVisibilityTimer, 0, cxAdvVisTicks, mIsShowingHelp);

    //--Grid timers.
    UpdateGridTimers();

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Set this flag.
    mDontShowHighlight = false;

    ///--[ ======== Update Intercepts ========= ]
    ///--[Help Menu]
    //--If help is active, pushing F1 or Cancel exits. All other controls are ignored.
    if(mIsShowingHelp)
    {
        AutoHelpClose("F1", "Cancel", NULL);
        return true;
    }

    //--Otherwise, push F1 to activate help.
    if(AutoHelpOpen("F1", NULL, NULL)) return true;

    ///--[ =========== Update Call ============ ]
    ///--[Grid Handler]
    //--Run subroutine. If it came back true, the grid selection changed.
    if(UpdateGridControls())
    {
        RecomputeCursorPositions();
        return true;
    }

    ///--[Activation Handler]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get what option is under the cursor.
        int tGridOption = GetGridCodeAtCursor();

        //--Rest. Activates immediately.
        if(tGridOption == AM_CAMPFIRE_REST)
        {
            //--Order the AdventureLevel to begin resting. This causes a fade out.
            AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
            if(rActiveLevel) rActiveLevel->BeginRestSequence();

            //--Exit.
            mCodeBackward = AM_MODE_EXITNOW;
            FlagExit();

            //--Also handles SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Chat submenu.
        else if(tGridOption == AM_CAMPFIRE_CHAT)
        {
            mCodeBackward = AM_CAMPFIRE_MODE_CHAT;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--File-selection submenu.
        else if(tGridOption == AM_CAMPFIRE_SAVE)
        {
            mCodeBackward = AM_CAMPFIRE_MODE_FILESELECT;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Costume submenu. SFX is handled by the subroutine.
        else if(tGridOption == AM_CAMPFIRE_COSTUMES)
        {
            mCodeBackward = AM_CAMPFIRE_MODE_COSTUMES;
            FlagExit();
        }
        //--Password submenu.
        else if(tGridOption == AM_CAMPFIRE_PASSWORD)
        {
            mCodeBackward = AM_CAMPFIRE_MODE_PASSWORD;
            FlagExit();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Relive submenu.
        else if(tGridOption == AM_CAMPFIRE_RELIVE)
        {
            mCodeBackward = AM_CAMPFIRE_MODE_RELIVE;
            FlagExit();
        }
        //--Forms submenu.
        else if(tGridOption == AM_CAMPFIRE_FORM)
        {
            mCodeBackward = AM_CAMPFIRE_MODE_FORMS;
            FlagExit();
        }
        //--Warp submenu.
        else if(tGridOption == AM_CAMPFIRE_WARP)
        {
            mCodeBackward = AM_CAMPFIRE_MODE_WARP;
            FlagExit();
        }
        //--Error.
        else
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mCodeBackward = AM_MODE_NONE;
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--We handled the update. If we passed the cancel check, unset the first-tick flag.
    return true;
}
void AdvUICampfire::UpdateBackground()
{
    ///--[Documentation and Setup]
    //--Update when the object knows it's in the background. Counts down its timer.
    if(mVisibilityTimer < 1) return;

    //--Run timers.
    mVisibilityTimer --;
    RecomputeCursorPositions();
    mHighlightPos.Complete();
    mHighlightSize.Complete();

    //--Unset this flag.
    mDontShowHighlight = true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void AdvUICampfire::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Rendering of the base UI. Follows the standard and adds the grid.
    StarlightColor::ClearMixer();

    ///--[Render Pieces]
    //--Render the four sub-components.
    RenderLft(pVisAlpha);
    RenderTop(pVisAlpha);
    RenderRgt(pVisAlpha);
    RenderBot(pVisAlpha);

    ///--[Render Grid]
    //--A grid in the middle indicates the player's selected by both rendering a highlight box over it,
    //  and slightly increasing the icon size.
    RenderGrid(pVisAlpha);

    //--Render the cursor.
    if(!mDontShowHighlight) RenderExpandableHighlight(mHighlightPos, mHighlightSize, 4.0f, AdvImages.rOverlay_Highlight);

    //--Render the name of the selected entry.
    RenderGridEntryName(GetPackageAtCursor(), pVisAlpha);

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Render Help]
    //--Typically overlays other pieces. Also usually slides in from the top but that's up to the individual UI.
    RenderHelp(pVisAlpha);

    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void AdvUICampfire::RenderTop(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders heading.
    float cColorAlpha = AutoPieceOpen(DIR_UP, pVisAlpha);

    ///--[Execution]
    //--Header.
    AdvImages.rOverlay_Header->Draw();

    //--Header text is yellow.
    mColorHeading.SetAsMixerAlpha(cColorAlpha);
    if(!mIsBenchMode)
    {
        AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Campfire Menu");
    }
    else
    {
        AdvImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 5.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Save Bench Menu");
    }
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cColorAlpha);

    //--Help text.
    mShowHelpString->DrawText(0.0f, cxAdvMainlineFontH * 0.0f, SUGARFONT_NOCOLOR, 1.0f, AdvImages.rFont_Mainline);

    ///--[Clean]
    AutoPieceClose();
}
void AdvUICampfire::RenderBot(float pVisAlpha)
{
    ///--[Documentation]
    //--Renders the party information, such as portraits, HP, names, etc.
    AutoPieceOpen(DIR_DOWN, pVisAlpha);

    ///--[Execution]
    //--Footer.
    AdvImages.rOverlay_Footer->Draw();

    ///--[Clean]
    AutoPieceClose();
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
