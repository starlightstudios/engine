///======================================= SteamManager ===========================================
//--Manages the Steam interface. Does nothing if the defines are not set. Most of these parts
//  are from the steam website and duplicated here.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
class CSteamStats;
class WorkshopItem;
#if defined _STEAM_API_
    #include "steam_api.h"
#define _STAT_ID( id,type,name ) { id, type, name, 0, 0, 0, 0 }

enum EStatTypes
{
	STAT_INT = 0,
	STAT_FLOAT = 1,
	STAT_AVGRATE = 2,
};

struct Stat_t
{
	int m_ID;
	EStatTypes m_eStatType;
	const char *m_pchStatName;
	int32 m_iValue;
	float m_flValue;
	float m_flAvgNumerator;
	float m_flAvgDenominator;
};
#endif

///===================================== Local Structures =========================================
///--[SteamAchievement]
typedef struct SteamAchievement
{
    char *mName;
    bool mIsAchieved;
    void Initialize()
    {
        mName = InitializeString("No Name");
        mIsAchieved = false;
    }
    void FreeMemory()
    {
        free(mName);
    }
}SteamAchievement;

///===================================== Local Definitions ========================================
#define STEAM_GAME_INDEX_STRING_TYRANT 0
#define STEAM_GAME_INDEX_STRING_TYRANT_DEMO 1
#define STEAM_GAME_INDEX_PANDEMONIUM 2
#define STEAM_GAME_INDEX_WITCHHUNTERIZANA 3
#define STRING_TYRANT_APPID 1286980
#define STRING_TYRANT_DEMO_APPID 1338120
#define PANDEMONIUM_APPID 1865730
#define WITCHHUNTERIZANA_APPID 2482900

///--[Manual Callback Handling]
//--If true, uses manual callback pumping and registration.
#define STEAM_MANUAL_CALLBACKS

///========================================== Classes =============================================
class SteamManager
{
    private:
    #if defined _STEAM_API_
    //--System
    bool mIsSteamRunning;
    int mTickOfLastStatRequest;
    bool mHasReceivedCurrentStats;
    CSteamStats *mSteamStats;

    //--Steam Achievement Storage
    int mAchievementsTotal;
    SteamAchievement *mAchievements;

    //--Callback Objects
    StarLinkedList *mWorkshopItems; //WorkshopItem *, master

    #endif

    protected:

    public:
    //--System
    SteamManager();
    ~SteamManager();
    void Boot(int pGameID);

    //--Public Variables
    static int xSteamAppID;

    //--Property Queries
    bool IsSteamRunning();

    //--Manipulators
    void RegisterWorkshopItem(WorkshopItem *pItem);

    //--Core Methods
    void AllocateAchievements(int pTotal);
    void SetAchievement(int pIndex, const char *pName);
    void UnlockAchievement(int pIndex);
    void UnlockAchievementS(const char *pName);
    bool RequestStats();

    private:
    //--Private Core Methods
    public:
    //--Update
    void PumpCallbackStack();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static SteamManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Steam_IsSteamGame(lua_State *L);
int Hook_Steam_Activate(lua_State *L);
int Hook_Steam_AllocateAchievements(lua_State *L);
int Hook_Steam_SetAchievement(lua_State *L);
int Hook_Steam_UnlockAchievement(lua_State *L);

