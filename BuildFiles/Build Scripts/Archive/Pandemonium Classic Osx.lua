-- |[ ================================= Pandemonium Classic Build ================================= ]|
--Operating System: OSX
--Library Type: SDL
local sBuildName = "Pandemonium Cls Osx"

--Check existence.
if(BP_Exists(sBuildName) == true) then return end

--Create.
BP_Create(sBuildName)

--Directories.
BP_SetProperty("Core Directory", "") --Engine directory
BP_SetProperty("Build Directory", "../builds/Pandemonium Classic Osx/")

-- |[ =========================================== Common ========================================== ]|
--Every build needs these. These instructions are setup, executables, dll files, dylibs, etc.

-- |[Basics]|
--Set the build directory up. This automatically backs up the previous build.
BP_SetProperty("Register Command", "Setup Build Directory")

-- |[Executables]|
--Executable files.
BP_SetProperty("Register Command", "Copy File", "StarlightEngineSDLOSX", "PandemoniumSDLOSX")

--Dylib files.
BP_SetProperty("Register Command", "Copy Folder", "osxbin", "osxbin")

-- |[Bootstraps and Counters]|
--Bootstrap.
BP_SetProperty("Register Command", "Copy File", "Bootstrap.lua", "Bootstrap.lua")

--Load counters.
BP_SetProperty("Register Command", "Copy File", "LoadCountersAL.lua",  "LoadCountersAL.lua")
BP_SetProperty("Register Command", "Copy File", "LoadCountersSDL.lua", "LoadCountersSDL.lua")

-- |[ ===================================== Base Instructions ===================================== ]|
-- |[Text Files]|
--Relevant changelogs.
BP_SetProperty("Register Command", "Copy File", "Changelog (Classic).txt", "Changelog.txt")

--Credits files.
BP_SetProperty("Register Command", "Copy File", "Credits Classic.txt", "Credits.txt")

--Readme.
BP_SetProperty("Register Command", "Copy File", "Readme Classic.txt", "Readme.txt")

-- |[Configuration Files]|
--Engine and Adventure configurations.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Classic.lua", "Config_Classic.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/Config_Engine.lua",  "Config_Engine.lua")

-- |[Batch Files]|
--For people with audio problems.
BP_SetProperty("Register Command", "Copy File", "BuildFiles/ZBlockAudio Adventure.bat", "ZBlockAudio.bat")--Adventure uses the same exe name as Classic
BP_SetProperty("Register Command", "Copy File", "BuildFiles/ZFullNoVAO.bat",            "ZFullNoVAO.bat")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/ZFullWithVAO.bat",          "ZFullWithVAO.bat")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/ZPotato.bat",               "ZFullWithVAO.bat")

-- |[ ===================================== Engine Subfolders ===================================== ]|
-- |[Data Directory]|
--Copy the data folder.
BP_SetProperty("Register Command", "Copy Folder", "Data", "Data")

--Delete the fonts we don't need.
BP_SetProperty("Register Command", "Delete File", "Data/segoeui.ttf")
BP_SetProperty("Register Command", "Delete File", "Data/segoeuib.ttf")

-- |[Saves Directory]|
--Copy the controls files.
BP_SetProperty("Register Command", "Create Directory", "Saves")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsAL.lua",  "Saves/AdventureControlsAL.lua")
BP_SetProperty("Register Command", "Copy File", "BuildFiles/AdventureControlsSDL.lua", "Saves/AdventureControlsSDL.lua")

-- |[Shaders Directory]|
--Copy the OSX shaders.
BP_SetProperty("Register Command", "Create Directory", "Shaders")
BP_SetProperty("Register Command", "Copy File", "Shaders/ZShaderExec.lua", "Shaders/ZShaderExec.lua")
BP_SetProperty("Register Command", "Copy Folder", "Shaders/OSX", "Shaders/OSX")

-- |[ ====================================== Game Subfolders ====================================== ]|
--[Adventure Directories]|
--Create a "Games/" folder.
BP_SetProperty("Register Command", "Create Directory", "Games")

--Copy all the games over.
BP_SetProperty("Register Command", "Copy Folder", "../classic/Games/ClassicMode",   "Games/ClassicMode")
BP_SetProperty("Register Command", "Copy Folder", "../classic/Games/ClassicMode3D", "Games/ClassicMode3D")
BP_SetProperty("Register Command", "Copy Folder", "../classic/Games/CorrupterMode", "Games/CorrupterMode")

-- |[ ========================================= Clean Up ========================================== ]|
--Pop the active object.
DL_PopActiveObject()
