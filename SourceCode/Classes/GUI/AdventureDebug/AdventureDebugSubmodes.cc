//--Base
#include "AdventureDebug.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "BuildPackage.h"
#include "DialogueTopic.h"
#include "ScriptHunter.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "StarTranslation.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

///=========================================== Update =============================================
void AdventureDebug::UpdateWarp()
{
    //--Warps to various destinations. Has a variable number of entries, so each one is assumed to be the path
    //  to a level. We do not check if the level exists first! Aiee! This is debug, you dope!
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && mMenuCursor >= 0 && mMenuCursor < xWarpDestinationsTotal)
        {
            //--Get the element under consideration:
            WarpListing *rWarpListing = (WarpListing *)rActiveListing->GetElementBySlot(mMenuCursor);
            if(rWarpListing)
            {
                //--Does the warp listing have a submenu? If so, make that the current menu.
                if(rWarpListing->mSubList)
                {
                    //--Store the cursor stuff on the cursor stack:
                    SetMemoryData(__FILE__, __LINE__);
                    CursorStackEntry *nStackEntry = (CursorStackEntry *)starmemoryalloc(sizeof(CursorStackEntry));
                    nStackEntry->mCursor = mMenuCursor;
                    nStackEntry->rActiveList = rActiveListing;
                    mCursorStack->AddElementAsHead("X", nStackEntry, &FreeThis);

                    //--Move to the next list.
                    mMenuCursor = 0;
                    mMenuMax = rWarpListing->mSubList->GetListSize();
                    rActiveListing = rWarpListing->mSubList;
                }
                //--Otherwise, warp to that room.
                else
                {
                    Hide();
                    rActiveLevel->Debug_TransitionTo(rWarpListing->mWarpTarget);
                }
            }
            //--Error:
            else
            {
                Hide();
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Cancel. Closes the menu or goes up one list on the stack.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        //--If there's anything on the cursor stack, go up one level:
        if(mCursorStack->GetListSize() > 0)
        {
            //--Verify the entry:
            CursorStackEntry *rHead = (CursorStackEntry *)mCursorStack->GetHead();
            if(rHead)
            {
                mMenuCursor = rHead->mCursor;
                mMenuMax = rHead->rActiveList->GetListSize();
                rActiveListing = rHead->rActiveList;
            }

            //--Pop.
            mCursorStack->RemoveElementI(0);
        }
        //--Otherwise, go back to the main menu:
        else
        {
            SetToMain();
            mMenuCursor = ADM_OPT_MAIN_OPEN_WARP_SUBMENU;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateScenes()
{
    //--Cutscene menu. Similar to the warp screen, has a variable argument listing. Activates cutscenes
    //  when activated.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && mMenuCursor >= 0 && mMenuCursor < xScenesTotal)
        {
            Hide();
            rActiveLevel->Debug_FireCutscene(xSceneListing[mMenuCursor]);
        }
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_SCENES_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateScriptVars()
{
    //--Script variable editor.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_SCRIPTVARS_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateEnemies()
{
    //--Enemies menu. Remember that this is accessed from the field, so we can wipe out field enemies without
    //  causing dangling pointer errors.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();
    if(!rActiveLevel) { SetToMain(); return; }

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Wipe all field enemies out.
        if(mMenuCursor == ADM_OPT_ENEMIES_WIPE_FIELD)
        {
            rActiveLevel->Debug_WipeFieldEnemies();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Forcibly respawn all field enemies.
        else if(mMenuCursor == ADM_OPT_ENEMIES_RESPAWN_ALL)
        {
            rActiveLevel->Debug_RespawnFieldEnemies();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Toggle Camouflage on or off.
        else if(mMenuCursor == ADM_OPT_ENEMIES_CAMOUFLAGE)
        {
            xCamouflageMode = !xCamouflageMode;
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Toggle always paragons on or off.
        else if(mMenuCursor == ADM_OPT_ENEMIES_ALWAYS_PARAGONS)
        {
            //--Get the variable.
            SysVar *rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Global/Paragons/iAlwaysParagon");
            if(rVariable)
            {
                rVariable->mNumeric = 1.0f - rVariable->mNumeric;
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Toggle paragons sucking on or off.
        else if(mMenuCursor == ADM_OPT_ENEMIES_PARAGONS_SUCK)
        {
            xParagonsSuck = !xParagonsSuck;
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Toggle always showing stats on or off.
        else if(mMenuCursor == ADM_OPT_ENEMIES_ALWAYS_SHOW_STATS)
        {
            //--Get the variable.
            SysVar *rVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Global/Debug/iAlwaysShowStats");
            if(rVariable)
            {
                rVariable->mNumeric = 1.0f - rVariable->mNumeric;
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Close this menu.
        else
        {
            SetToMain();
            mMenuCursor = ADM_OPT_MAIN_OPEN_ENEMIES_SUBMENU;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_ENEMIES_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateParty()
{
    //--Party menu. Refill health, gain XP and money and items, that sort of thing.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Restore the party to full health.
        if(mMenuCursor == ADM_OPT_PARTY_FULL_RESTORE)
        {
            rAdventureCombat->Debug_RestoreParty();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give everyone in the party 200 XP.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_EXPA)
        {
            rAdventureCombat->Debug_AwardXP(200);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give everyone in the party 2000 XP.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_EXPB)
        {
            rAdventureCombat->Debug_AwardXP(2000);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give everyone in the party 20000 XP.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_EXPC)
        {
            rAdventureCombat->Debug_AwardXP(20000);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give the party 1000 Platina.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_CASH)
        {
            rInventory->Debug_AwardPlatina();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give the party 10 of each crafting material.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_CRAFTING)
        {
            rInventory->Debug_AwardCrafting();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give the party 100 JP for their current job.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_JP)
        {
            rAdventureCombat->Debug_AwardJP(1000);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Remove 200 XP.
        else if(mMenuCursor == ADM_OPT_PARTY_LOSE_EXPA)
        {
            rAdventureCombat->Debug_AwardXP(-200);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Remove 2000 XP.
        else if(mMenuCursor == ADM_OPT_PARTY_LOSE_EXPB)
        {
            rAdventureCombat->Debug_AwardXP(-2000);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Remove 20000 XP.
        else if(mMenuCursor == ADM_OPT_PARTY_LOSE_EXPC)
        {
            rAdventureCombat->Debug_AwardXP(-20000);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Close this menu.
        else
        {
            SetToMain();
            mMenuCursor = ADM_OPT_MAIN_OPEN_PARTY_SUBMENU;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_PARTY_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateForms()
{
    //--Similar to the cutscenes, uses the static listing. Closes the menu upon execution.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate") && xFormsTotal > 0)
    {
        //--Build the name.
        char tBuffer[256];
        const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
        sprintf(tBuffer, "%s/FormHandlers/%s.lua", rAdventurePath, xFormListing[mMenuCursor]);
        LuaManager::Fetch()->ExecuteLuaFile(tBuffer);

        //--Hide this menu.
        Hide();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_FORM_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateTopics()
{
    //--Uses the topic listing. Dynamically sized. Left and Right modify
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get the topic listing.
    StarLinkedList *rTopicList = WorldDialogue::Fetch()->GetTopicList();

    //--Left and Right edit the topic values.
    if(rControlManager->IsFirstPress("Left"))
    {
        //--Edit.
        if(mTopicValues[mMenuCursor] > -1)
        {
            mTopicValues[mMenuCursor] --;
            DialogueTopic *rTopic = (DialogueTopic *)rTopicList->GetElementBySlot(mMenuCursor);
            if(rTopic)
            {
                rTopic->SetLevel(mTopicValues[mMenuCursor]);
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    else if(rControlManager->IsFirstPress("Right"))
    {
        //--Edit.
        mTopicValues[mMenuCursor] ++;
        DialogueTopic *rTopic = (DialogueTopic *)rTopicList->GetElementBySlot(mMenuCursor);
        if(rTopic)
        {
            rTopic->SetLevel(mTopicValues[mMenuCursor]);
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_TOPIC_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateScriptHunter()
{
    //--Uses the Script Hunter listing. This is built when the game boots.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--Select the entry and run the Script Hunter.
        const char *rPath = (const char *)xmScriptHunterPathsList->GetElementBySlot(mMenuCursor);
        if(!rPath) return;

        //--Build and run.
        ScriptHunter *tScriptHunter = new ScriptHunter();
        tScriptHunter->SetTargetDirectory(rPath);
        tScriptHunter->Execute();
        delete tScriptHunter;
        AudioManager::Fetch()->PlaySound("Menu|Save");
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_SCRIPT_HUNTER;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateTranslate()
{
    ///--[Documentation]
    //--Updates handling for Translation Mode. This allows the building of a fresh translation file.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Activate]
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Build translation for Monoceros.
        if(mMenuCursor == ADM_BUILD_TRANSLATE_BUILD_MONOCEROS)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Build translation for Monoceros, using an external directory path. Items version.
        else if(mMenuCursor == ADM_BUILD_TRANSLATE_BUILD_ITEMS_MONOCEROSEXTERNAL)
        {
            BuildTranslationFileOnPath("../projectmonoceros/Chapter C/Items/", TRANSLATE_RUN_ITEMSSKILLS, "Translation Item Output.csv");
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Build translation for Monoceros, using an external directory path. Dialogue version.
        else if(mMenuCursor == ADM_BUILD_TRANSLATE_BUILD_DIALOGUE_MONOCEROSEXTERNAL)
        {
            BuildTranslationFileOnPath("../projectmonoceros/Chapter C/", TRANSLATE_RUN_DIALOGUE, "Translation Dialogue Output.csv");
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Makes sure the block and clear tags line up between the original strings and a machine translation.
        else if(mMenuCursor == ADM_BUILD_TRANSLATE_REPAIR_DIALOGUE_MONOCEROSEXTERNAL)
        {
            RunRepairOnDialogueSpreadsheet("../projectmonoceros/Chapter C/Translations/JP Dialogue.csv", "DiaRepair.txt");
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Cancel.
        else if(mMenuCursor == ADM_BUILD_TRANSLATE_CANCEL)
        {
            SetToMain();
            mMenuCursor = ADM_OPT_MAIN_OPEN_TRANSLATION;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    ///--[Cancel]
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_TRANSLATION;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
