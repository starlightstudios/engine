///================================== MonocerosLoadInterrupt ======================================
//--Version of the StarLoadInterrupt built for chapter 1.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"
#include "StarLoadInterrupt.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
#define LI_MONO_LOADFRAMES 5

///========================================== Classes =============================================
class MonocerosLoadInterrupt : public StarLoadInterrupt
{
    private:
    ///--[System]
    ///--[Images]
    //--Filmstrip.
    bool mIsReady;
    StarBitmap *mLoadingFrames[LI_MONO_LOADFRAMES];

    protected:

    public:
    //--System
    MonocerosLoadInterrupt();
    virtual ~MonocerosLoadInterrupt();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators

    //--Core Methods
    virtual void DeallocateImages();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void Call(bool pForceRender);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_LI_BootMonoceros(lua_State *L);
