//--Base
#include "MonoUIQuit.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"
#include "StarLinkedList.h"
#include "StarTranslation.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"
#include "MapManager.h"

///========================================== System ==============================================
MonoUIQuit::MonoUIQuit()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    ///--[Visiblity]
    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== AdvUISample ======= ]
    ///--[System]
    ///--[Images]
    ///--[ ====== MonoUICommon ====== ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== MonoUIQuit ====== ]
    ///--[System]
    ///--[Images]
    memset(&MonoImages, 0, sizeof(MonoImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("MonoImages", &MonoImages, sizeof(MonoImages));
}
MonoUIQuit::~MonoUIQuit()
{
}
void MonoUIQuit::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    ResolveSeriesInto("Monoceros Quit UI",           &MonoImages, sizeof(MonoImages));
    ResolveSeriesInto("Adventure Quit UI Monoceros", &AdvImages,  sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",           &HelpImages, sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///--[Public Statics]
//--Static reference to translatable string lookups.
MonoUIQuit_Strings *MonoUIQuit::xrStringData = NULL;

///======================================== Translation ===========================================
MonoUIQuit_Strings::MonoUIQuit_Strings()
{
    //--Local name: "MonoUIQuit"
    MonoUIQuit::xrStringData = this;

    //--Strings
    str.mTextQuitToTitle = InitializeString("MonoUIQuit_mQuitToTitle"); //"Quit to title?"
    str.mTextCancel      = InitializeString("MonoUIQuit_mCancel");      //"Cancel"
    str.mTextToTitle     = InitializeString("MonoUIQuit_mToTitle");     //"To Title"
}
MonoUIQuit_Strings::~MonoUIQuit_Strings()
{
    for(int i = 0; i < MONOUIQUIT_STRINGS_TOTAL; i ++) free(mem.mPtr[i]);
}
int MonoUIQuit_Strings::GetSize()
{
    return MONOUIQUIT_STRINGS_TOTAL;
}
void MonoUIQuit_Strings::SetString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= MONOUIQUIT_STRINGS_TOTAL) return;
    ResetString(mem.mPtr[pSlot], pString);
}

///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
///======================================= Core Methods ===========================================
void MonoUIQuit::RecomputeCursorPositions()
{
    ///--[Documentation]
    //--There are only two positions for the cursor here so this is pretty easy to manage.
    if(mCursor == cxMonoMenuVal_Cancel)
    {
        mHighlightPos.MoveTo( 449.0f, 253.0f, cMonoCurTicks);
        mHighlightSize.MoveTo(173.0f,  51.0f, cMonoCurTicks);
    }
    else if(mCursor == cxMonoMenuVal_ToTitle)
    {
        mHighlightPos.MoveTo( 698.0f, 253.0f, cMonoCurTicks);
        mHighlightSize.MoveTo(266.0f,  51.0f, cMonoCurTicks);
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool MonoUIQuit::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[Documentation]
    //--Handles timers and player input for the quit submenu. If this submenu handled input, returns
    //  true, otherwise returns false.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Quit Fadeout]
    //--When in quit mode, fade to black, then quit or return to the title. This overrides all other
    //  actions and controls.
    if(mIsQuitting)
    {
        //--Timer.
        mQuitFadeTimer ++;

        //--Ending.
        if(mQuitFadeTimer >= ADVMENU_QUIT_FINAL_TICKS)
        {
            MapManager::Fetch()->mBackToTitle = true;
        }
        return true;
    }

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mVisibilityTimer < mVisibilityTimerMax) mVisibilityTimer ++;

    //--Easing packages.
    mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mHighlightSize.Increment(EASING_CODE_QUADOUT);

    ///--[Control Handling]
    //--This object has no sub-modes and very little control handling.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Left.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Set.
        if(mCursor != cxMonoMenuVal_Cancel)
        {
            mCursor = cxMonoMenuVal_Cancel;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Right.
    else if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Set.
        if(mCursor != cxMonoMenuVal_ToTitle)
        {
            mCursor = cxMonoMenuVal_ToTitle;
            RecomputeCursorPositions();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Activate. Cancels or returns to title.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Cancel:
        if(mCursor == cxMonoMenuVal_Cancel)
        {
            FlagExit();
        }
        //--To-title.
        else if(mCursor == cxMonoMenuVal_ToTitle)
        {
            mIsQuitting = true;
        }

        //--Common.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return true;
    }

    //--Cancel. Return to previous menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        FlagExit();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }

    ///--[Finish Up]
    //--We handled the update.
    return true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void MonoUIQuit::RenderPieces(float pVisAlpha)
{
    ///====================== Documentation and Setup =========================
    //--Render nothing if everything is offscreen.
    if(mVisibilityTimer < 1) return;

    //--Option. Selects between fading and sliding.
    bool tUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");

    ///--[Timer]
    //--Resolve timer offset.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, (float)ADVMENU_QUIT_VIS_TICKS);

    ///--[Setup]
    //--If the UI is using translate logic, set the alpha to 1.0f.
    if(!tUseFade) cAlpha = 1.0f;

    //--Reset mixer.
    StarlightColor::ClearMixer();

    ///============================= Left Block ===============================
    ///============================= Top Block ================================
    //--Offset.
    AutoPieceOpen(DIR_UP, cAlpha);

    //--Backing.
    MonoImages.rFrame_Main->Draw();

    //--Buttons switch which one draws based on the cursor.
    if(mCursor == cxMonoMenuVal_Cancel)
    {
        MonoImages.rButton_CancelSel->Draw();
        MonoImages.rButton_ToTitle->Draw();
    }
    else if(mCursor == cxMonoMenuVal_ToTitle)
    {
        MonoImages.rButton_Cancel->Draw();
        MonoImages.rButton_ToTitleSel->Draw();
    }

    //--Header text is yellow.
    mMonoTitleColor.SetAsMixerAlpha(cAlpha);
    MonoImages.rFont_DoubleHeading->DrawText(VIRTUAL_CANVAS_X * 0.50f, 166.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextQuitToTitle);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Back-to-Title
    MonoImages.rFont_Heading->DrawText(536.0f, 253.0f - 2.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextCancel);

    //--Exit Program
    MonoImages.rFont_Heading->DrawText(831.0f, 253.0f - 2.0f, SUGARFONT_AUTOCENTER_X, 1.0f, xrStringData->str.mTextToTitle);

    //--Clean.
    AutoPieceClose();

    ///============================ Right Block ===============================
    ///============================ Bottom Block ==============================
    ///============================ No Blocking ===============================
    ///============================= Finish Up ================================
    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();

    ///=============================== Finale =================================
    if(mQuitFadeTimer > 0)
    {
        //--Fade percent.
        float cFadeAlpha = EasingFunction::QuadraticInOut(mQuitFadeTimer, ADVMENU_QUIT_FINAL_TICKS);
        StarBitmap::DrawFullBlack(cFadeAlpha);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
