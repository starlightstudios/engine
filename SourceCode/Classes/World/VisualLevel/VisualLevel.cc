//--Base
#include "VisualLevel.h"

//--Classes
#include "Actor.h"
#include "ContextMenu.h"
#include "FlexMenu.h"
#include "StarBitmap.h"
#include "VisualRoom.h"
#include "WADFile.h"
#include "ZoneEditor.h"
#include "ZoneNode.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "EasingFunctions.h"
#include "DeletionFunctions.h"
#include "Global.h"
#include "HitDetection.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "CameraManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "EntityManager.h"
#include "StarLumpManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "OptionsManager.h"

///--[Debug]
//#define VL_DEBUG
#ifdef VL_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--Disallow saving. Used so the user (or the coder...) doesn't accidentally try to save out the incomplete
//  data from a precached file. Only turn this off if you're rebuilding level geometry and lighting.
#define VL_DISALLOW_SAVES true

///========================================== System ==============================================
VisualLevel::VisualLevel()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_VISUALLEVEL;

    ///--[IRenderable]
    //--System

    ///--[RootLevel]
    //--System
    ResetString(mName, "Unnamed VisualLevel");

    ///--[PandemoniumLevel]
    //--System
    //--Construction
    //--Final Layout
    //--Virtual Buttons
    //--Console Strings
    //--Status Strings
    //--Context Menu
    //--Character Pane
    //--Inventory Command
    //--Entity Pane
    //--Menu Activity Stack
    //--Rendering

    ///--[VisualLevel]
    //--System
    mDisallowSaving = VL_DISALLOW_SAVES;
    mReloadPath = NULL;
    mMustRepositionPlayer = false;

    //--Overall Map
    mOvermap = NULL;

    //--WAD Compilation
    mGeometrySavePath = NULL;
    mLocalWADFile = NULL;

    //--Zone Data
    mSaveDataPath = NULL;
    mZoneEditor = new ZoneEditor();

    //--Display
    mIsShowingMap = false;
    cComparisonPoints[0] = 500.0f;
    cComparisonPoints[1] = 125.0f;
    cComparisonPoints[2] = 500.0f;
    cComparisonPoints[3] = 640.0f;
    cComparisonPoints[4] = 860.0f;
    cComparisonPoints[5] = 380.0f;
    cComparisonPoints[6] = 160.0f;
    cComparisonPoints[7] = 380.0f;

    //--Player Stuff
    mStopMovingAfterTurn = false;
    mIsFreeMove = false;
    mPlayerX = 0.0f;
    mPlayerY = 0.0f;
    mPlayerZ = 0.0f;
    mPlayerNode = 1;
    mPlayerMoveSpeed = PLAYER_BASE_MOVE_SPEED;
    mPlayerZSpeed = 0.0f;
    mPlayerFacing = 0;
    memset(mNavigationAlphas, 0, sizeof(float) * NAV_ALPHAS_TOTAL);

    //--Entities
    mEntitySwitchTimer = 0;
    mNewTurnTimer = 0;
    mSelected3DEntity = 0;
    mSelected3DEntityPrevious = 0;
    mStoredEntityCount = 0;
    mRefreshClickPoints = 2;
    mSlotsMoving = 0;
    memset(mClickPoints, 0, sizeof(TwoDimensionReal) * VL_ENTITIES_PER_SCREEN);
    mExtraRenderEntitiesList = new StarLinkedList(true);

    //--Entity Rendering
    mInitialRotation = 0.0f;
    mCurrentRotation = 0.0f;
    memset(mUnifiedRenderListing, 0, sizeof(UnifiedObjectPack) * VL_ENTITIES_PER_SCREEN);

    //--Mouselook
    mOldMouseX = -100.0f;
    mOldMouseY = -100.0f;
    mKeyboardRotationTimer = 0;
    mKeyboardRotationClamp = 5;
    mKeyboardRotationSpeed = 3.0f;

    //--Node Movement Turning
    mRecheckFacing = false;
    mProceedImmediately = false;
    mIsTurning = 0.0f;
    mIsTurningRight = false;
    mLookingTimer = 0.0f;
    mStartLookZ = 0.0f;
    mEndLookZ = 0.0f;

    //--Teleportation
    mIsTeleporting = false;
    mTeleportTimer = 0;
    mTeleportStart.Set(0.0f, 0.0f, 0.0f);
    mTeleportEnd.Set(0.0f, 0.0f, 0.0f);

    //--Node Movement Moving
    mMoveToNodeID = 0;
    mTicksMovingSoFar = 0;
    mTicksSlowingDown = 0;
    mLookingFlag = 0;
    rPreviousRoom = NULL;

    //--UI Rendering
    mItemOffset = 0;
    mItemsRendered = 2;
    mHightlightedRoom = -1;
    mPreviousScreenW = -1.0f;
    mPreviousScreenH = -1.0f;
    mConsoleOffsetTimer = 0;
    mConsoleOffsetDelta = 0;
    mConsoleOffsetY = 200.0f;
    mPreviousZ = 0;

    //--UI Constants
    mDontRepositionOnWorldRender = false;
    mLastRenderScale = 1.0f;
    mLastRenderX = 0.0f;
    cScreenTextScale = 1.25f;
    memset(mBorderSizes, 0, sizeof(TwoDimensionReal) * 9);
    mRadarSize.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    mSelfMenuBtnSize.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    mItemScrollUpBtn.SetWH(-1000.0f, -1000.0f, 1.0f, 1.0f);
    mItemScrollDnBtn.SetWH(-1000.0f, -1000.0f, 1.0f, 1.0f);
    mSkipTurnBtn.SetWH(-1000.0f, -1000.0f, 1.0f, 1.0f);

    //--Self-Menu Thievery
    mSelfMenuDimensions.Set(-1000.0f, -1000.0f, 1.0f, 1.0f);
    mStoredSelfMenu = NULL;

    //--Character Card Positions
    mCharacterPortraitDim.Set(0.0f, 0.0f, 1.0f, 1.0f);
    mInventoryDim.Set(0.0f, 0.0f, 1.0f, 1.0f);
    mPortraitRemapList = new StarLinkedList(true);

    //--Story Display
    mShowStoryDisplay = false;
    mStoryConsoleWaitForKeypress = false;
    mStoryFadeTimer = VL_CONSOLE_FADE_TICKS;
    rCurrentStoryBitmap = NULL;
    mStoryConsoleStrings = new StarLinkedList(true);
    mStoryConsoleFadingString = new StarLinkedList(true);

    //--Images
    memset(&Images, 0, sizeof(Images));

    ///--[Protected Variable Overrides]
    //--Color Constants
    cBorderColor.SetRGBAI(0, 0, 0, 64);
    cInnerColor.SetRGBAI(0, 0, 0, 192);

    //--Console size
    float cConsolePaneHeight = 250.0f;
    ConsolePane.cTop = VIRTUAL_CANVAS_Y - cConsolePaneHeight;
    ConsolePane.cHei = cConsolePaneHeight;
    mConsoleOffsetY = cConsolePaneHeight;

    //--Scaling
    cUseScale = WAD_TO_VR_SCALE * -1.0f;
    cMapViewFactorX = 0.45f;
    cMapViewFactorY = 0.45f;

    ///--[Overrides]
    ConsolePane.cIndent = 3.0f;
    ConsolePane.cLft = 0.0f;
    ConsolePane.cTop = VIRTUAL_CANVAS_Y - cConsolePaneHeight;
    xMapZoom = 1.5f;

    ///--[Image Resolve]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Images.Data.rBorderCard    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/3DUI/CardBorder");
    Images.Data.rSelfMenuBtn   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/3DUI/SelfMenuBtn");
    Images.Data.rSystemMenuBtn = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/3DUI/SystemMenuBtn");
    Images.Data.rWeaponIcon    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/Weapon");
    Images.Data.rArmorIcon     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/Armor");
    Images.Data.rSkipTurnBtn   = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/3DUI/SkipTurnBtn");
    Images.Data.rHitOverlay    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/3DUI/HitOverlay");
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(StarBitmap *));

    ///--[UI Setup]
    SetUIConstants();

    ///--[Options]
    //--Modify values as set by the OptionsManager.
    float tWalkSpeedFactor = OptionsManager::Fetch()->GetOptionF("WalkSpeedFactor");
    if(tWalkSpeedFactor != OM_ERROR_FLOAT)
    {
        if(tWalkSpeedFactor < 0.50f) tWalkSpeedFactor = 0.50f;
        if(tWalkSpeedFactor > 3.50f) tWalkSpeedFactor = 3.50f;
        mPlayerMoveSpeed = mPlayerMoveSpeed * tWalkSpeedFactor;
    }
}
VisualLevel::~VisualLevel()
{
    if(xrActiveWADFile == mLocalWADFile) xrActiveWADFile = NULL;
    free(mReloadPath);
    free(mSaveDataPath);
    free(mGeometrySavePath);
    delete mStoredSelfMenu;
    delete mZoneEditor;
    delete mLocalWADFile;
    delete mOvermap;
    delete mPortraitRemapList;
    delete mExtraRenderEntitiesList;
    delete mStoryConsoleStrings;
    delete mStoryConsoleFadingString;
}

///--[Public Statics]
VisualLevel *VisualLevel::xrConstructionLevel = NULL;
WADFile *VisualLevel::xrActiveWADFile = NULL;

//--If set to true, the radar will rotate as the player rotates their facing. Thus, what is above them
//  on the radar is always what's directly in front of them.
bool VisualLevel::xRadarRotatesWithPlayer = true;

//--Because the radar rotating by 1 degree can make a straight line look jagged, we can set the radar to
//  only rate in certain steps. Set this to 0.0f to turn off incremental rotation. This does not take
//  place while the player is turning, though, to keep the turns smooth.
float VisualLevel::xRadarRotateIncrement = 3.0f;

//--When entities render, they normally render in front of the player's viewspace. If this flag is set
//  to true, entities will render in fixed 3D coordinate rotation, and rotating can remove all entities from view.
//  Entities will still default to rendering in front of the player when a new room is entered.
bool VisualLevel::xRenderEntitiesInWorldRotation = true;

//--Shows a larger UI on the right side of the screen. Obscures more of the screen but shows more character details.
bool VisualLevel::xUseAlternateUI = false;

//--Shows a red/violet overlay on the edges of the screen when the player is damaged. Set by the OptionsManager.
bool VisualLevel::xAllowDamageOverlay = false;

///--[Private Statics]
bool VisualLevel::xMustBuildConstants = true;
float VisualLevel::cxPositions[VL_ENTITIES_PER_SCREEN];

///===================================== Property Queries =========================================
bool VisualLevel::IsOfType(int pType)
{
    //--VisualLevel is also of PandemoniumLevel type.
    if(pType == POINTER_TYPE_PANDEMONIUMLEVEL) return true;
    return (pType == mType);
}
bool VisualLevel::IsMoving()
{
    if(mIsTurning) return true;
    if(mMoveToNodeID) return true;
    if(mProceedImmediately) return true;
    return false;
}
bool VisualLevel::IsEntityOnExtraRenderList(uint32_t pID)
{
    if(!pID) return false;

    ExtraEntityRenderPack *rCheckPack = (ExtraEntityRenderPack *)mExtraRenderEntitiesList->PushIterator();
    while(rCheckPack)
    {
        if(rCheckPack->mParentID == pID)
        {
            mExtraRenderEntitiesList->PopIterator();
            return true;
        }
        rCheckPack = (ExtraEntityRenderPack *)mExtraRenderEntitiesList->AutoIterate();
    }

    return false;
}
int VisualLevel::GetFixedTextWidth(const char *pString)
{
    if(!Images.mIsReady) return 1;
    return PandemoniumLevel::Images.Data.rUIFontMedium->GetTextWidth(pString);
}

///======================================= Manipulators ===========================================
void VisualLevel::SetReloadPath(const char *pPath)
{
    ResetString(mReloadPath, pPath);
}
void VisualLevel::BuildSkybox(const char *pPattern)
{
    if(!pPattern || !mLocalWADFile) return;
    mLocalWADFile->BuildSkyboxPattern(pPattern);
}
void VisualLevel::SetSavePath(const char *pPath)
{
    //--Note: NULL is legal.
    ResetString(mSaveDataPath, pPath);
    mZoneEditor->SetSavePath(mSaveDataPath);
}
void VisualLevel::SetGeometryPath(const char *pPath)
{
    //--Note: NULL is legal.
    ResetString(mGeometrySavePath, pPath);
}
void VisualLevel::RegisterExtraEntityPack(ExtraEntityRenderPack *pPack)
{
    //--Note: Takes ownership of the pack.
    mExtraRenderEntitiesList->AddElement("X", pPack, &FreeThis);
}
void VisualLevel::RebuildSelfMenu()
{
    //--Forces the self-menu to rebuild.
    delete mStoredSelfMenu;
    mStoredSelfMenu = NULL;
    if(mSelfMenuPopulationScript)
    {
        mStoredSelfMenu = new FlexMenu();
        LuaManager::Fetch()->PushExecPop(mStoredSelfMenu, mSelfMenuPopulationScript);
    }
}

///======================================= Core Methods ===========================================
void VisualLevel::HandlePlayerReposition()
{
    ///--[Documentation]
    //--If the ZoneEditor moved the player, read the position from the camera. This can be called
    //  in multiple places since the ZoneEditor can be updated during the game or during loading.
    //--This can also be called during gameplay as the player moves around, since their internal
    //  node position is updated externally.

    ///--[Editing Case]
    //--Called if the game is not initialized and we're probably editing nodes.
    if(!mMustRepositionPlayer && mZoneEditor && mZoneEditor->mHasMovedPlayer)
    {
        //--Unset the flag to indicate we read it.
        mZoneEditor->mHasMovedPlayer = false;

        //--Get the position from the camera.
        float tCamX, tCamY, tCamZ;
        float tXRot, tYRot, tZRot;
        StarCamera3D *rActiveCamera = CameraManager::FetchActiveCamera3D();
        rActiveCamera->Get3DPosition(tCamX, tCamY, tCamZ);
        rActiveCamera->Get3DRotation(tXRot, tYRot, tZRot);

        //--Set.
        mPlayerX =  tCamX;
        mPlayerY = -tCamY;
        mPlayerZ =  tCamZ;

        //--Figure out the closest node and store that. It might be NULL!
        ZoneNode *rClosestNode = mZoneEditor->GetNodeNearestTo(-mPlayerX, -mPlayerY);
        if(rClosestNode)
        {
            //--Move the player to this exact position.
            if(!mIsFreeMove)
            {
                mPlayerX = rClosestNode->GetX();
                mPlayerY = rClosestNode->GetY() * -1.0f;
            }

            //--Store its ID.
            mPlayerNode = rClosestNode->GetNodeID();

            //--Store the facing.
            FacingInfo *rInfo = rClosestNode->GetNearestFacingByAngle(tZRot);
            if(rInfo)
            {
                rActiveCamera->SetRotation(rInfo->mXRotation, rInfo->mYRotation, rInfo->mZRotation);
                mPlayerFacing = rClosestNode->GetSlotOfFacing(rInfo);
            }

            //--Attempt to resolve the Z.
            if(mLocalWADFile) mPlayerZ = mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, mPlayerZ);
        }

        //--Set the camera position.
        rActiveCamera->SetPosition(-mPlayerX, -mPlayerY, -(mPlayerZ + PLAYER_HEIGHT));
    }
    ///--[Game Case]
    //--Called if the game is running. The player, who should have been set by the EM as the last
    //  acting Actor, will need to be reposition.
    else if(mMustRepositionPlayer)
    {
        //--Flag.
        if(mZoneEditor) mZoneEditor->mHasMovedPlayer = false;

        //--Make sure the EM has set the Actor. If not, ignore it for now.
        Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
        if(!rPlayerCharacter) return;

        //--Unset the flag.
        mMustRepositionPlayer = false;

        //--Move to the Player's room. We can find it by its node ID.
        PandemoniumRoom *rPlayerRoom = rPlayerCharacter->GetLocation();
        if(rPlayerRoom && rPlayerRoom->IsOfType(POINTER_TYPE_VISUAL_ROOM))
        {
            //--Cast.
            VisualRoom *rVisualRoom = (VisualRoom *)rPlayerRoom;

            //--Find the node.
            uint32_t tNodeID = rVisualRoom->GetAssociatedNodeID();
            ZoneNode *rNode = GetNodeByID(tNodeID);
            if(!rNode) return;

            //--Everything checked out, reposition.
            mPlayerX = rNode->GetX();
            mPlayerY = rNode->GetY() * -1.0f;

            //--Store its ID.
            mPlayerNode = rNode->GetNodeID();

            //--Store the facing.
            FacingInfo *rInfo = rNode->GetNearestFacingByAngle(0.0f);
            if(rInfo)
            {
                CameraManager::FetchActiveCamera3D()->SetRotation(rInfo->mXRotation, rInfo->mYRotation, rInfo->mZRotation);
                mPlayerFacing = rNode->GetSlotOfFacing(rInfo);
            }

            //--Attempt to resolve the Z.
            if(mLocalWADFile) mPlayerZ = mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, mPlayerZ);
            CameraManager::FetchActiveCamera3D()->SetPosition(-mPlayerX, -mPlayerY, -(mPlayerZ + PLAYER_HEIGHT));

            //--Store this room.
            rPreviousRoom = rPlayerRoom;

            //--Compute and store how many entities are visible. We look at the middle entity by default, rounded down.
            //  If there are no entities, 0 is a safe value.
            mEntitySwitchTimer = 0;
            mStoredEntityCount = CountVisibleEntities();
            mSelected3DEntity = mStoredEntityCount / 2;
            mSelected3DEntityPrevious = mSelected3DEntity;
        }
    }
}
void VisualLevel::HandlePlayerCommandOverride()
{
    ///--[Documentation]
    //--This will be called whenever a player-controlled entity attempts to perform an action, but
    //  an AI or override script forced them to do something else. Since this "something else" can
    //  include moving, we have to handle movements here.
    //--Note that the command override does not care if the Actor can move to the target.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerCharacter) return;

    //--Check what room the player is in. If it's not the node we expect, then move them there.
    PandemoniumRoom *rPlayerRoom = rPlayerCharacter->GetLocation();
    if(rPlayerRoom && rPlayerRoom->IsOfType(POINTER_TYPE_VISUAL_ROOM))
    {
        //--Cast.
        VisualRoom *rVisualRoom = (VisualRoom *)rPlayerRoom;
        uint32_t tPlayerRoomID = rVisualRoom->GetAssociatedNodeID();

        //--Check.
        if(tPlayerRoomID != mPlayerNode)
        {
            //--Walk through the node list backwards in case this is a transition node.
            uint32_t tNearestNode = VisualRoom::FindFirstID(mPlayerNode, tPlayerRoomID, this);
            if(tNearestNode)
            {
                StartMovementTo(tNearestNode);
            }
        }
    }
}
void VisualLevel::HandleNewTurn()
{
    //--Called at the end of a turn. Modifies a few timers.
    mNewTurnTimer = 0;

    //--Call the base class version.
    PandemoniumLevel::HandleNewTurn();

    //--Delete the self-menu, as it may have changed.
    delete mStoredSelfMenu;
    mStoredSelfMenu = NULL;
    if(mSelfMenuPopulationScript)
    {
        mStoredSelfMenu = new FlexMenu();
        LuaManager::Fetch()->PushExecPop(mStoredSelfMenu, mSelfMenuPopulationScript);
    }

    //--Reference Actor.
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerActor) return;

    //--Store the rotation for entities offsetting.
    if(rPlayerActor->GetLocation() != rPreviousRoom)
    {
        //--Store.
        rPreviousRoom = rPlayerActor->GetLocation();

        float tDummyX, tDummyY;
        float tCamX, tCamY, tCamZ;
        CameraManager::FetchActiveCamera3D()->Get3DPosition(tCamX, tCamY, tCamZ);
        CameraManager::FetchActiveCamera3D()->Get3DRotation(tDummyX, tDummyY, mInitialRotation);
        mInitialRotation = mInitialRotation + 90.0f;
        mRefreshClickPoints = 2;

        mStoredEntityCount = CountVisibleEntities();
        mSelected3DEntity = mStoredEntityCount / 2;
        mSelected3DEntityPrevious = mSelected3DEntity;

        //--Did the player change which room they're in? If so, face the center entity.
        //--Deprecated, the teleport check does this job a lot better.
        if(false)
        {
            //--Update.
            rPreviousRoom = rPlayerActor->GetLocation();

            //--Teleport check. If the player's X/Y are not at the node in question, they might have teleported there.
            //  If that happens, we need to reposition the X/Y and camera.
            VisualRoom *rPreviousVisualRoom = (VisualRoom *)rPreviousRoom;
            if(rPreviousVisualRoom && rPreviousVisualRoom->IsOfType(POINTER_TYPE_VISUAL_ROOM))
            {
                //--Get the node associated with this room.
                uint32_t tNewRoomNode = rPreviousVisualRoom->GetAssociatedNodeID();
                ZoneNode *rNewNode = GetNodeByID(tNewRoomNode);
                VisualRoom *rAssociatedRoom = GetRoomByNodeID(tNewRoomNode);

                //--If it wasn't the same as the old one, we need to reposition.
                if(tNewRoomNode != mPlayerNode && rNewNode)
                {
                    //--Flags.
                    mPlayerNode = tNewRoomNode;
                    mPlayerFacing = 0;

                    //--Resolve position.
                    mPlayerX = rNewNode->GetX();
                    mPlayerY = rNewNode->GetY() * -1.0f;
                    if(mLocalWADFile) mPlayerZ = mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, rNewNode->GetZ());

                    //--Set the camera position.
                    CameraManager::FetchActiveCamera3D()->SetPosition(-mPlayerX, -mPlayerY, -(mPlayerZ + PLAYER_HEIGHT));

                    //--Debug.
                    fprintf(stderr, "Player teleported to %i %s\n", mPlayerNode, rAssociatedRoom->GetName());
                }
            }
        }
    }
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void VisualLevel::Update()
{
    ///--[Documentation]
    //--Entry point for the VisualLevel's update cycle. No longer incorporates anything from the
    //  original update cycle.
    //--The update cycle branches off into the UI update and the Node/Freelook update. These can
    //  be modified using hotkeys.
    DebugPush(false, "VisualLevel: Begin Update.\n");

    ///--[Shader Swap]
    ControlManager *rControlManager = ControlManager::Fetch();
    if(rControlManager->IsFirstPress("Shader0"))
    {
        mLocalWADFile->SetShaderCode(0);
    }
    else if(rControlManager->IsFirstPress("Shader1"))
    {
        mLocalWADFile->SetShaderCode(1);
    }
    else if(rControlManager->IsFirstPress("Shader2"))
    {
        mLocalWADFile->SetShaderCode(2);
    }

    ///--[Timers]
    //--Console sliding.
    mConsoleOffsetTimer += mConsoleOffsetDelta;
    if(mConsoleOffsetDelta < 0 && mConsoleOffsetTimer < 0)
    {
        mConsoleOffsetTimer = 0;
        mConsoleOffsetDelta = 0;
    }
    else if(mConsoleOffsetDelta > 0 && mConsoleOffsetTimer > VL_CONSOLE_SLIDE_TICKS)
    {
        mConsoleOffsetTimer = VL_CONSOLE_SLIDE_TICKS;
        mConsoleOffsetDelta = 0;
    }

    //--Check for repositioning.
    DebugPrint("Repositioning player.\n");
    HandlePlayerReposition();

    //--Damage update. Locks the rest of the update out.
    if(IsDamageAnimating())
    {
        UpdateDamageAnimation();
        return;
    }

    //--If in story display, that handles the rest of the update.
    if(mShowStoryDisplay)
    {
        UpdateStoryMode();
        return;
    }

    //--If people are fading, don't do anything.
    if(mExtraRenderEntitiesList->GetListSize() > 0 && !IsMoving())
    {
        DebugPop("VisualLevel: Objects are fading, update completes.\n");
        return;
    }

    //--Entity switching. If entities are switching then the update is locked out, since we don't
    //  want the player to try to click a moving entity that might move offscreen and delete itself.
    if(mEntitySwitchTimer > 0)
    {
        mEntitySwitchTimer --;
        return;
    }
    if(mEntitySwitchTimer < 0)
    {
        mEntitySwitchTimer ++;
        return;
    }

    //--If the entities changed clamp counts, swing to the middle of the visible entity grouping. This doesn't happen
    //  if there are exactly zero entities since there's no need to slow things down.
    if((mSelected3DEntity < 0 || mSelected3DEntity >= mStoredEntityCount) && CountVisibleEntities() >= 1)
    {
        //--Store what the previous entity was.
        mSelected3DEntityPrevious = mSelected3DEntity;

        //--Set the target.
        mStoredEntityCount = CountVisibleEntities();
        mSelected3DEntity = mStoredEntityCount / 2;

        if(mSelected3DEntity < 0)
            mEntitySwitchTimer = VL_ENTITY_SLIDE_TICKS;
        else
            mEntitySwitchTimer = -VL_ENTITY_SLIDE_TICKS;

        return;
    }

    ///--[WADFile]
    //--If the WADFile exists, update its internal timers.
    DebugPrint("Updating WAD file.\n");
    if(mLocalWADFile)
    {
        //--Run the update.
        mLocalWADFile->Update();
    }

    //--Update rooms if they're animating something.
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        mRooms[i]->UpdatePulse();
    }

    ///--[UI Subroutine]
    //--Run the UI update. This does most of the things that the parent class did. If the function
    //  returns true, then ignore the rest of the hotkeys.
    DebugPrint("Running UI update.\n");
    if(HandleUIUpdate())
    {
        DebugPop("VisualLevel: Update is complete, UI subroutine handled it.\n");
        return;
    }

    ///--[Keyboard Input]
    //--Setup.

    //--Toggle free movement on or off.
    DebugPrint("Checking movement toggle.\n");
    if(rControlManager->IsFirstPress("ToggleFreeMovement"))
    {
        //--Toggle.
        mIsFreeMove = !mIsFreeMove;

        //--Lock the mouse when going into free movement mode.
        DisplayManager *rDisplayManager = DisplayManager::Fetch();
        if(mIsFreeMove && !rDisplayManager->IsMouseLocked())
        {
            rDisplayManager->LockMouse();
            rDisplayManager->HideHardwareMouse();
        }
        //--Unlock the mouse when going into node movement.
        else if(!mIsFreeMove && DisplayManager::Fetch()->IsMouseLocked())
        {
            rDisplayManager->UnlockMouse();
            rDisplayManager->ShowHardwareMouse();
        }
    }

    //--Handle movement, if there is any.
    DebugPrint("Handling free movement.\n");
    if(mIsFreeMove)
    {
        //--Basic.
        DebugPrint("Running movement subroutine.\n");
        HandleMovement();

        //--Hotkey: Quickly create a node at the current position.
        if(rControlManager->IsFirstPress("Quick Node"))
        {
            DebugPrint("Placing quick node.\n");
            mZoneEditor->ExecuteCommand(ZE_FUNCTION_NEW_NODE);
            mZoneEditor->ExecuteCommand(ZE_FUNCTION_NODE_SET_POSITION);
        }
    }
    //--Movement is handled by the VisualRoom transition logic.
    else
    {
        //--Make sure the xrActiveWADFile is still correct. The rooms need this for their update.
        xrActiveWADFile = mLocalWADFile;

        //--Run the update. This is in VisualLevelNodeMovement.cc.
        DebugPrint("Running node movement subroutine.\n");
        HandleNodeMovementUpdate();
    }

    ///--[Hotkeys]
    //--Hotkey to run the lights builder. Saving must be allowed.
    if(rControlManager->IsFirstPress("Build Lighting") && !mDisallowSaving)
    {
        DebugPrint("Ordering engine to build lights.\n");
        mLocalWADFile->BuildLighting();
    }

    //--Hotkey to reload the level. Saving must be allowed.
    if(rControlManager->IsFirstPress("Reload Level") && mReloadPath && !mDisallowSaving)
    {
        DebugPrint("Resetting level path. MapManager will reload at end of tick.\n");
        ResetString(MapManager::Fetch()->mReloadAtEndOfTick, mReloadPath);
    }

    //--Hotkey to save the level data to a compiled file. Saving must be allowed.
    if(rControlManager->IsFirstPress("Save Level Data") && mGeometrySavePath && mLocalWADFile && !mDisallowSaving)
    {
        fprintf(stderr, "ENGINE WRITE.\n");
        DebugPrint("Ordering engine to save geometry.\n");
        mLocalWADFile->SaveToFile(mGeometrySavePath);
    }

    //--Hotkey to load the level data from a compiled file.
    if(rControlManager->IsFirstPress("Load Level Data"))
    {
        DebugPrint("Ordering engine to load geometry.\n");
        LoadGeometryNow();
    }

    ///--[Finish Up]
    //--Debug.
    DebugPop("VisualLevel: Update is complete.\n");
}
void VisualLevel::LoadNodesNow()
{
    if(!mZoneEditor || !mSaveDataPath) return;
    mZoneEditor->LoadFromFile(mSaveDataPath);
    HandlePlayerReposition();
    mIsFreeMove = false;
}
void VisualLevel::LoadGeometryNow()
{
    if(!mGeometrySavePath || !mLocalWADFile) return;
    mLocalWADFile->LoadFromFile(mGeometrySavePath);
}

///========================================= File I/O =============================================
void VisualLevel::ConstructWithFile(const char *pPath)
{
    //--Overload of below, constructs with just the base file.
    ConstructWithFile(pPath, 0, NULL);
}
void VisualLevel::ConstructWithFile(const char *pPath, int pAdditionalPaths, const char **pPaths)
{
    //--Given the path to a file, opens it and any additional WAD files using the overwrite mode.
    //  Pay attention to the load order!
    //--At least one path is specified, which is the base WAD. It is legal to pass 0 for pAdditionalPaths.
    //--If the path is NULL, the WADFile will be created but will contain no data.

    //--Debug.
    DebugPush(true, "VisualLevel: Constructing with file %s.\n", pPath);

    //--The first file should be the base WAD. We don't respect the IWAD/PWAD disctinction in this
    //  engine, but if you were going to, this is where you'd do that.
    mLocalWADFile = new WADFile();
    DebugPrint("Booted new WADFile.\n", pPath);
    mLocalWADFile->Clear(false);
    DebugPrint("Cleared WADFile.\n", pPath);
    if(pPath)
    {
        DebugPrint("Exec ParseFile() on %s.\n", pPath);
        mLocalWADFile->ParseFile(pPath);
    }

    //--This file becomes the active one.
    DebugPrint("Setting static reference file.\n");
    xrActiveWADFile = mLocalWADFile;

    //--Now parse the additional paths, assuming they exist. One could still legally pass NULL in,
    //  but why would you want to?
    if(pAdditionalPaths > 0 && pPaths)
    {
        DebugPrint("Handling %i additional paths.\n", pAdditionalPaths);
        for(int i = 0; i < pAdditionalPaths; i ++)
        {
            DebugPrint(" Path %i: %s\n", i, pPaths[i]);
            mLocalWADFile->ParseFile(pPaths[i]);
        }
    }

    //--Run this function to set the active level to the first one.
    DebugPrint("Exec BeginLevel().\n");
    mLocalWADFile->BeginLevel(0);

    //--Store the CameraX/Y values. These are the player's position, assuming the THINGS lump was parsed correctly.
    DebugPrint("Storing camera info.\n");
    if(pPath)
    {
        //--The X/Y positions are flipped since the camera is inverted in OpenGL.
        StarCamera3D *r3DCamera = CameraManager::FetchActiveCamera3D();
        r3DCamera->Get3DPosition(mPlayerX, mPlayerY, mPlayerZ);
        mPlayerX = mPlayerX * -1.0f;
        mPlayerY = mPlayerY * -1.0f;

        //--The Z position is determined by the WADFile's geometry.
        mPlayerZ = mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, mPlayerZ);
    }

    //--Debug.
    DebugPop("Construction finished normally.\n");
}
void VisualLevel::Finalize()
{
    //--Finishes the level and compiles it. It should now use the same basic structures as the PandemoniumRoom.
    DebugPush(true, "VisualLevel:Finalize - Running finalize routine.\n");

    //--Room condensation. Replaces PL_FinalizeRooms().
    PandemoniumLevel::FinalizeRooms();

    //--Mark the player for repositioning once the loading cycle is done.
    mMustRepositionPlayer = true;

    //--[Mouse Grabbing]
    //--As soon as the level has finished booting, grab and lock the mouse if not done so already.
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    if(!rDisplayManager->IsMouseLocked() && mIsFreeMove)
    {
        rDisplayManager->LockMouse();
        rDisplayManager->HideHardwareMouse();
    }

    //--Finish up.
    DebugPop("VisualLevel:Finalize - Complete.\n");
}

///========================================== Drawing =============================================
void VisualLevel::Render()
{
    //--Flip this flag. We handle menu rendering locally (or deliberately don't).
    MapManager::xHasRenderedMenus = true;

    //--Render the WAD File.
    if(!mLocalWADFile || mMustRepositionPlayer || !Images.mIsReady) return;

    //--Set the UI contants. This will only do anything if the screen has changed sizes.
    SetUIConstants();

    //--Render the level geometry in a frustum.
    mLocalWADFile->SetupFrustum();

    //--Camera, render.
    mLocalWADFile->SetupCamera();
    mLocalWADFile->Render();

    //--Render nodes within the level geometry. Must be done before the Camera is reset.
    if(mIsFreeMove)// && !DisplayManager::Fetch()->IsMouseLocked())
    {
        mZoneEditor->RenderNodesInWorld();
        mZoneEditor->RenderNodeNames();
    }

    //--Reset camera, render skybox, and clean up.
    mLocalWADFile->TakeDownCamera();
    mLocalWADFile->RenderSkybox();
    mLocalWADFile->TakeDownFrustum();

    //--If the mouselook is being ignored, render the ZoneEditor's UI.
    if(mIsFreeMove && !DisplayManager::Fetch()->IsMouseLocked())
    {
        mZoneEditor->Render();
        return;
    }

    //--Standard game UI. Doesn't render if the ZoneEditor is up.
    //RenderView();

    //--Entities Pane. This is not a pane, but rather the entities over the 3D space. It must render
    //  before the characters pane since some text may underlay the pane.
    if(!mIsFreeMove) RenderEntitiesPane();

    //--Character Pane.
    if(!mIsFreeMove) RenderCharacterPane();

    //--World. This is actually the minimap.
    RenderWorld();

    //--Console Pane. Doesn't render if the story display is, since that makes the text harder to see.
    if(!mShowStoryDisplay)
    {
        RenderConsolePane();
    }
    //--Story display. Renders over everything else.
    else
    {
        RenderStoryMode();
    }

    //--Render the context menu over everything else.
    if(mShowContextMenu) mContextMenu->Render();

    //--[System Menu]
    //--Render a button in the top-left to open up the System Menu. The button borrows the repositioning code from
    //  the console to compute its left-edge.
    MapManager::xHasRenderedMenus = true;
    if(!MapManager::Fetch()->MenuStackHasContents())
    {
        Images.Data.rSystemMenuBtn->Draw(ConsolePane.cLft + 4, 4);
    }

    //--[Damage Overlay]
    //--Renders a red/purple overlay over the edges of the screen if the player is getting damaged. Can be toggled
    //  by the OptionsManager.
    void *rPlayerCheckPtr = EntityManager::Fetch()->GetLastPlayerEntity();
    DamageAnimPack *rPack = (DamageAnimPack *)mDamageAnimationQueue->GetElementBySlot(0);
    if(rPack && rPack->rTargetRefPtr == rPlayerCheckPtr && xAllowDamageOverlay)
    {
        //--If both the damage components are 0, don't render this overlay. Instead, render a miss.
        if(rPack->mDamageHP < 1 && rPack->mDamageWP < 1)
        {

        }
        //--Render the overlay.
        else
        {
            //--Bind it, since we may need to stretch it for widescreen.
            Images.Data.rHitOverlay->Bind();

            //--Base borders. Recompute to handle widescreen as necessary.
            float cLft = 0.0f;
            float cTop = 0.0f;
            float cRgt = VIRTUAL_CANVAS_X;
            float cBot = VIRTUAL_CANVAS_Y;
            GLOBAL *rGlobal = Global::Shared();
            if(rGlobal->gWindowWidth > VIRTUAL_CANVAS_X)
            {
                cLft = cLft - (rGlobal->gWindowWidth - VIRTUAL_CANVAS_X) / 2.0f;
                cRgt = cRgt + (rGlobal->gWindowWidth - VIRTUAL_CANVAS_X) / 2.0f;
            }

            //--Compute the alpha level.
            float tAlpha = (sinf(rPack->mDamageTimer / (float)PL_DAM_TICKS_TOTAL* 3.1415926f) * 2.50f) - 1.50f;
            glColor4f(1.0f, 1.0f, 1.0f, tAlpha);

            //--Render.
            glBegin(GL_QUADS);
                glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTop);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTop);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(cRgt, cBot);
                glTexCoord2f(0.0f, 1.0f); glVertex2f(cLft, cBot);
            glEnd();

            //--Clean.
            glColor3f(1.0f, 1.0f, 1.0f);
        }
    }

    //--If the menu stack has something on it, render them. We also render an overlay to grey
    //  out everything else.
    if(MapManager::Fetch()->MenuStackHasContents())
    {
        //--[Setup]
        //--Base borders.
        float cLft = 0.0f;
        float cTop = 0.0f;
        float cRgt = VIRTUAL_CANVAS_X;
        float cBot = VIRTUAL_CANVAS_Y;

        //--Recompute the borders. We need to make the overlay cover the whole screen, accounting for no letterbox in 3D mode.
        GLOBAL *rGlobal =  Global::Shared();
        if(rGlobal->gWindowWidth > VIRTUAL_CANVAS_X)
        {
            cLft = cLft - (rGlobal->gWindowWidth - VIRTUAL_CANVAS_X) / 2.0f;
            cRgt = cRgt + (rGlobal->gWindowWidth - VIRTUAL_CANVAS_X) / 2.0f;
        }

        //--[Overlay]
        //--Render a grey overlay.
        glColor4f(0.0f, 0.0f, 0.0f, 0.75f);
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
            glVertex2f(cLft, cTop);
            glVertex2f(cRgt, cTop);
            glVertex2f(cRgt, cBot);
            glVertex2f(cLft, cBot);
        glEnd();
        glEnable(GL_TEXTURE_2D);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

        //--Now render the menu components.
        MapManager::Fetch()->RenderMenuStack();
    }
}
void VisualLevel::RenderView()
{
}

///====================================== Pointer Routing =========================================
StarFont *VisualLevel::GetFont()
{
    return PandemoniumLevel::Images.Data.rUIFontMedium;
}
VisualRoom *VisualLevel::GetRoomByNodeID(uint32_t pID)
{
    if(!pID || mRoomsTotal < 1 || !mRooms) return NULL;
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        if(!mRooms[i]->IsOfType(POINTER_TYPE_VISUAL_ROOM)) continue;
        VisualRoom *rRoom = (VisualRoom *)mRooms[i];
        if(rRoom->GetAssociatedNodeID() == pID) return rRoom;
    }
    return NULL;
}
ZoneNode *VisualLevel::GetNodeByID(uint32_t pID)
{
    if(!pID || !mZoneEditor) return NULL;
    return mZoneEditor->GetNode(pID);
}
ZoneNode *VisualLevel::GetNodeByName(const char *pName)
{
    if(!pName || !mZoneEditor) return NULL;
    return mZoneEditor->GetNode(pName);
}

///===================================== Static Functions =========================================
float VisualLevel::ResolveLargestScale(StarFont *pFont, float pWidth, const char *pText)
{
    //--Returns the largest scale (largest as 1.0f) that the given text can be rendered at while still
    //  fitting into pWidth pixels. Useful to prevent lines from running over an edge.
    //--Can never return 0.0f under any circumstances. This is to prevent scaling crashes.
    if(!pFont || !pText || pWidth < 1.0f) return 1.0f;

    //--Initial. This may be all that's necessary if the text is narrow enough.
    float tTextLen = pFont->GetTextWidth(pText);

    //--If the initial size was small enough, just return 1.0f.
    if(tTextLen <= pWidth)
    {
        return 1.0f;
    }

    //--If the text length came back as 1 pixel or less, fail.
    if(tTextLen <= 1) return 1.0f;

    //--Otherwise, return division.
    return pWidth / tTextLen;
}
VisualLevel *VisualLevel::Fetch()
{
    PandemoniumLevel *rCheckLevel = PandemoniumLevel::Fetch();
    if(rCheckLevel && rCheckLevel->IsOfType(POINTER_TYPE_VISUALLEVEL)) return (VisualLevel *)rCheckLevel;
    return NULL;
}

///======================================== Lua Hooking ===========================================
void VisualLevel::HookToLuaState(lua_State *pLuaState)
{
    /* VL_BeginLevelConstruction()
       VL_BeginLevelConstruction(sDataFile)
       Creates a new VisualLevel and pushes it as the Active Object. If sDataFile is passed, the
       program will attempt to build the level based on that file as if it were an slf file.
       Note that VL_FinishConstruction() must be called to actually save the file! */
    lua_register(pLuaState, "VL_BeginLevelConstruction", &Hook_VL_BeginLevelConstruction);

    /* VL_SetProperty("Node Data Path", sPath)
       VL_SetProperty("Reload Path", sPath)
       VL_SetProperty("Geometry Path", sPath)
       VL_SetProperty("Load Geometry Now")
       VL_SetProperty("Load Nodes Now")
       Sets the requested property in the active VisualLevel. */
    lua_register(pLuaState, "VL_SetProperty", &Hook_VL_SetProperty);

    /* VL_AddSkybox(sPattern)
       Creates or replaces the skybox in the local WADFile with the one provided by the pattern.
       If WAD-style rendering is not in use, does nothing. */
    lua_register(pLuaState, "VL_AddSkybox", &Hook_VL_AddSkybox);

    /* VL_AddRoom(sRoomName)
       Creates a new VisualRoom during construction, and pushes it as the active object. */
    lua_register(pLuaState, "VL_AddRoom", &Hook_VL_AddRoom);

    /* VL_ReplaceTexturesFromFile(sFilePath)
       Given a .slf file, finds and replaces all the textures with the same names in the current
       VisualLevel. The VL must still be in construction and not finalized. */
    lua_register(pLuaState, "VL_ReplaceTexturesFromFile", &Hook_VL_ReplaceTexturesFromFile);

    /* VL_FinishConstruction()
       Assuming a VisualLevel is atop the Activity Stack, pops the stack and registers the level to
       the MapManager. */
    lua_register(pLuaState, "VL_FinishConstruction", &Hook_VL_FinishConstruction);

    /* VL_CreateTextureRemap(sDLPath, fLft, fTop, fRgt, fBot)
       Creates a new Remap Pack, used to determine which part of the portrait is visible in the
       character pane. This should be done after level construction is complete. */
    lua_register(pLuaState, "VL_CreateTextureRemap", &Hook_VL_CreateTextureRemap);

    /* VL_ActivateStoryDisplay()
       Activates the Story Display in the Visual Level. Fails safely if no VL is active. */
    lua_register(pLuaState, "VL_ActivateStoryDisplay", &Hook_VL_ActivateStoryDisplay);

    /* VL_FlagTeleport()
       Indicates that the previous movement action was a teleportation. Moves the camera to the location
       the player is at and ignores collisions. */
    lua_register(pLuaState, "VL_FlagTeleport", &Hook_VL_FlagTeleport);

    /* VL_ShouldTruncateSelfMenu() (1 boolean)
       Returns whether or not the Self-Menu should should be having the close/examine options. If this
       comes back true, it means the menu is always "open". */
    lua_register(pLuaState, "VL_ShouldTruncateSelfMenu", &Hook_VL_ShouldTruncateSelfMenu);

    /* VL_RebuildSelfMenu()
       Immediately rebuilds the self-menu. Used when a turn has not ended but a player's form has changed,
       such as during the Rusalka TF. */
    lua_register(pLuaState, "VL_RebuildSelfMenu", &Hook_VL_RebuildSelfMenu);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_VL_BeginLevelConstruction(lua_State *L)
{
    //VL_BeginLevelConstruction()
    //VL_BeginLevelConstruction(sDataFile)
    //VL_BeginLevelConstruction(sDataFile, sMoreFiles, ...)
    int tArgs = lua_gettop(L);

    //--Create and push.
    VisualLevel *nNewLevel = new VisualLevel();
    DataLibrary::Fetch()->PushActiveEntity(nNewLevel);
    VisualLevel::xrConstructionLevel = nNewLevel;

    //--If no args were passed, construct with no data file. Note this does the same thing as passing
    //  an invalid path.
    if(tArgs < 1)
    {
        nNewLevel->ConstructWithFile(NULL);
    }
    //--A pathname was passed, so execute the construction based on the standard file type.
    else if(tArgs == 1)
    {
        nNewLevel->ConstructWithFile(lua_tostring(L, 1));
    }
    //--More than one pathname is passed. This builds using a load-order structure, with the 0th path
    //  being the base wad.
    else
    {
        //--Assemble a list.
        SetMemoryData(__FILE__, __LINE__);
        const char **tPathList = (const char **)starmemoryalloc(sizeof(const char *) * (tArgs - 1));
        for(int i = 0; i < tArgs-1; i ++)
        {
            tPathList[i] = lua_tostring(L, i+2);
        }

        //--Send it.
        nNewLevel->ConstructWithFile(lua_tostring(L, 1), tArgs-1, tPathList);

        //--Clean.
        free(tPathList);
    }

    //--Done.
    return 0;
}
int Hook_VL_SetProperty(lua_State *L)
{
    //VL_SetProperty("Node Data Path", sPath)
    //VL_SetProperty("Reload Path", sPath)
    //VL_SetProperty("Geometry Path", sPath)
    //VL_SetProperty("Load Geometry Now")
    //VL_SetProperty("Load Nodes Now")
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("VL_SetProperty");

    //--Get and check the active object.
    VisualLevel *rActiveLevel = (VisualLevel *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveLevel || !rActiveLevel->IsOfType(POINTER_TYPE_VISUALLEVEL)) return LuaTypeError("VL_SetProperty", L);

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    //--Path to search for node information.
    if(!strcasecmp("Node Data Path", rSwitchType) && tArgs == 2)
    {
        rActiveLevel->SetSavePath(lua_tostring(L, 2));
    }
    //--Path to execute when reloading the level.
    else if(!strcasecmp("Reload Path", rSwitchType) && tArgs == 2)
    {
        rActiveLevel->SetReloadPath(lua_tostring(L, 2));
    }
    //--Path to to the file containing world geometry.
    else if(!strcasecmp("Geometry Path", rSwitchType) && tArgs == 2)
    {
        rActiveLevel->SetGeometryPath(lua_tostring(L, 2));
    }
    //--Immediately load the geometry. Used if you're not constructing from a WAD file.
    else if(!strcasecmp("Load Geometry Now", rSwitchType) && tArgs == 1)
    {
        rActiveLevel->LoadGeometryNow();
    }
    //--Immediately load the nodes.
    else if(!strcasecmp("Load Nodes Now", rSwitchType) && tArgs == 1)
    {
        rActiveLevel->LoadNodesNow();
    }
    //--Error case.
    else
    {
        return LuaPropertyError("VL_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_VL_AddSkybox(lua_State *L)
{
    //VL_AddSkybox(sPattern)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("VL_AddSkybox");

    //--Get and check the active object.
    VisualLevel *rActiveLevel = (VisualLevel *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveLevel || !rActiveLevel->IsOfType(POINTER_TYPE_VISUALLEVEL)) return LuaTypeError("VL_AddSkybox", L);

    //--Set.
    rActiveLevel->BuildSkybox(lua_tostring(L, 1));
    return 0;
}
int Hook_VL_AddRoom(lua_State *L)
{
    //VL_AddRoom(sRoomName)

    //--Get the active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    VisualLevel *rActiveLevel = (VisualLevel *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("VL_AddRoom");

    //--Get and check the active object.
    if(!rActiveLevel || !rActiveLevel->IsOfType(POINTER_TYPE_VISUALLEVEL)) return LuaTypeError("VL_AddRoom", L);

    //--Add a new VisualRoom and register it.
    VisualRoom *nRoom = new VisualRoom();
    nRoom->SetName(lua_tostring(L, 1));
    rActiveLevel->RegisterRoom(nRoom);
    rDataLibrary->rActiveObject = nRoom;
    return 0;
}
int Hook_VL_ReplaceTexturesFromFile(lua_State *L)
{
    //VL_ReplaceTexturesFromFile(sFilePath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("VL_ReplaceTexturesFromFile");

    //--Get and check the active object.
    VisualLevel *rActiveLevel = (VisualLevel *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveLevel || !rActiveLevel->IsOfType(POINTER_TYPE_VISUALLEVEL)) return LuaTypeError("VL_ReplaceTexturesFromFile", L);

    //--Send the instruction.
    rActiveLevel->ReplaceTexturesFrom(lua_tostring(L, 1));
    return 0;
}
int Hook_VL_FinishConstruction(lua_State *L)
{
    //VL_FinishConstruction()

    //--Check the activity stack.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    VisualLevel::xrConstructionLevel = NULL;
    VisualLevel *rLevel = (VisualLevel *)rDataLibrary->PopActiveEntity();
    if(!rLevel || !rLevel->IsOfType(POINTER_TYPE_VISUALLEVEL)) return LuaTypeError("VL_FinishConstruction", L);

    //--Set as the active level.
    MapManager::Fetch()->ReceiveLevel(rLevel);

    //--Run the finalization.
    rLevel->Finalize();
    return 0;
}
int Hook_VL_CreateTextureRemap(lua_State *L)
{
    //VL_CreateTextureRemap(sDLPath, fLft, fTop, fWid, fHei)
    int tArgs = lua_gettop(L);
    if(tArgs < 5) return LuaArgError("VL_CreateTextureRemap");

    //--Get the VisualLevel. Fails safely if the level exists but is not a VisualLevel. Construction
    //  must have finished for this to work!
    VisualLevel *rActiveLevel = VisualLevel::Fetch();
    if(!rActiveLevel) return 0;

    //--Run.
    rActiveLevel->CreateRemapPack(lua_tostring(L, 1), lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    return 0;
}
int Hook_VL_ActivateStoryDisplay(lua_State *L)
{
    //VL_ActivateStoryDisplay()
    VisualLevel *rActiveLevel = VisualLevel::Fetch();
    if(rActiveLevel) rActiveLevel->ActivateStoryMode();
    return 0;
}
int Hook_VL_FlagTeleport(lua_State *L)
{
    //VL_FlagTeleport()
    VisualLevel *rActiveLevel = VisualLevel::Fetch();
    if(rActiveLevel) rActiveLevel->FlagTeleportation();
    return 0;
}
int Hook_VL_ShouldTruncateSelfMenu(lua_State *L)
{
    //VL_ShouldTruncateSelfMenu()
    VisualLevel *rActiveLevel = VisualLevel::Fetch();

    //--No level, always return false.
    if(!rActiveLevel)
    {
        lua_pushboolean(L, false);
        return 1;
    }

    //--Otherwise, return true.
    lua_pushboolean(L, true);
    return 1;
}
int Hook_VL_RebuildSelfMenu(lua_State *L)
{
    //VL_RebuildSelfMenu()
    VisualLevel *rActiveLevel = VisualLevel::Fetch();
    if(!rActiveLevel) return 0;
    rActiveLevel->RebuildSelfMenu();
    return 0;
}
