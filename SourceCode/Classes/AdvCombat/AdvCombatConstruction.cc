//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "AdventureLevel.h"
#include "AliasStorage.h"
#include "FieldAbility.h"
#include "TilemapActor.h"

//--CoreClasses
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarTranslation.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioPackage.h"
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"

///--[Debug]
//#define COMBAT_CONSTRUCTION_DEBUG
#ifdef COMBAT_CONSTRUCTION_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///--[Local Definitions]
#define ADVCOMBAT_INTRO_TICKS 15

///======================================== Initialize ============================================
void AdvCombat::Initialize()
{
    ///--[ =========== Documentation and Setup ========== ]
    //--In order to keep the constructor in very large classes from becoming unmanageable, this Initialize() function is called
    //  when it is created. It otherwise does all the work of the constructor.
    //--Should only be called once on allocation. If resetting the class, use Reinitialize().

    //--If already initialized, stop.
    if(mHasInitializedAdvCombat) return;
    mHasInitializedAdvCombat = true;

    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_ADVCOMBAT;

    ///--[ ======== AdvCombat ======= ]
    ///--[System]
    mCombatReinitialized = false;
    mIsActive = false;
    mWasLastDefeatSurrender = false;
    mAIHandledAction = false;
    mPlayerGainedInitiative = false;
    mShowDetailedAbilityDescriptions = ADVCOMBAT_DESCRIPTIONS_SIMPLE;
    mDoctorResolvePath = NULL;
    mDoctorResolveValue = 0;
    mStatisticsPath = NULL;

    ///--[Changeable Constants]
    //--Constants that can be updated by derived classes.
    cIntroMoveTicks = ADVCOMBAT_INTRO_TICKS + 30;
    cStdMoveTicks = ADVCOMBAT_STD_MOVE_TICKS;
    cMoveTargetTicks = ADVCOMBAT_MOVE_TARGET_TICKS;

    ///--[Colors]
    mHeadingStatic.SetRGBAF(1.0f, 0.8f, 0.1f, 1.0f);

    ///--[Field Abilities]
    mExtraScripts = new StarLinkedList(true);
    for(int i = 0; i < ADVCOMBAT_FIELD_ABILITY_SLOTS; i ++)
    {
        rActiveFieldAbilities[i] = NULL;
    }

    ///--[Introduction]
    mIsIntroduction = true;
    mIntroTimer = 0;

    ///--[Title]
    mShowTitle = false;
    mTitleTicks = 1;
    mTitleTicksMax = 1;
    mTitleText = NULL;

    ///--[Turn State]
    mIsTurnActive = false;
    mCurrentTurn = -1;
    mTurnDisplayTimer = ACE_TURN_DISPLAY_TICKS;
    mTurnBarMoveTimer = 1;
    mTurnBarMoveTimerMax = 1;
    mTurnBarNewTurnTimer = 1;
    mTurnBarNewTurnTimerMax = 1;
    mTurnWidStart = 0;
    mTurnWidCurrent = 0;
    mTurnWidTarget = 0;
    mrTurnOrder = new StarLinkedList(false);
    rTurnDiscardImg = NULL;

    ///--[Action Handling]
    mNoEventsEndsAction = false;
    mWasLastActionFree = false;
    mWasLastActionEffortless = false;
    mActionFirstTick = false;
    mIsActionOccurring = false;
    mIsActionConcluding = false;
    mIsPerformingPlayerUpdate = false;
    mEventFirstTick = false;
    mEventCanRun = false;
    mEntitiesHaveRepositioned = false;
    mEventTimer = 0;
    rEventZeroPackage = NULL;
    rFiringEventPackage = NULL;
    mEventQueue = new StarLinkedList(true);
    mTextScatterCounter = 0;
    mApplicationQueue = new StarLinkedList(true);
    mToggleDescriptionString = new StarlightString();

    ///--[Player Input]
    mHideCombatUI = false;
    mPlayerInterfaceTimer = 0;
    mPlayerCardTimer = 0;
    mPlayerJobTimer = 0;
    mPlayerMemorizedTimer = 0;
    mPlayerTacticsTimer = 0;
    mPlayerMode = ADVCOMBAT_INPUT_CARDS;
    mPlayerCPFrameTimer = 0;
    mPlayerDescriptionTimer = 0;
    mPlayerCPTimer = 0;
    mAbilitySelectionX = 0;
    mAbilitySelectionY = 0;
    mAbilityHighlightPos.Initialize();
    mAbilityHighlightSize.Initialize();
    mHideUIString = new StarlightString();
    mShowUIString = new StarlightString();
    mToggleDescriptionsString = new StarlightString();
    mChangeCardString = new StarlightString();

    ///--[Expert Mode]
    mExpertSecondPage = false;
    mExpertPageSwapTime = 0;
    mExpertArrowTimer = 0;

    ///--[Target Selection]
    mLockTargetClusters = false;
    mTargetClusterCounter = 0;
    mIsSelectingTargets = false;
    mTargetClusterCursor = 0;
    mTargetFlashTimer = 0;
    mLastUsedTargetType = InitializeString("Custom");
    mTargetClusters = new StarLinkedList(true);

    ///--[Prediction Boxes]
    mIsClearingPredictionBoxes = false;
    rPredictionCluster = NULL;
    mPredictionBoxes = new StarLinkedList(true);

    ///--[Confirmation Window]
    mIsShowingConfirmationWindow = false;
    mConfirmationTimer = 0;
    mConfirmationString = new StarlightString();
    mAcceptString = new StarlightString();
    mCancelString = new StarlightString();
    rConfirmationFunction = NULL;

    ///--[Combat Inspector]
    //--System
    mIsShowingCombatInspector = false;
    mInspectorShowStatistics = false;
    mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
    mCombatInspectorAbilitiesType = ADVCOMBAT_INSPECTOR_ABILITIES_JOB;
    mrInspectorEffectListing = NULL;
    mInspectorStatLookups[ADVCOMBAT_INSPECTOR_STATUS_ICON_HPMAX]             = STATS_HPMAX;
    mInspectorStatLookups[ADVCOMBAT_INSPECTOR_STATUS_ICON_MPMAX]             = STATS_MPMAX;
    mInspectorStatLookups[ADVCOMBAT_INSPECTOR_STATUS_ICON_MPREGEN]           = STATS_MPREGEN;
    mInspectorStatLookups[ADVCOMBAT_INSPECTOR_STATUS_ICON_ATTACK]            = STATS_ATTACK;
    mInspectorStatLookups[ADVCOMBAT_INSPECTOR_STATUS_ICON_INITIATIVE]        = STATS_INITIATIVE;
    mInspectorStatLookups[ADVCOMBAT_INSPECTOR_STATUS_ICON_ACCURACY]          = STATS_ACCURACY;
    mInspectorStatLookups[ADVCOMBAT_INSPECTOR_STATUS_ICON_EVADE]             = STATS_EVADE;
    mInspectorStatLookups[ADVCOMBAT_INSPECTOR_STATUS_ICON_THREAT_MULTIPLIER] = STATS_THREAT_MULTIPLIER;

    //--Timers
    mCombatInspectorTimer = 0;
    mCombatInspectorJobTimer = 0;
    mCombatInspectorMemorizedTimer = 0;
    mCombatInspectorTacticsTimer = 0;

    //--Cursors
    mInspectorEntityCursor = 0;
    mInspectorEntitySkip = 0;
    mInspectorEffectCursor = 0;
    mInspectorEffectSkip = 0;
    mInspectorAbilityCursorX = 0;
    mInspectorAbilityCursorY = 0;
    mrInspectorEntityList = new StarLinkedList(false);

    //--Highlight
    mInspectorHighlightPos.Initialize();
    mInspectorHighlightSize.Initialize();

    //--Colors
    mInspectorBonus.SetRGBAF(0.80f, 0.10f, 0.80f, 1.0f);
    mInspectorMalus.SetRGBAF(0.80f, 0.10f, 0.15f, 1.0f);

    //--Strings
    mCombatInspectorShow              = new StarlightString();
    mCombatInspectorHelp              = new StarlightString();
    mCombatInspectorScrollFast        = new StarlightString();
    mCombatInspectorEffects           = new StarlightString();
    mCombatInspectorAbilities         = new StarlightString();
    mCombatInspectorToggleResistances = new StarlightString();
    mCombatInspectorToggleSkills      = new StarlightString();

    ///--[Ability Execution]
    mSkillCatalystExtension = 0;
    rCurrentApplicationPack = NULL;
    rActiveAbility = NULL;
    rActiveTargetCluster = NULL;

    ///--[System Abilities]
    mVolunteerScript = NULL;
    mSysAbilityPassTurn = new AdvCombatAbility();
    mSysAbilityPassTurn->SetScriptResponse(ACA_SCRIPT_CODE_CREATE, true);
    mSysAbilityPassTurn->SetScriptResponse(ACA_SCRIPT_CODE_SPECIALCREATE, true);
    mSysAbilityPassTurn->SetInternalName("System|PassTurn");
    mSysAbilityRetreat = new AdvCombatAbility();
    mSysAbilityRetreat->SetScriptResponse(ACA_SCRIPT_CODE_CREATE, true);
    mSysAbilityRetreat->SetScriptResponse(ACA_SCRIPT_CODE_SPECIALCREATE, true);
    mSysAbilityRetreat->SetInternalName("System|Retreat");
    mSysAbilitySurrender = new AdvCombatAbility();
    mSysAbilitySurrender->SetScriptResponse(ACA_SCRIPT_CODE_CREATE, true);
    mSysAbilitySurrender->SetScriptResponse(ACA_SCRIPT_CODE_SPECIALCREATE, true);
    mSysAbilitySurrender->SetInternalName("System|Surrender");
    mSysAbilityVolunteer = new AdvCombatAbility();
    mSysAbilityVolunteer->SetScriptResponse(ACA_SCRIPT_CODE_CREATE, true);
    mSysAbilityVolunteer->SetScriptResponse(ACA_SCRIPT_CODE_SPECIALCREATE, true);
    mSysAbilityVolunteer->SetInternalName("System|Volunteer");

    ///--[Animation Storage]
    mCombatAnimations = new StarLinkedList(true);
    mTextPackages = new StarLinkedList(true);

    ///--[Option Flags]
    mAutoUseDoctorBag = true;
    mTouristMode = false;
    mMemoryCursor = true;
    mRestoreMainWeaponOnCombatEnd = true;

    ///--[Combat State Flags]
    mIsUnwinnable = false;
    mIsUnloseable = false;
    mIsUnretreatable = false;
    mIsUnsurrenderable = false;
    mResolutionTimer = 0;
    mResolutionState = 0;
    mCombatResolution = ADVCOMBAT_END_NONE;
    mResolutionDoctorStart = 0;
    mResolutionDoctorEnd = 0;
    mPostScriptHandler = NULL;

    ///--[Enemy Storage]
    mUniqueEnemyIDCounter = 0;
    mParagonScript = NULL;
    mEnemyAutoScript = NULL;
    mDefeatedUnitScript = NULL;
    mLastParagonValue = ADVCOMBAT_PARAGON_NO;
    mEnemyAliases = new AliasStorage();
    rKnockoutEntity = NULL;
    rLastRegisteredEnemy = NULL;
    mEnemyRoster = new StarLinkedList(true);
    mrEnemyCombatParty = new StarLinkedList(false);
    mrEnemyReinforcements = new StarLinkedList(false);
    mrEnemyGraveyard = new StarLinkedList(false);
    mWorldReferences = new StarLinkedList(true);

    ///--[Party Storage]
    mPartyRoster = new StarLinkedList(true);
    mrActiveParty = new StarLinkedList(false);
    mrCombatParty = new StarLinkedList(false);

    ///--[Stat Storage for Level Ups]
    mJobTempStatistics.Zero();

    ///--[Victory Storage]
    mXPToAward = 0;
    mJPToAward = 0;
    mPlatinaToAward = 0;
    mDoctorToAward = 0;
    mXPBonus = 0;
    mJPBonus = 0;
    mPlatinaBonus = 0;
    mDoctorBonus = 0;
    mXPToAwardStorage = 0;
    mJPToAwardStorage = 0;
    mPlatinaToAwardStorage = 0;
    mDoctorToAwardStorage = 0;
    mItemsToAwardList = new StarLinkedList(true);
    mItemsAwardedList = new StarLinkedList(true);

    ///--[Effect Storage]
    mMustRebuildEffectReferences = false;
    mSpecialShieldString = new StarlightString();
    mSpecialAdrenalineString = new StarlightString();
    mGlobalEffectList = new StarLinkedList(true);
    mEffectGraveyard = new StarLinkedList(true);
    mrTempEffectList = new StarLinkedList(false);

    ///--[Response Constants]
    //--Entity Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = -1; //--Handled by AdvCombatEntity::HandleCombatStart().
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ACE_SCRIPT_CODE_BEGINTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ACE_SCRIPT_CODE_BEGINACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ACE_SCRIPT_CODE_BEGINFREEACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ENDACTION]                    = ACE_SCRIPT_CODE_ENDACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ENDTURN]                      = ACE_SCRIPT_CODE_ENDTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ACE_SCRIPT_CODE_ENDCOMBAT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = ACE_SCRIPT_CODE_EVENTQUEUED;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = -1;

    //--AI Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = ACE_AI_SCRIPT_CODE_COMBATSTART;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ACE_AI_SCRIPT_CODE_TURNSTART;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ACE_AI_SCRIPT_CODE_ACTIONBEGIN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ACE_AI_SCRIPT_CODE_FREEACTIONBEGIN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ENDACTION]                    = ACE_AI_SCRIPT_CODE_ACTIONEND;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ENDTURN]                      = ACE_AI_SCRIPT_CODE_TURNEND;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ACE_AI_SCRIPT_CODE_COMBATENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = ACE_AI_SCRIPT_CODE_KNOCKEDOUT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = ACE_AI_SCRIPT_CODE_APPLICATION_START;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = ACE_AI_SCRIPT_CODE_APPLICATION_END;

    //--Job Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = ADVCJOB_CODE_BEGINCOMBAT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ADVCJOB_CODE_BEGINTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ADVCJOB_CODE_BEGINACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ADVCJOB_CODE_BEGINFREEACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ENDACTION]                    = ADVCJOB_CODE_ENDACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ENDTURN]                      = ADVCJOB_CODE_ENDTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ADVCJOB_CODE_ENDCOMBAT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = ADVCJOB_CODE_EVENTQUEUED;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = -1;

    //--Ability Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = -1; //--Handled by AdvCombatEntity::HandleCombatStart().
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ACA_SCRIPT_CODE_BEGINTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ACA_SCRIPT_CODE_BEGINACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ACA_SCRIPT_CODE_BEGINFREEACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ENDACTION]                    = ACA_SCRIPT_CODE_POSTACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ENDTURN]                      = ACA_SCRIPT_CODE_TURNENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ACA_SCRIPT_CODE_COMBATENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = ACA_SCRIPT_CODE_EVENT_QUEUED;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = ACA_SCRIPT_CODE_PAINTTARGETS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = ACA_SCRIPT_CODE_PAINTTARGETS_RESPONSE;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = ACA_SCRIPT_CODE_EXECUTE;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = ACA_SCRIPT_CODE_GUI_APPLY_EFFECT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = -1;

    //--Effect Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ACEFF_SCRIPT_CODE_BEGINTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ACEFF_SCRIPT_CODE_BEGINACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ACEFF_SCRIPT_CODE_BEGINFREEACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ENDACTION]                    = ACEFF_SCRIPT_CODE_POSTACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ENDTURN]                      = ACEFF_SCRIPT_CODE_TURNENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ACEFF_SCRIPT_CODE_COMBATENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = ACEFF_SCRIPT_CODE_APPLYSTATS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = ACEFF_SCRIPT_CODE_UNAPPLYSTATS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = -1;

    ///--[Cutscene Handlers]
    mCutsceneExecList = new StarLinkedList(true);
    mStandardRetreatScript = NULL;
    mStandardDefeatScript = NULL;
    mStandardVictoryScript = NULL;
    mCurrentRetreatScript = NULL;
    mCurrentDefeatScript = NULL;
    mCurrentVictoryScript = NULL;
    mCombatResponseScript = NULL;

    ///--[Audio]
    mSoundList = new StarLinkedList(true);
    mDefaultCombatMusic = InitializeString("Null");
    mDefaultVictoryMusic = InitializeString("CombatVictory");
    mCombatMusicStart = 0.0f;
    mNextCombatMusic = NULL;
    mNextCombatMusicStart = 0.0f;
    mEndCombatTracksTotal = 0;
    mEndCombatMusicResume = NULL;
    mQuietBackgroundMusicForVictory = false;
    rPreviousMusicWhenReplaying = NULL;
    mPreviousTimeWhenReplaying = 0.0f;

    ///--[Auto-Win Handler]
    mAutoWinFlag = false;
    mAutoWinPath = NULL;

    ///--[Images]
    memset(&Images, 0, sizeof(Images));

    ///--[Translatable Strings]
    for(int i = 0; i < COMBAT_TRANSLATABLE_STRINGS_TOTAL; i ++)
    {
        AdvString.mem.mPtr[i] = NULL;
    }

    ///--[Public Variables]
    mIsRunningEventQuery = false;
}

///======================================= Reinitialize ===========================================
void AdvCombat::Reinitialize(bool pHandleMusic)
{
    ///--[ =========== Documentation and Setup ========== ]
    //--Re-initializes the class back to neutral variables, but leaves constants and referenced images in place. Used when the
    //  combat reboots between battles.
    DebugPush(true, "AdvCombat:Reinitialize() running.\n");
    DebugPrint("Resetting variables, clearing lists.\n");

    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    //--System
    mType = POINTER_TYPE_ADVCOMBAT;

    ///--[ ======== AdvCombat ======= ]
    ///--[System]
    mCombatReinitialized = true;
    mAIHandledAction = false;
    mPlayerGainedInitiative = false;
    mDoctorResolveValue = 0;

    ///--[Colors]
    ///--[Field Abilities]
    DebugPrint("Field Abilities.\n");
    mExtraScripts->ClearList();

    ///--[Introduction]
    ///--[Title]
    DebugPrint("Title.\n");
    mShowTitle = false;
    mTitleTicks = 1;
    mTitleTicksMax = 1;
    ResetString(mTitleText, NULL);

    ///--[Introduction]
    mIsIntroduction = true;
    mIntroTimer = 0;
    mWasLastDefeatSurrender = false;

    ///--[Turn State]
    mIsTurnActive = false;
    mCurrentTurn = -1;
    mTurnDisplayTimer = ACE_TURN_DISPLAY_TICKS;
    mTurnBarMoveTimer = 1;
    mTurnBarMoveTimerMax = 1;
    mTurnWidStart = 0;
    mTurnWidCurrent = 0;
    mTurnWidTarget = 0;
    mrTurnOrder->ClearList();
    rTurnDiscardImg = NULL;

    ///--[Action Handling]
    DebugPrint("Action handling.\n");
    mNoEventsEndsAction = false;
    mWasLastActionFree = false;
    mWasLastActionEffortless = false;
    mActionFirstTick = false;
    mIsActionOccurring = false;
    mIsActionConcluding = false;
    mIsPerformingPlayerUpdate = false;
    mAbilitySelectionX = 0;
    mAbilitySelectionY = 0;
    DebugPrint("Positions.\n");
    mAbilityHighlightPos.Initialize();
    mAbilityHighlightSize.Initialize();
    mEventTimer = 0;
    mEventFirstTick = true;
    mEventCanRun = false;
    mEntitiesHaveRepositioned = false;
    rEventZeroPackage = NULL;
    rFiringEventPackage = NULL;
    DebugPrint("Event queue.\n");
    mEventQueue->ClearList();
    mTextScatterCounter = 0;
    DebugPrint("Application queue.\n");
    mApplicationQueue->ClearList();

    ///--[Player Input]
    DebugPrint("Input.\n");
    mHideCombatUI = false;
    mPlayerInterfaceTimer = 0;
    mPlayerCardTimer = 0;
    mPlayerJobTimer = 0;
    mPlayerMemorizedTimer = 0;
    mPlayerTacticsTimer = 0;
    mPlayerMode = ADVCOMBAT_INPUT_CARDS;
    mPlayerCPTimer = 0;
    mPlayerDescriptionTimer = 0;
    mAbilitySelectionX = 0;
    mAbilitySelectionY = 0;
    mAbilityHighlightPos.Initialize();
    mAbilityHighlightSize.Initialize();

    ///--[Expert Mode]
    mExpertSecondPage = false;
    mExpertPageSwapTime = 0;

    ///--[Target Selection]
    DebugPrint("Target selection.\n");
    mLockTargetClusters = false;
    mTargetClusterCounter = 0;
    mIsSelectingTargets = false;
    mTargetClusterCursor = 0;
    mTargetFlashTimer = 0;
    ResetString(mLastUsedTargetType, "Custom");
    mTargetClusters->ClearList();

    ///--[Prediction Boxes]
    DebugPrint("Predictions.\n");
    mIsClearingPredictionBoxes = false;
    rPredictionCluster = NULL;
    mPredictionBoxes->ClearList();

    ///--[Confirmation Window]
    DebugPrint("Confirmation.\n");
    mIsShowingConfirmationWindow = false;
    mConfirmationTimer = 0;
    rConfirmationFunction = NULL;

    ///--[Combat Inspector]
    //--Debug.
    DebugPrint("Combat Inspector.\n");

    //--System
    mIsShowingCombatInspector = false;
    mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
    mCombatInspectorAbilitiesType = ADVCOMBAT_INSPECTOR_ABILITIES_JOB;
    mrInspectorEffectListing = NULL;

    //--Timers
    mCombatInspectorTimer = 0;
    mCombatInspectorJobTimer = 0;
    mCombatInspectorMemorizedTimer = 0;
    mCombatInspectorTacticsTimer = 0;

    //--Cursors
    mInspectorEntityCursor = 0;
    mInspectorEntitySkip = 0;
    mInspectorEffectCursor = 0;
    mInspectorEffectSkip = 0;
    mInspectorAbilityCursorX = 0;
    mInspectorAbilityCursorY = 0;

    //--Highlight
    mInspectorHighlightPos.Initialize();
    mInspectorHighlightSize.Initialize();

    //--Strings
    ///--[Ability Execution]
    rCurrentApplicationPack = NULL;
    rActiveAbility = NULL;
    rActiveTargetCluster = NULL;

    ///--[System Abilities]
    ///--[Animation Storage]
    mTextPackages->ClearList();

    ///--[Option Flags]
    ///--[Combat State Flags]
    mIsUnwinnable = false;
    mIsUnloseable = false;
    mIsUnretreatable = false;
    mIsUnsurrenderable = false;
    mResolutionTimer = 0;
    mResolutionState = 0;
    mCombatResolution = ADVCOMBAT_END_NONE;

    ///--[Enemy Storage]
    DebugPrint("Enemy storage.\n");
    rKnockoutEntity = NULL;
    rLastRegisteredEnemy = NULL;
    mUniqueEnemyIDCounter = 0;
    mEnemyRoster->ClearList();
    mrEnemyCombatParty->ClearList();
    mrEnemyReinforcements->ClearList();
    mrEnemyGraveyard->ClearList();
    mWorldReferences->ClearList();

    ///--[Party Storage]
    DebugPrint("Party storage.\n");
    mrCombatParty->ClearList();
    AdvCombatEntity *rCharacterPtr = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rCharacterPtr)
    {
        rCharacterPtr->Reinitialize();
        mrCombatParty->AddElementAsTail(mrActiveParty->GetIteratorName(), rCharacterPtr);
        rCharacterPtr = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }

    ///--[Stat Storage for Level Ups]
    mJobTempStatistics.Zero();

    ///--[Victory Storage]
    mXPToAward = 0;
    mJPToAward = 0;
    mPlatinaToAward = 0;
    mDoctorToAward = 0;
    mXPBonus = 0;
    mJPBonus = 0;
    mPlatinaBonus = 0;
    mDoctorBonus = 0;
    mXPToAwardStorage = 0;
    mJPToAwardStorage = 0;
    mPlatinaToAwardStorage = 0;
    mDoctorToAwardStorage = 0;
    mItemsToAwardList->ClearList();
    mItemsAwardedList->ClearList();

    ///--[Effect Storage]
    DebugPrint("Effect storage.\n");
    mMustRebuildEffectReferences = false;
    mGlobalEffectList->ClearList();
    mEffectGraveyard->ClearList();
    mrTempEffectList->ClearList();

    ///--[Cutscene Handlers]
    DebugPrint("Scene strings\n");
    ResetString(mCurrentRetreatScript, NULL);
    ResetString(mCurrentDefeatScript, NULL);
    ResetString(mCurrentVictoryScript, NULL);

    ///--[Audio]
    DebugPrint("Audio.\n");
    free(mEndCombatMusicResume);
    mEndCombatTracksTotal = 0;
    mEndCombatMusicResume = NULL;

    ///--[Auto-Win Handler]
    ///--[Images]
    ///--[Public Variables]
    mIsRunningEventQuery = false;

    ///-- [ ============= Starlight Strings ============= ]
    ///--[Debug]
    DebugPrint("Resetting control strings.\n");

    ///--[Show/Hide UI Strings]
    //--Strings that prompt the player to show or hide the UI.
    mShowUIString->SetString(AdvString.str.mShowUI);
    mShowUIString->AllocateImages(1);
    mShowUIString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("F4"));
    mShowUIString->CrossreferenceImages();

    mHideUIString->SetString(AdvString.str.mHideUI);
    mHideUIString->AllocateImages(1);
    mHideUIString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("F4"));
    mHideUIString->CrossreferenceImages();

    ///--[Other UI Strings]
    mToggleDescriptionsString->SetString(AdvString.str.mToggleDescriptions);
    mToggleDescriptionsString->AllocateImages(1);
    mToggleDescriptionsString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("F2"));
    mToggleDescriptionsString->CrossreferenceImages();

    mChangeCardString->SetString(AdvString.str.mCycleSkills);
    mChangeCardString->AllocateImages(2);
    mChangeCardString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("UpLevel"));
    mChangeCardString->SetImageP(1, 5.0f, ControlManager::Fetch()->ResolveControlImage("DnLevel"));
    mChangeCardString->CrossreferenceImages();

    ///--[Confirmation Strings]
    //--These strings reinitialize every time combat starts since the player may have changed controls.
    mAcceptString->SetString(AdvString.str.mAccept);
    mAcceptString->AllocateImages(1);
    mAcceptString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mAcceptString->CrossreferenceImages();

    mCancelString->SetString(AdvString.str.mCancel);
    mCancelString->AllocateImages(1);
    mCancelString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mCancelString->CrossreferenceImages();

    ///--[Description String]
    //--Player may have changed controls.
    mToggleDescriptionString->SetString(AdvString.str.mToggleAbilityDetails);
    mToggleDescriptionString->AllocateImages(1);
    mToggleDescriptionString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("F2"));
    mToggleDescriptionString->CrossreferenceImages();

    ///--[Combat Inspector Strings]
    //--Displays Combat Inspector
    mCombatInspectorShow->SetString(AdvString.str.mOpenCombatInspector);
    mCombatInspectorShow->AllocateImages(1);
    mCombatInspectorShow->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mCombatInspectorShow->CrossreferenceImages();

    //--Show help.
    mCombatInspectorHelp->SetString(AdvString.str.mShowHelp);
    mCombatInspectorHelp->AllocateImages(1);
    mCombatInspectorHelp->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("F1"));
    mCombatInspectorHelp->CrossreferenceImages();

    //--Scroll x10
    mCombatInspectorScrollFast->SetString(AdvString.str.mScrollx10);
    mCombatInspectorScrollFast->AllocateImages(1);
    mCombatInspectorScrollFast->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Ctrl"));
    mCombatInspectorScrollFast->CrossreferenceImages();

    //--Effects String
    mCombatInspectorEffects->SetString(AdvString.str.mInspect);
    mCombatInspectorEffects->AllocateImages(1);
    mCombatInspectorEffects->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mCombatInspectorEffects->CrossreferenceImages();

    //--Abilities String
    mCombatInspectorAbilities->SetString(AdvString.str.mInspect);
    mCombatInspectorAbilities->AllocateImages(1);
    mCombatInspectorAbilities->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Jump"));
    mCombatInspectorAbilities->CrossreferenceImages();

    //--Toggle Resistances String
    mCombatInspectorToggleResistances->SetString(AdvString.str.mToggleResistances);
    mCombatInspectorToggleResistances->AllocateImages(1);
    mCombatInspectorToggleResistances->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("OpenFieldAbilityMenu"));
    mCombatInspectorToggleResistances->CrossreferenceImages();

    //--Change Skill Block
    mCombatInspectorToggleSkills->SetString(AdvString.str.mToggleSkillBlock);
    mCombatInspectorToggleSkills->AllocateImages(1);
    mCombatInspectorToggleSkills->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("Run"));
    mCombatInspectorToggleSkills->CrossreferenceImages();

    ///--[ ================ Data Library ================ ]
    //--Debug.
    DebugPrint("Clearing temporary library variables.\n");

    //--Create this path to store temporary variables.
    DataLibrary::Fetch()->AddPath("Root/Variables/Combat/");
    DataLibrary::Fetch()->AddPath("Root/Variables/Combat/AIGlobals/");
    DataLibrary::Fetch()->AddPath("Root/Variables/Combat/ResetAtTurnStart/");

    ///--[ ================= Statistics ================= ]
    //--Call this to reinitialize combat statistics.
    CallStatisticsScript(ACS_SCRIPT_CODE_COMBATSTART);

    ///--[ =================== Effects ================== ]
    //--Call this to clear any dangling effect references from all entities.
    RebuildEffectReferences();

    ///--[ ============= Memory Cursor Reset ============ ]
    //--Reset memory cursor to 0 for all entities.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rEntity)
    {
        rEntity->ClearMemoryCursors();
        rEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }

    ///--[ ==================== Music =================== ]
    //--Debug.
    DebugPrint("Handling music.\n");
    mQuietBackgroundMusicForVictory = false;
    if(pHandleMusic) HandleMusic();

    ///--[ ====== Call Response Handler ======= ]
    //--Debug.
    DebugPrint("Handling begin-combat response calls.\n");

    //--Call.
    RunGeneralResponse(ADVCOMBAT_RESPONSE_BEGINCOMBAT);

    ///--[Debug]
    DebugPop("AdvCombat:Reinitialize() completes.\n");
}
void AdvCombat::HandleMusic()
{
    ///--[Documentation]
    //--Plays music at combat start. Typically only called if ordered to by the Reinitialize()
    //  function. Music can also be blocked by global flags (such as -blockmusic).
    if(!mNextCombatMusic && !mDefaultCombatMusic) return;

    ///--[Music Resolve]
    //--Setup.
    AudioManager *rAudioManager = AudioManager::Fetch();
    AdventureLevel *rCheckLevel = AdventureLevel::Fetch();

    //--Determine which track to use.
    bool tDeallocateMusic = false;
    char *rUseMusic = mDefaultCombatMusic;
    float tUseMusicStart = mCombatMusicStart;
    if(mNextCombatMusic)
    {
        rUseMusic = mNextCombatMusic;
        tUseMusicStart = mNextCombatMusicStart;
    }

    //--Stupid kazoo battle theme.
    if(rUseMusic && !strcasecmp(rUseMusic, "BattleThemeChristine"))
    {
        //--Perform a 1/1000 roll to see if it's going to be Kumquat's kazoo theme.
        int tRoll = (rand() % 1000);
        if(tRoll == 299)
        {
            tDeallocateMusic = true;
            rUseMusic = InitializeString("BattleThemeChristineKazoo");
        }
    }

    ///--[Normal Playing]
    //--Not Layering Tracks
    if(rCheckLevel && !rCheckLevel->IsLayeringMusic())
    {
        //--If the music start position is -1, don't reset the music at all. Let it play.
        if(tUseMusicStart == -1.0f)
        {
            mQuietBackgroundMusicForVictory = true;
        }
        //--Store the music position and play the new music.
        else
        {
            //--Store.
            AudioPackage *rPlayingMusic = rAudioManager->GetPlayingMusic();
            if(rPlayingMusic)
            {
                mEndCombatTracksTotal = 1;
                mEndCombatMusicResume = (float *)starmemoryalloc(sizeof(float));
                mEndCombatMusicResume[0] = rPlayingMusic->GetPosition();
            }

            //--Start playing music.
            rAudioManager->PlayMusic(rUseMusic);
            rAudioManager->SeekMusicTo(tUseMusicStart);
        }
    }
    ///--[Layered Tracks]
    //--Layered tracks. This is only the case if the combat music is not the maximum intensity track.
    //  The music positions are still stored but the values won't be used.
    else if(rCheckLevel && !AdventureLevel::xIsCombatMaxIntensity)
    {
        //--Allocate.
        mEndCombatTracksTotal = AdventureLevel::xCurrentlyRunningTracks;
        mEndCombatMusicResume = (float *)starmemoryalloc(sizeof(float) * mEndCombatTracksTotal);

        //--Iterate.
        for(int i = 0; i < AdventureLevel::xCurrentlyRunningTracks; i ++)
        {
            //--Check the package. If it exists, store its position.
            AudioPackage *rLayer = rAudioManager->GetMusicPack(AdventureLevel::xLayerNames[i]);
            if(rLayer)
            {
                mEndCombatMusicResume[i] = rLayer->GetPosition();
                rLayer->FadeOut(15);
            }
            //--Otherwise, store 0.0f.
            else
            {
                mEndCombatMusicResume[i] = 0.0f;
            }
        }

        //--Start playing music.
        rAudioManager->PlayMusic(rUseMusic);
        rAudioManager->SeekMusicTo(tUseMusicStart);
    }
    ///--[Layered Track, Max Intensity]
    //--Mandate combat intensity as maximum if combat is set that way.
    else if(rCheckLevel && AdventureLevel::xIsCombatMaxIntensity)
    {
        AdventureLevel::xCombatMandatedIntensity = 100.0f;
    }

    ///--[Finish Up]
    //--If the next-music handler was used, clear it.
    if(mNextCombatMusic)
    {
        ResetString(mNextCombatMusic, NULL);
        mNextCombatMusicStart = 0.0f;
    }

    //--Clean.
    if(tDeallocateMusic)
    {
        free(rUseMusic);
    }
}

///======================================= Construction ===========================================
void AdvCombat::Construct()
{
    ///--[Documentation and Setup]
    //--Resolve all image references in the object, then verify them. Also sets up hardcoded positions.
    //  This may be called more than once if the images are required to change dynamically, so all
    //  memory allocated within needs to be cleared before it can be used.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    DebugPush(true, "Cross-referencing combat images.\n");
    bool tShowCrossloadDebug = false;
    #ifdef COMBAT_CONSTRUCTION_DEBUG
    tShowCrossloadDebug = true;
    #endif

    ///--[Fonts]
    //--Victory
    Images.Data.rVictoryFontHeader  = rDataLibrary->GetFont("Adventure Combat Header");
    Images.Data.rVictoryFontExp     = rDataLibrary->GetFont("Adventure Combat Exp");
    Images.Data.rVictoryFontPlatina = rDataLibrary->GetFont("Adventure Combat Platina");
    Images.Data.rVictoryFontDoctor  = rDataLibrary->GetFont("Adventure Combat Doctor");
    Images.Data.rVictoryFontItem    = rDataLibrary->GetFont("Adventure Combat VicItem");

    //--Ally Bar
    Images.Data.rAllyHPFont = rDataLibrary->GetFont("Adventure Combat Ally Bar");

    //--Player Bar
    Images.Data.rPlayerNameFont = rDataLibrary->GetFont("Adventure Combat Player Bar Name");
    Images.Data.rPlayerCardFont = rDataLibrary->GetFont("Adventure Combat Card Title");
    Images.Data.rPlayerHPFont   = rDataLibrary->GetFont("Adventure Combat Player Bar");

    //--Enemy Bar
    Images.Data.rEnemyHPFont = rDataLibrary->GetFont("Adventure Combat Enemy Bar");

    //--Misc.
    Images.Data.rAbilityTitleFont  = rDataLibrary->GetFont("Adventure Combat Ability Title");
    Images.Data.rNewTurnFont       = rDataLibrary->GetFont("Adventure Combat New Turn Font");
    Images.Data.rTargetClusterFont = rDataLibrary->GetFont("Adventure Combat Target Cluster Selection");
    Images.Data.rPredictionFont    = rDataLibrary->GetFont("Adventure Combat Prediction");
    Images.Data.rCombatTextFont    = rDataLibrary->GetFont("Adventure Combat Damage Effect");

    //--Descriptions
    Images.Data.rAbilityHeaderFont            = rDataLibrary->GetFont("Adventure Combat Description Header");
    Images.Data.rAbilityDescriptionFont       = rDataLibrary->GetFont("Adventure Combat Description");
    Images.Data.rAbilitySimpleDescriptionFont = rDataLibrary->GetFont("Adventure Combat Description Simple");

    //--Confirmation UI
    Images.Data.rConfirmationFontBig   = rDataLibrary->GetFont("Adventure Combat Confirmation Big");
    Images.Data.rConfirmationFontSmall = rDataLibrary->GetFont("Adventure Combat Confirmation Small");

    //--Combat Inspector
    Images.Data.rInspectorHeading   = rDataLibrary->GetFont("Adventure Combat Inspector Header");
    Images.Data.rInspectorMainline  = rDataLibrary->GetFont("Adventure Combat Inspector Main");
    Images.Data.rInspectorStatistic = rDataLibrary->GetFont("Adventure Combat Inspector Statistic");

    //--Help Font
    Images.Data.rHelpFont = rDataLibrary->GetFont("Adventure Combat Help");

    //--Reinforcement font, used by TilemapActors. Resolved by this object for convenience.
    TilemapActor::xrReinforcementFont = rDataLibrary->GetFont("Adventure Level Reinforcement");

    ///--[Font Debug]
    #ifdef COMBAT_CONSTRUCTION_DEBUG
    /*
        fprintf(stderr, "Combat Font Report:\n");
        fprintf(stderr, " Victory Header: %p\n",             Images.Data.rVictoryFontHeader);
        fprintf(stderr, " Victory Exp: %p\n",                Images.Data.rVictoryFontExp);
        fprintf(stderr, " Victory Platina: %p\n",            Images.Data.rVictoryFontPlatina);
        fprintf(stderr, " Victory Doctory: %p\n",            Images.Data.rVictoryFontDoctor);
        fprintf(stderr, " Victory Item: %p\n",               Images.Data.rVictoryFontItem);
        fprintf(stderr, " Ally HP: %p\n",                    Images.Data.rAllyHPFont);
        fprintf(stderr, " Portrait HP: %p\n",                Images.Data.rPortraitHPFont);
        fprintf(stderr, " Active Char Name: %p\n",           Images.Data.rActiveCharacterNameFont);
        fprintf(stderr, " New Turn: %p\n",                   Images.Data.rNewTurnFont);
        fprintf(stderr, " Ability Header: %p\n",             Images.Data.rAbilityHeaderFont);
        fprintf(stderr, " Ability Description: %p\n",        Images.Data.rAbilityHeaderFont);
        fprintf(stderr, " Ability Simple Description: %p\n", Images.Data.rAbilitySimpleDescriptionFont);
        fprintf(stderr, " Confirmation Big: %p\n",           Images.Data.rConfirmationFontBig);
        fprintf(stderr, " Confirmation Small: %p\n",         Images.Data.rConfirmationFontSmall);
        fprintf(stderr, " Target Cluster: %p\n",             Images.Data.rTargetClusterFont);
        fprintf(stderr, " Combat Text: %p\n",                Images.Data.rCombatTextFont);
        fprintf(stderr, " Ability Title: %p\n",              Images.Data.rAbilityTitleFont);
        fprintf(stderr, " Effect: %p\n",                     Images.Data.rEffectFont);
        fprintf(stderr, " Prediction: %p\n",                 Images.Data.rPredictionFont);
        fprintf(stderr, " Inspector Heading: %p\n",          Images.Data.rInspectorHeading);
        fprintf(stderr, " Inspector Name: %p\n",             Images.Data.rInspectorNameFont);
        fprintf(stderr, " Inspector Effect: %p\n",           Images.Data.rInspectorEffectFont);
        */
    #endif

    ///--[Base UI]
    const char *cBaseUINames[] = {"BG Gradient", "Card Attack", "Card Job", "Card Memorized", "Card Tactics", "Character Backing", "Character Banner", "Character CP Bar Pip", "Character CP Frame", "Character HP Fill", "Character HP Frame", "Character MP Fill", "Character MP Frame", "Description", "Job Skills Frame", "Memorized Skills Frame", "Tactics Skills Frame"};
    Crossload(&Images.Data.rBGGradient, sizeof(StarBitmap *) * 17, sizeof(StarBitmap *), "Root/Images/AdventureUI/Combat/Base|%s", cBaseUINames, tShowCrossloadDebug);

    ///--[Ally Bar]
    const char *cAllyBarNames[] = {"AllyCPPip", "AllyFrame", "AllyHPFill", "AllyMPFill", "AllyPortraitMask"};
    Crossload(&Images.Data.rAllyCPPip, sizeof(StarBitmap *) * 5, sizeof(StarBitmap *), "Root/Images/AdventureUI/Combat/AllyBar|%s", cAllyBarNames, tShowCrossloadDebug);

    ///--[Defeat Overlay]
    const char *cDefeatNames[] = {"Defeat_0", "Defeat_1", "Defeat_2", "Defeat_3", "Defeat_4", "Defeat_5"};
    Crossload(&Images.Data.rDefeatFrames[0], sizeof(StarBitmap *) * ADVCOMBAT_DEFEAT_FRAMES_TOTAL, sizeof(StarBitmap *), "Root/Images/AdventureUI/Combat/Defeat|%s", cDefeatNames, tShowCrossloadDebug);

    ///--[Enemy Health Bar]
    const char *cEnemyHealthBarNames[] = {"Enemy HP Fill", "Enemy HP Frame", "Enemy Stun Bar Fill", "Enemy Stun Fill", "Enemy Stun Frame"};
    Crossload(&Images.Data.rEnemyHPFill, sizeof(StarBitmap *) * 5, sizeof(StarBitmap *), "Root/Images/AdventureUI/Combat/EnemyHealthBar|%s", cEnemyHealthBarNames, tShowCrossloadDebug);

    ///--[Expert UI]
    const char *cExpertNames[] = {"CP Fill", "HP Fill", "MP Fill", "Page Arrow D", "Page Arrow U", "Page Frame", "Skills Frame", "Statistic Frames Back", "Statistic Frames"};
    Crossload(&Images.Data.rExpertCPFill, sizeof(StarBitmap *) * 9, sizeof(StarBitmap *), "Root/Images/AdventureUI/Combat/Expert|%s", cExpertNames, false);

    //--CP Animation
    Images.Data.rExpertCPAnim[0] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPAnim0");
    Images.Data.rExpertCPAnim[1] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPAnim1");
    Images.Data.rExpertCPAnim[2] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPAnim2");
    Images.Data.rExpertCPAnim[3] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPAnim3");
    Images.Data.rExpertCPAnim[4] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPAnim4");
    Images.Data.rExpertCPAnim[5] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPAnim5");
    Images.Data.rExpertCPAnim[6] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPAnim6");
    Images.Data.rExpertCPAnim[7] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPAnim7");

    //--CP Frame Animation
    Images.Data.rExpertCPFrameAnim[0] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPFrameAnim0");
    Images.Data.rExpertCPFrameAnim[1] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPFrameAnim1");
    Images.Data.rExpertCPFrameAnim[2] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPFrameAnim2");
    Images.Data.rExpertCPFrameAnim[3] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPFrameAnim3");
    //Images.Data.rExpertCPFrameAnim[4] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPFrameAnim4");
    //Images.Data.rExpertCPFrameAnim[5] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPFrameAnim5");
    //Images.Data.rExpertCPFrameAnim[6] = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/CPFrameAnim6");

    ///--[Combat Inspector]
    //--Base.
    const char *cInspectorNames[] = {"AllyCPPip", "AllyHPFill", "AllyMPFill", "Effects Detail", "Effects Frame", "Effects Scrollbar", "Entities Frame", "Entities Scrollbar", "Header", "Resistances Detail", "Resistances Frame", "Summary Frame", "Scrollbar_Front"};
    Crossload(&Images.Data.rInspectorAllyCPPip, sizeof(StarBitmap *) * 13, sizeof(StarBitmap *), "Root/Images/AdventureUI/Combat/Inspector|%s", cInspectorNames, tShowCrossloadDebug);

    //--Inspector Icons
    const char *cInspectorStatusNames[] = {"Health", "Mana", "ManaPerTurn", "Attack", "Initiative", "Accuracy", "Evade", "Threat"};
    Crossload(Images.Data.rInspectorStatusIcons, sizeof(StarBitmap *) * ADVCOMBAT_INSPECTOR_STATUS_ICON_TOTAL, sizeof(StarBitmap *), "Root/Images/AdventureUI/DamageTypeIcons/%s", cInspectorStatusNames, tShowCrossloadDebug);

    const char *cInspectorResistNames[] = {"Protection", "Slashing", "Striking", "Piercing", "Flaming", "Freezing", "Shocking", "Crusading", "Obscuring", "Bleeding", "Poisoning", "Corroding", "Terrifying"};
    Crossload(Images.Data.rInspectorResistIcons, sizeof(StarBitmap *) * ADVCOMBAT_INSPECTOR_RESIST_ICONS_TOTAL, sizeof(StarBitmap *), "Root/Images/AdventureUI/DamageTypeIcons/%s", cInspectorResistNames, tShowCrossloadDebug);

    ///--[Icons]
    Images.Data.rHealthIcon     = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Health");
    Images.Data.rShieldsIcon    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Shield");
    Images.Data.rAdrenalineIcon = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Adrenaline");
    Images.Data.rManaIcon       = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Mana");
    Images.Data.rComboIcon      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/CmbPnt");
    Images.Data.rClockIcon      = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Clock");
    Images.Data.rClockLgIcon    = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIconsLg/Clock");

    ///--[Player Interface]
    const char *cPlayerInterfaceNames[] = {"ArrowLft", "ArrowRgt", "PredictionBox"};
    Crossload(&Images.Data.rArrowLft, sizeof(StarBitmap *) * 3, sizeof(StarBitmap *), "Root/Images/AdventureUI/Combat/PlayerInterface|%s", cPlayerInterfaceNames, tShowCrossloadDebug);

    ///--[Turn Order]
    const char *cTurnOrderNames[] = {"TurnOrderEnemy", "TurnOrderFriendly", "TurnOrderNext"};
    Crossload(&Images.Data.rTurnOrderEnemy, sizeof(StarBitmap *) * 3, sizeof(StarBitmap *), "Root/Images/AdventureUI/Combat/TurnOrder|%s", cTurnOrderNames, tShowCrossloadDebug);

    ///--[Victory]
    const char *cVictoryNames[] = {"BannerBack", "BannerBig", "DoctorBack", "DoctorFill", "DoctorFrame", "DropFrame", "ExpFrameBack", "ExpFrameFill", "ExpFrameFront", "ExpFrameMask"};
    Crossload(&Images.Data.rVictoryBannerBack, sizeof(StarBitmap *) * 10, sizeof(StarBitmap *), "Root/Images/AdventureUI/Combat/Victory|%s", cVictoryNames, tShowCrossloadDebug);

    ///--[Standard]
    Images.Data.rHighlight        = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Standard/Highlight");

    ///--[Verify]
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *), false);
    if(!Images.mIsReady)
    {
        fprintf(stderr, "Error in AdvCombat construction. Report:\n");
        VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *), true);
    }

    ///--[Special Strings]
    mSpecialShieldString->AllocateImages(1);
    mSpecialShieldString->SetImageS(0, 4.0f, "Root/Images/AdventureUI/StatisticIcons/Shields");
    mSpecialAdrenalineString->AllocateImages(1);
    mSpecialAdrenalineString->SetImageS(0, 4.0f, "Root/Images/AdventureUI/StatisticIcons/Adrenaline");
    DebugPop("Finished combat construction, result %i.\n", Images.mIsReady);

    ///--[ ======= Translatable Strings ======= ]
    //--If the strings already exist, deallocate them.
    for(int i = 0; i < COMBAT_TRANSLATABLE_STRINGS_TOTAL; i ++)
    {
        if(AdvString.mem.mPtr[i])
        {
            free(AdvString.mem.mPtr[i]);
            AdvString.mem.mPtr[i] = NULL;
        }
    }

    //--Initialize.
    AdvString.str.mShowUI               = InitializeString("[IMG0] Show UI");
    AdvString.str.mHideUI               = InitializeString("[IMG0] Hide UI");
    AdvString.str.mToggleDescriptions   = InitializeString("[IMG0] Toggle Descriptions");
    AdvString.str.mCycleSkills          = InitializeString("[IMG0][IMG1] Cycle Skills");
    AdvString.str.mAccept               = InitializeString("[IMG0][IMG1] Accept");
    AdvString.str.mCancel               = InitializeString("[IMG0][IMG1] Cancel");
    AdvString.str.mToggleAbilityDetails = InitializeString("[IMG0][IMG1] Toggle Ability Details");
    AdvString.str.mOpenCombatInspector  = InitializeString("[IMG0][IMG1] Open Combat Inspector");
    AdvString.str.mShowHelp             = InitializeString("[IMG0][IMG1] Show Help");
    AdvString.str.mScrollx10            = InitializeString("[IMG0][IMG1] (Hold) Scroll x10");
    AdvString.str.mInspect              = InitializeString("[IMG0][IMG1] Inspect");
    AdvString.str.mToggleResistances    = InitializeString("[IMG0][IMG1] Toggle Resistances/Statistics");
    AdvString.str.mToggleSkillBlock     = InitializeString("[IMG0][IMG1] Toggle Skill Block");

    //--Translate, if available.
    StarTranslation *rTranslation = (StarTranslation *)rDataLibrary->GetEntry(TRANSPATH_COMBAT);
    if(rTranslation)
    {
        for(int i = 0; i < COMBAT_TRANSLATABLE_STRINGS_TOTAL; i ++)
        {
            rTranslation->Translate(AdvString.mem.mPtr[i]);
        }
    }
}

///======================================= Deallocation ===========================================
void AdvCombat::Disassemble()
{
    ///--[Documentation and Setup]
    //--Performs the work of the destructor but does not delete the class.
    DebugPush(true, "Disassembling AdvCombat data.\n");

    ///--[AdvCombat]
    //--System
    DebugPrint(" System.\n");
    free(mDoctorResolvePath);
    free(mStatisticsPath);

    //--Colors
    //--Field Abilities
    DebugPrint(" Field Abilities.\n");
    delete mExtraScripts;

    //--Introduction
    //--Title
    DebugPrint(" Title.\n");
    free(mTitleText);

    //--Turn State
    DebugPrint(" Turn State.\n");
    delete mrTurnOrder;

    //--Action Handling
    DebugPrint(" Action Handling.\n");
    delete mEventQueue;
    delete mApplicationQueue;
    delete mToggleDescriptionString;

    //--Player Input
    delete mHideUIString;
    delete mShowUIString;
    delete mToggleDescriptionsString;
    delete mChangeCardString;

    //--Expert Mode
    //--Target Selection
    DebugPrint(" Target Clusters.\n");
    free(mLastUsedTargetType);
    delete mTargetClusters;

    //--Prediction Boxes
    DebugPrint(" Prediction Boxes.\n");
    delete mPredictionBoxes;

    //--Confirmation Window
    DebugPrint(" Confirmation Window.\n");
    delete mConfirmationString;
    delete mAcceptString;
    delete mCancelString;

    //--Combat Inspector
    DebugPrint(" Combat Inspector.\n");
    delete mrInspectorEffectListing;
    delete mrInspectorEntityList;
    delete mCombatInspectorShow;
    delete mCombatInspectorHelp;
    delete mCombatInspectorScrollFast;
    delete mCombatInspectorEffects;
    delete mCombatInspectorAbilities;
    delete mCombatInspectorToggleResistances;
    delete mCombatInspectorToggleSkills;

    //--Ability Execution
    //--System Abilities
    DebugPrint(" System Abilities.\n");
    free(mVolunteerScript);
    delete mSysAbilityPassTurn;
    delete mSysAbilityRetreat;
    delete mSysAbilitySurrender;
    delete mSysAbilityVolunteer;

    //--Animation Storage
    DebugPrint(" Animation Storage.\n");
    delete mCombatAnimations;
    delete mTextPackages;

    //--Option Flags
    //--Combat State Flags
    DebugPrint(" Post Script Handler.\n");
    free(mPostScriptHandler);

    //--Enemy Storage
    DebugPrint(" Enemy Storage.\n");
    free(mParagonScript);
    free(mEnemyAutoScript);
    free(mDefeatedUnitScript);
    delete mEnemyAliases;
    delete mEnemyRoster;
    delete mrEnemyCombatParty;
    delete mrEnemyReinforcements;
    delete mrEnemyGraveyard;
    delete mWorldReferences;

    //--Party Storage
    DebugPrint(" Party Storage.\n");
    delete mPartyRoster;
    delete mrActiveParty;
    delete mrCombatParty;

    //--Stat Storage for Level Ups
    //--Victory Storage
    DebugPrint(" Items To Award List.\n");
    delete mItemsToAwardList;
    delete mItemsAwardedList;

    //--Effect Storage
    DebugPrint(" Effect Storage.\n");
    delete mSpecialShieldString;
    delete mSpecialAdrenalineString;
    delete mGlobalEffectList;
    delete mEffectGraveyard;
    delete mrTempEffectList;

    //--Cutscene Handlers
    DebugPrint(" Cutscene Handlers.\n");
    delete mCutsceneExecList;
    free(mStandardRetreatScript);
    free(mStandardDefeatScript);
    free(mStandardVictoryScript);
    free(mCurrentRetreatScript);
    free(mCurrentDefeatScript);
    free(mCurrentVictoryScript);
    free(mCombatResponseScript);

    //--Audio
    DebugPrint(" Audio.\n");
    delete mSoundList;
    free(mDefaultCombatMusic);
    free(mDefaultVictoryMusic);
    free(mNextCombatMusic);
    free(mEndCombatMusicResume);

    //--Auto-Win Handler
    DebugPrint(" Auto-win.\n");
    free(mAutoWinPath);

    //--Images
    //--Strings
    DebugPrint(" Strings.\n");
    for(int i = 0; i < COMBAT_TRANSLATABLE_STRINGS_TOTAL; i ++)
    {
        free(AdvString.mem.mPtr[i]);
    }

    //--Debug.
    DebugPop("Done disassembling.\n");
}

///--[Public Statics]
bool AdvCombat::xIsAISpawningEvents = false;

//--Used for diagnosing callouts, this is called when the player clicks a turn portrait in combat.
char *AdvCombat::xDebugCalloutPath = NULL;
