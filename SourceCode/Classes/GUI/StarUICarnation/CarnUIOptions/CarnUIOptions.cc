//--Base
#include "CarnUIOptions.h"

//--Classes
//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///--[Debug]
//#define CARNUIOPTIONS_DEBUG
#ifdef CARNUIOPTIONS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

///========================================== System ==============================================
CarnUIOptions::CarnUIOptions()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_CARNUIOPTIONS;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "COPT");

    ///--[Visiblity]
    mVisibilityTimerMax = 1;

    ///--[Help Menu]
    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ====== CarnUIOptions ======= ]
    ///--[System]
    mPrintRestartRequired = false;
    mIsTitleMode = false;

    ///--[Modes]
    mActiveMode = CMO_MODE_GAME;
    strcpy(mModeNames[CMO_MODE_GAME],  "Game");
    strcpy(mModeNames[CMO_MODE_AUDIO], "Audio");
    strcpy(mModeNames[CMO_MODE_VIDEO], "Video");

    ///--[Options]
    rDraggedOption = NULL;
    rDescriptionPack = NULL;
    mMasterOptionList = new StarLinkedList(true);

    ///--[Resolutions]
    mResolutionsTotal = 0;
    mResolutionStrings = NULL;

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    //--Add the help image structure to the verification list. If a derived type does not want to bother
    //  verifying this or any other structure, they can be removed in a later constructor.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));

    ///--[Hitboxes]
    RegisterHitboxWH("ModeGame",  212.0f, 75.0f, 102.0f, 54.0f);
    RegisterHitboxWH("ModeAudio", 314.0f, 75.0f, 102.0f, 54.0f);
    RegisterHitboxWH("ModeVideo", 416.0f, 75.0f, 102.0f, 54.0f);
    RegisterHitboxWH("ButtonConfirm",  206.0f, 649.0f, 228.0f, 61.0f);
    RegisterHitboxWH("ButtonCancel",   931.0f, 649.0f, 228.0f, 61.0f);
    RegisterHitboxWH("ButtonDefaults", 690.0f, 649.0f, 228.0f, 61.0f);

    //--Options.
    char tBuf[32];
    float cOptionLft = 220.0f;
    float cOptionTop = 178.0f;
    float cOptionRgt = 423.0f;
    float cOptionFrR = 670.0f;
    float cOptionHei =  26.0f;
    for(int i = cxHitbox_OptionStartL; i <= cxHitbox_OptionEndL; i += 2)
    {
        //--Left side.
        sprintf(tBuf, "OptionL%02i", i-cxHitbox_OptionStartL);
        RegisterHitboxWH(tBuf, cOptionLft, cOptionTop, cOptionRgt-cOptionLft, cOptionHei-1);

        //--Right side.
        sprintf(tBuf, "OptionR%02i", i-cxHitbox_OptionStartR);
        RegisterHitboxWH(tBuf, cOptionRgt, cOptionTop, cOptionFrR-cOptionRgt, cOptionHei-1);

        //--Next.
        cOptionTop = cOptionTop + cOptionHei;
    }
}
CarnUIOptions::~CarnUIOptions()
{
    delete mMasterOptionList;
    for(int i = 0; i < mResolutionsTotal; i ++) free(mResolutionStrings[i]);
    free(mResolutionStrings);
}
void CarnUIOptions::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.

    //--Subroutines handle resolving image pointers.
    if(mIsTitleMode)
    {
        ResolveSeriesInto("Title Carnation Options UI", &CarnImages, sizeof(CarnImages));
        ResolveSeriesInto("Title Carnation Common UI",  &CarnImagesCommon, sizeof(CarnImagesCommon));
        ResolveSeriesInto("Title Adventure Help UI",    &HelpImages, sizeof(HelpImages));
    }
    else
    {
        ResolveSeriesInto("Carnation Options UI", &CarnImages, sizeof(CarnImages));
        ResolveSeriesInto("Carnation Common UI",  &CarnImagesCommon, sizeof(CarnImagesCommon));
        ResolveSeriesInto("Adventure Help UI",    &HelpImages, sizeof(HelpImages));
    }

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();
}

///===================================== Property Queries =========================================
bool CarnUIOptions::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_STARUIPIECE) return true;
    if(pType == POINTER_TYPE_CARNUIOPTIONS) return true;
    return false;
}
float CarnUIOptions::ComputeBarPosition(TwoDimensionReal *pHitboxR)
{
    ///--[Documentation]
    //--Returns the left position of the bar used to show floating point values. This is used in two
    //  spots in the code so it is standardized here.
    if(!mImagesReady || !pHitboxR) return 0.0f;

    //--Compute.
    return (pHitboxR->mLft + ((pHitboxR->GetWidth()  - CarnImages.rOverlay_ValueBar->GetWidth())  / 2.0f));
}

///======================================= Manipulators ===========================================
void CarnUIOptions::TakeForeground()
{
    ///--[Documentation]
    //--Called when the object takes the foreground. Reset to gameplay options, store all options in
    //  case the player cancels out.
    mIsForegroundObject = true;
    mActiveMode = CMO_MODE_GAME;
    rDraggedOption = NULL;
    rDescriptionPack = NULL;

    //--Call construction function.
    ConstructOptions();
    RefreshOptions();
    mVisibilityTimer = mVisibilityTimerMax;
}
void CarnUIOptions::SetTitleMode()
{
    mIsTitleMode = true;
}

///======================================= Core Methods ===========================================
void CarnUIOptions::RefreshMenuHelp()
{
}
void CarnUIOptions::RecomputeCursorPositions()
{
}

///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIOptions::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ========== Documentation =========== ]
    //--Handles updates when this is the active object.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[ ============== Timers ============== ]
    //--Immediately cap the vis timer.
    mVisibilityTimer = mVisibilityTimerMax;

    ///--[ ========== Mouse Handling ========== ]
    //--Retrieve the mouse coordinates. Subroutines can then use them freely.
    GetMouseInfo();
    RunHitboxCheck();

    ///--[ ============== Update ============== ]
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Drag Handling]
    //--If there is a drag target:
    if(rDraggedOption)
    {
        //--Mouse is still down.
        if(rControlManager->IsDown("MouseLft"))
        {
            //--Set the floating point value according to the X position. Note that we use the first
            //  hitbox here, because the Y position does not matter.
            TwoDimensionReal *rHitboxR = (TwoDimensionReal *)mHitboxList->GetElementBySlot(cxHitbox_OptionStartR);
            float tBarLft = ComputeBarPosition(rHitboxR);

            //--Get and clamp the value. First, get the X position on the bar. It clamps from 0 to 1.
            float tBarVal = (mMouseX - tBarLft) / (float)CarnImages.rOverlay_ValueBar->GetWidthSafe();
            if(tBarVal < 0.0f) tBarVal = 0.0f;
            if(tBarVal > 1.0f) tBarVal = 1.0f;

            //--Next, compute the float position based on its ranges.
            rDraggedOption->mCurValue.f = rDraggedOption->mRangeLo + ((rDraggedOption->mRangeHi - rDraggedOption->mRangeLo) * tBarVal);
        }
        //--Release.
        else
        {
            rDraggedOption = NULL;
        }

        //--Stop update.
        return true;
    }

    ///--[Mouse Over]
    //--Can change the description when mousing over options.
    HandleMouseOver();

    ///--[Left Click]
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        HandleLeftClick();
        return true;
    }

    ///--[Right Click / Cancel]
    //--Player right-clicks. Return to active selection mode.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        HandleRightClick();
        return true;
    }

    return true;
}
void CarnUIOptions::UpdateBackground()
{
    ///--[ ============== Timers ============== ]
    //--Immediately cap the vis timer.
    if(!mIsForegroundObject) mVisibilityTimer = 0;
}
void CarnUIOptions::HandleMouseOver()
{
    ///--[Documentation]
    //--Handles mousing over an option, which changes the description.

    ///--[Option Checking]
    //--Get the active list.
    StarLinkedList *rActiveOptionList = (StarLinkedList *)mMasterOptionList->GetElementBySlot(mActiveMode);
    if(!rActiveOptionList) return;

    //--Get which option the click was on.
    int tReply = CheckRepliesWithin(cxHitbox_OptionStartL, cxHitbox_OptionEndR);
    if(tReply != -1)
    {
        //--Resolve the option in question.
        int tIndex = (tReply - cxHitbox_OptionStartL) / 2;
        CarnMenuOptionPack *rOptionPack = (CarnMenuOptionPack *)rActiveOptionList->GetElementBySlot(tIndex);
        if(rOptionPack)
        {
            //--SFX.
            //if(rDescriptionPack != rOptionPack) AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Set.
            rDescriptionPack = rOptionPack;
        }

        //--Stop.
        return;
    }
}
void CarnUIOptions::HandleLeftClick()
{
    ///--[Documentation]
    //--Handles the left-click case. Fairly involved.

    //--Common functions. If a tab was clicked, switch modes.
    if(!mIsTitleMode)
    {
        if(CommonTabClickUpdate())
        {
            mIsForegroundObject = false;
            return;
        }
    }

    ///--[Option Checking]
    //--Get the active list.
    StarLinkedList *rActiveOptionList = (StarLinkedList *)mMasterOptionList->GetElementBySlot(mActiveMode);
    if(!rActiveOptionList) return;

    //--Get which option the click was on.
    int tReply = CheckRepliesWithin(cxHitbox_OptionStartL, cxHitbox_OptionEndR);
    if(tReply != -1)
    {
        //--Ignore even number clicks, those are on the left side of the option.
        if(tReply % 2 != 0)
        {
            //--Resolve the option in question.
            int tIndex = (tReply - cxHitbox_OptionStartR) / 2;
            CarnMenuOptionPack *rOptionPack = (CarnMenuOptionPack *)rActiveOptionList->GetElementBySlot(tIndex);
            if(rOptionPack)
            {
                //--Boolean. Toggle.
                if(rOptionPack->mEditType == CMOP_BOOLEAN)
                {
                    rOptionPack->mCurValue.b = !rOptionPack->mCurValue.b;
                }
                //--Integer. Count up to max.
                else if(rOptionPack->mEditType == CMOP_INTRANGE)
                {
                    if(rOptionPack->mCurValue.i < rOptionPack->mRangeHi) rOptionPack->mCurValue.i ++;
                }
                //--Floating point. Activate dragging.
                else
                {
                    rDraggedOption = rOptionPack;
                }
            }
        }

        //--Stop.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Mode Checking]
    tReply = CheckRepliesWithin(cxHitbox_ModeGame, cxHitbox_ModeVideo);
    if(tReply != -1)
    {
        //--Gameplay options.
        if(tReply == cxHitbox_ModeGame)
        {
            mActiveMode = CMO_MODE_GAME;
        }
        //--Audio options.
        else if(tReply == cxHitbox_ModeAudio)
        {
            mActiveMode = CMO_MODE_AUDIO;
        }
        //--Video options.
        else if(tReply == cxHitbox_ModeVideo)
        {
            mActiveMode = CMO_MODE_VIDEO;
        }

        //--Clicking any of these clears the description.
        rDescriptionPack = NULL;

        //--Stop.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }

    ///--[Button Checking]
    tReply = CheckRepliesWithin(cxHitbox_ButtonConfirm, cxHitbox_ButtonDefaults);
    if(tReply != -1)
    {
        //--Confirm button.
        if(tReply == cxHitbox_ButtonConfirm)
        {
            SaveOptions();
            FlagExit();
        }
        //--Cancel. Exits without saving anything.
        else if(tReply == cxHitbox_ButtonCancel)
        {
            FlagExit();
        }
        //--Defaults. Resets everything to default, does not exit.
        else if(tReply == cxHitbox_ButtonDefaults)
        {
            DefaultOptions();
        }

        //--Stop.
        AudioManager::Fetch()->PlaySound("Menu|Select");
        return;
    }
}
void CarnUIOptions::HandleRightClick()
{
    ///--[Documentation]
    //--Handles the right-click case. Fairly involved.

    ///--[Option Checking]
    //--Get the active list.
    StarLinkedList *rActiveOptionList = (StarLinkedList *)mMasterOptionList->GetElementBySlot(mActiveMode);
    if(!rActiveOptionList) return;

    //--Get which option the click was on.
    int tReply = CheckRepliesWithin(cxHitbox_OptionStartL, cxHitbox_OptionEndR);
    if(tReply != -1)
    {
        //--Ignore even number clicks, those are on the left side of the option.
        if(tReply % 2 != 0)
        {
            //--Resolve the option in question.
            int tIndex = (tReply - cxHitbox_OptionStartR) / 2;
            CarnMenuOptionPack *rOptionPack = (CarnMenuOptionPack *)rActiveOptionList->GetElementBySlot(tIndex);
            if(rOptionPack)
            {
                //--Boolean. Toggle.
                if(rOptionPack->mEditType == CMOP_BOOLEAN)
                {
                    rOptionPack->mCurValue.b = !rOptionPack->mCurValue.b;
                }
                //--Integer. Count up to max.
                else if(rOptionPack->mEditType == CMOP_INTRANGE)
                {
                    if(rOptionPack->mCurValue.i > rOptionPack->mRangeLo) rOptionPack->mCurValue.i --;
                }
                //--Floating point. Does nothing.
                else
                {

                }
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIOptions::RenderForeground(float pVisAlpha)
{
    ///--[Documentation]
    //--Rendering when the object is the front object. The caller may optionally modify the alpha,
    //  which may be handled as a fade mixing or as a translation. See HandlePositioning().
    //--This is the standard rendering cycle. It does error checks, calls RenderPieces(), and then
    //  cleans up.

    ///--[Visibility Checks]
    //--Images failed to resolve. Do nothing.
    if(!mImagesReady) return;

    //--Caller set the visibility at zero, do nothing.
    if(pVisAlpha <= 0.0f) return;

    //--Render nothing if everything is offscreen.
    if(mVisibilityTimer < 1) return;

    ///--[Call Pieces Rendering]
    //--Compute how on-screen this object is by calculating the alpha from the vis timer. Multiply
    //  with the provided alpha so the host doesn't fade differently from sub-components.
    float cLocalAlpha = 1.0f;

    //--Call subroutine. Can be overriden.
    RenderPieces(cLocalAlpha);

    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void CarnUIOptions::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--Carnation does not use by-side rendering, though it still uses piece rendering for subsections.
    //  Carnation also does not use any alphas except the primary one, UI pieces instantly switch.
    DebugPush(true, "CarnUIOptions:RenderPieces() - Begin.\n");

    //--Visibility check.
    //if(mVisibilityTimer < 1) { DebugPop("Not visible. Stopping.\n"); return; }

    ///--[Common Parts]
    //--Subroutines.
    if(!mIsTitleMode) RenderTopTabs(mCodeBackward);

    ///--[Static Parts]
    //--Base.
    CarnImages.rFrame_Base->Draw();

    //--Render a selection box for each options page.
    for(int i = 0; i < CMO_MODE_TOTAL; i ++)
    {
        //--Get and check the position.
        TwoDimensionReal *rHitbox = (TwoDimensionReal *)mHitboxList->GetElementBySlot(cxHitbox_ModeGame + i);
        if(!rHitbox) continue;

        //--Backing.
        CarnImages.rOverlay_MenuSelection->Draw(rHitbox->mLft, rHitbox->mTop);

        //--Name.
        CarnImages.rFont_Mainline->DrawText(rHitbox->mXCenter, rHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, mModeNames[i]);

        //--If this mode is selected, render a heading.
        if(mActiveMode == i)
        {
            char tBuf[128];
            sprintf(tBuf, "%s Options", mModeNames[i]);
            CarnImages.rFont_Heading->DrawText(218.0f, 143.0f, 0, 1.0f, tBuf);
        }
    }

    //--Confirm button.
    TwoDimensionReal *rConfirmHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("ButtonConfirm");
    if(rConfirmHitbox)
    {
        CarnImages.rFrame_Button->Draw(rConfirmHitbox->mLft, rConfirmHitbox->mTop);
        CarnImages.rFont_Button->DrawText(rConfirmHitbox->mXCenter, rConfirmHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Confirm");
    }

    //--Cancel button.
    TwoDimensionReal *rCancelHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("ButtonCancel");
    if(rCancelHitbox)
    {
        CarnImages.rFrame_Button->Draw(rCancelHitbox->mLft, rCancelHitbox->mTop);
        CarnImages.rFont_Button->DrawText(rCancelHitbox->mXCenter, rCancelHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Cancel");
    }

    //--Defaults button.
    TwoDimensionReal *rDefaultsHitbox = (TwoDimensionReal *)mHitboxList->GetElementByName("ButtonDefaults");
    if(rDefaultsHitbox)
    {
        CarnImages.rFrame_Button->Draw(rDefaultsHitbox->mLft, rDefaultsHitbox->mTop);
        CarnImages.rFont_Button->DrawText(rDefaultsHitbox->mXCenter, rDefaultsHitbox->mYCenter, SUGARFONT_AUTOCENTER_XY, 1.0f, "Defaults");
    }

    //--Help text.
    CarnImages.rFont_Mainline->DrawText(207.0f, 594.0f, 0, 1.0f, "Left click to increase option.");
    CarnImages.rFont_Mainline->DrawText(207.0f, 620.0f, 0, 1.0f, "Right click to decrease option.");

    //--Restart required.
    if(mPrintRestartRequired)
    {
        CarnImages.rFont_Mainline->DrawText(439.0f, 650.0f, 0, 1.0f, "Some options require a");
        CarnImages.rFont_Mainline->DrawText(439.0f, 676.0f, 0, 1.0f, "restart to take effect.");
    }

    ///--[Options]
    //--Get the selected options list.
    StarLinkedList *rActiveOptionsList = (StarLinkedList *)mMasterOptionList->GetElementBySlot(mActiveMode);
    if(rActiveOptionsList)
    {
        //--The list contains a set of CarnMenuOptionPacks which a subroutine renders.
        int i = 0;
        CarnMenuOptionPack *rOptionPack = (CarnMenuOptionPack *)rActiveOptionsList->PushIterator();
        while(rOptionPack)
        {
            //--Get associated hitboxs.
            TwoDimensionReal *rHitboxL = (TwoDimensionReal *)mHitboxList->GetElementBySlot(cxHitbox_OptionStartL + (i*2));
            TwoDimensionReal *rHitboxR = (TwoDimensionReal *)mHitboxList->GetElementBySlot(cxHitbox_OptionStartR + (i*2));

            //--Call routine.
            RenderOption(rOptionPack, rHitboxL, rHitboxR);

            //--Next.
            i ++;
            rOptionPack = (CarnMenuOptionPack *)rActiveOptionsList->AutoIterate();
        }
    }

    ///--[Description]
    if(rDescriptionPack)
        RenderDescription(695.0f, 78.0f, 26.0f, rDescriptionPack->mDescriptionList, CarnImages.rFont_Mainline);

    ///--[Finish Up]
    //--Diagnostics.
    DebugPop("Completed normally.\n");
}
void CarnUIOptions::RenderOption(CarnMenuOptionPack *pOptionPack, TwoDimensionReal *pHitboxL, TwoDimensionReal *pHitboxR)
{
    ///--[Documentation]
    //--Renders an options pack, the name on the left and the values on the right.
    if(!pOptionPack || !pHitboxL || !pHitboxR) return;

    ///--[Name]
    if(pOptionPack->mDisplayName)
    {
        CarnImages.rFont_Mainline->DrawText(pHitboxL->mLft, pHitboxL->mYCenter, SUGARFONT_AUTOCENTER_Y, 1.0f, pOptionPack->mDisplayName);
    }

    ///--[Boolean Version]
    //--Just true and false.
    if(pOptionPack->mEditType == CMOP_BOOLEAN)
    {
        if(pOptionPack->mCurValue.b == true)
        {
            CarnImages.rFont_Mainline->DrawText(pHitboxR->mRgt, pHitboxR->mYCenter, SUGARFONT_AUTOCENTER_Y | SUGARFONT_RIGHTALIGN_X, 1.0f, "True");
        }
        else
        {
            CarnImages.rFont_Mainline->DrawText(pHitboxR->mRgt, pHitboxR->mYCenter, SUGARFONT_AUTOCENTER_Y | SUGARFONT_RIGHTALIGN_X, 1.0f, "False");
        }
    }
    ///--[Integer Version]
    else if(pOptionPack->mEditType == CMOP_INTRANGE)
    {
        //--If there is no options string list, just print the number.
        if(!pOptionPack->mValueStrings)
        {
            CarnImages.rFont_Mainline->DrawTextArgs(pHitboxR->mRgt, pHitboxR->mYCenter, SUGARFONT_AUTOCENTER_Y | SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pOptionPack->mCurValue.i);
        }
        //--If there is, check if the given integer value has a matching string.
        else
        {
            //--Get the string. If it doesn't exist or is "Null" then print the number.
            const char *rString = pOptionPack->mValueStrings->GetNameOfElementBySlot(pOptionPack->mCurValue.i);
            if(!rString || !strcasecmp(rString, "NULL"))
            {
                CarnImages.rFont_Mainline->DrawTextArgs(pHitboxR->mRgt, pHitboxR->mYCenter, SUGARFONT_AUTOCENTER_Y | SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pOptionPack->mCurValue.i);
            }
            //--Print the associated string value.
            else
            {
                CarnImages.rFont_Mainline->DrawText(pHitboxR->mRgt, pHitboxR->mYCenter, SUGARFONT_AUTOCENTER_Y | SUGARFONT_RIGHTALIGN_X, 1.0f, rString);
            }
        }
    }
    ///--[Floating-point Version]
    else if(pOptionPack->mEditType == CMOP_BAR)
    {
        //--Render the backing bar. Center it in the hitbox.
        float tBarLft = ComputeBarPosition(pHitboxR);
        float tBarTop = pHitboxR->mTop + ((pHitboxR->GetHeight() - CarnImages.rOverlay_ValueBar->GetHeight()) / 2.0f);
        CarnImages.rOverlay_ValueBar->Draw(tBarLft, tBarTop);

        //--Draw the pip.
        float tPipLft = tBarLft + (CarnImages.rOverlay_ValueBar->GetWidth() * pOptionPack->mCurValue.f);
        CarnImages.rOverlay_ValuePip->Draw(tPipLft, tBarTop - 6.0f);

        //--Render the actual value on the right side between 0 and 1.
        CarnImages.rFont_Mainline->DrawTextArgs(pHitboxR->mRgt, pHitboxR->mYCenter, SUGARFONT_AUTOCENTER_Y | SUGARFONT_RIGHTALIGN_X, 1.0f, "%5.2f", pOptionPack->mCurValue.f);
    }

    //--Render the hitbox.
    if(false)
    {
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_LINE_LOOP);
            glVertex2f(pHitboxL->mLft, pHitboxL->mTop);
            glVertex2f(pHitboxL->mRgt, pHitboxL->mTop);
            glVertex2f(pHitboxL->mRgt, pHitboxL->mBot);
            glVertex2f(pHitboxL->mLft, pHitboxL->mBot);
        glEnd();
        glBegin(GL_LINE_LOOP);
            glVertex2f(pHitboxR->mLft, pHitboxR->mTop);
            glVertex2f(pHitboxR->mRgt, pHitboxR->mTop);
            glVertex2f(pHitboxR->mRgt, pHitboxR->mBot);
            glVertex2f(pHitboxR->mLft, pHitboxR->mBot);
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
