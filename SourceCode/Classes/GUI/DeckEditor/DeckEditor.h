///======================================== DeckEditor ============================================
//--Used in Text Adventure game modes where the combat uses the card deck system. This allows the
//  player to customize their card deck. The results are punted to Lua when done and do not interact
//  directly with the combat system.

#pragma once

///========================================= Includes =============================================
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

///===================================== Local Structures =========================================
//--Represents an object the player can click on the interface.
typedef struct DeckEditorButton
{
    bool mIsDisabled;
    bool mIsPressed;
    TwoDimensionReal mDimensions;
    char mText[STD_MAX_LETTERS];
}DeckEditorButton;

//--Represents a type of card, depicting how many there are of that card, maximum, type, etc.
typedef struct DeckEditorHand
{
    char mTitle[STD_MAX_LETTERS];
    int mCurrent;
    int mMaximum;
    int mMinimum;
    TwoDimensionReal mPosition;
    StarBitmap *rCardImg;
}DeckEditorHand;

///===================================== Local Definitions ========================================
//--Visibility
#define DE_VIS_TICKS 25

//--Hand Listing
#define DE_HAND_WATER 0
#define DE_HAND_FIRE 1
#define DE_HAND_WIND 2
#define DE_HAND_EARTH 3
#define DE_HAND_LIFE 4
#define DE_HAND_DEATH 5
#define DE_HAND_ATTACK 6
#define DE_HAND_DEFEND 7
#define DE_HAND_BRIDGEAND 8
#define DE_HAND_BRIDGEOF 9
#define DE_HAND_TOTAL 10

//--Buttons
#define DE_BTN_ACCEPT 0
#define DE_BTN_CANCEL 1
#define DE_BTN_DEFAULTS 2
#define DE_BTN_EXIT_TOTAL 3
#define DE_BTN_ADDSUB_START 3
#define DE_BTN_WATERADD 3
#define DE_BTN_WATERSUB 4
#define DE_BTN_FIREADD 5
#define DE_BTN_FIRESUB 6
#define DE_BTN_WINDADD 7
#define DE_BTN_WINDSUB 8
#define DE_BTN_EARTHADD 9
#define DE_BTN_EARTHSUB 10
#define DE_BTN_LIFEADD 11
#define DE_BTN_LIFESUB 12
#define DE_BTN_DEATHADD 13
#define DE_BTN_DEATHSUB 14
#define DE_BTN_ATTACKADD 15
#define DE_BTN_ATTACKSUB 16
#define DE_BTN_DEFENDADD 17
#define DE_BTN_DEFENDSUB 18
#define DE_BTN_BRIDGEANDADD 19
#define DE_BTN_BRIDGEANDSUB 20
#define DE_BTN_BRIDGEOFADD 21
#define DE_BTN_BRIDGEOFSUB 22
#define DE_BTN_ADDSUB_END 22
#define DE_BTN_HELP_START 23
#define DE_BTN_HELP_DECKSIZE 23
#define DE_BTN_HELP_WATER 24
#define DE_BTN_HELP_FIRE 25
#define DE_BTN_HELP_WIND 26
#define DE_BTN_HELP_EARTH 27
#define DE_BTN_HELP_LIFE 28
#define DE_BTN_HELP_DEATH 29
#define DE_BTN_HELP_ATTACK 30
#define DE_BTN_HELP_DEFEND 31
#define DE_BTN_HELP_BRIDGEAND 32
#define DE_BTN_HELP_BRIDGEOF 33
#define DE_BTN_HELP_STATS 34
#define DE_BTN_HELP_ELEMENTBONUS 35
#define DE_BTN_HELP_END 35
#define DE_BTN_TOTAL 36

#define DE_BTN_HAND_START DE_BTN_WATERADD
#define DE_BTN_HAND_BTNS 2
#define DE_BTN_HAND_BTN_ADD 0
#define DE_BTN_HAND_BTN_SUB 1

//--Stat Lookups
#define DE_STAT_ATTACK 0
#define DE_STAT_DEFEND 1
#define DE_STAT_STARTSHIELD 2
#define DE_STAT_WATERBONUS 3
#define DE_STAT_FIREBONUS 4
#define DE_STAT_WINDBONUS 5
#define DE_STAT_EARTHBONUS 6
#define DE_STAT_LIFEBONUS 7
#define DE_STAT_DEATHBONUS 8

//--Help Properties
#define DE_HELP_MAXLINES 13

///========================================== Classes =============================================
class DeckEditor : public RootObject
{
    private:
    //--System
    bool mIsActive;
    char *mPostExecScript;

    //--Visibility
    bool mIsVisible;
    int mVisibilityTimer;

    //--Help Handler
    bool mHelpVisible;
    int mHelpVisibleTimer;
    char *mHelpExecPath;
    char *mHelpHeader;
    char *mHelpStrings[DE_HELP_MAXLINES];

    //--Buttons and Positions
    DeckEditorHand mHands[DE_HAND_TOTAL];
    DeckEditorButton mButtons[DE_BTN_TOTAL];

    //--Deck Statistics
    int mDeckSizeMin;
    int mDeckSizeMax;

    //--Other Strings/Values
    int mAttackBonus;
    int mDefendBonus;
    int mStartShieldBonus;
    char mSentenceLenBonus[STD_MAX_LETTERS];
    int mBonusWater;
    int mBonusFire;
    int mBonusWind;
    int mBonusEarth;
    int mBonusLife;
    int mBonusDeath;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            StarFont *rFontHeader;
            StarFont *rFontMainLg;
            StarFont *rFontMainSm;
            StarFont *rFontButton;

            //--UI Parts
            StarBitmap *rBackground;
            StarBitmap *rHelpInlay;
            StarBitmap *rButtonCancel;
            StarBitmap *rButtonReset;
            StarBitmap *rButtonSave;
            StarBitmap *rButtonSmallAdd;
            StarBitmap *rButtonSmallSub;
            StarBitmap *rButtonSmallHlp;

            //--Lookups
            StarBitmap *rExitBtnLookups[DE_BTN_EXIT_TOTAL];
        }Data;
    }Images;

    protected:

    public:
    //--System
    DeckEditor();
    virtual ~DeckEditor();
    void Construct();

    //--Public Variables
    //--Property Queries
    bool IsActive();
    int GetHandCount(int pType);
    int GetDeckSize();

    //--Manipulators
    void Activate();
    void Deactivate();
    void SetPostExec(const char *pPath);
    void SetHelpExec(const char *pPath);
    void SetButtonProperties(int pSlot, float pLft, float pTop, float pWid, float pHei, const char *pText);
    void SetHandBasicProperties(int pSlot, float pLft, float pTop, float pWid, float pHei, const char *pText, StarBitmap *pImg);
    void SetHandCounters(int pSlot, int pCurrent, int pMinimum, int pMaximum);
    void SetDeckMinMaxSize(int pMinSize, int pMaxSize);
    void SetBonus(int pType, int pValue);
    void SetSentenceLenBonus(const char *pValue);
    void SetHelpHeader(const char *pText);
    void SetHelpString(int pLine, const char *pText);

    //--Core Methods
    void HandleButtonClick(int pSlot);
    void ActivateHelpMode(int pButtonSlot);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

