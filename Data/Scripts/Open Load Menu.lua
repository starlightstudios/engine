-- |[ ===================================== Open Load Menu ===================================== ]|
--Switches the menu to loading mode. Note that loading mode can attempt to open savefiles from games
-- that may not be supported, no engine configuration checks this!
MapM_PushMenuStackHead()
	FlexMenu_ActivateLoading()
DL_PopActiveObject()
