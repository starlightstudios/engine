//--Base
#include "PeakFreaksLevel.h"

//--Classes
#include "TileLayer.h"

//--CoreClasses
#include "VirtualFile.h"

//--Definitions
//--Libraries
//--Managers
#include "MapManager.h"
#include "SugarLumpManager.h"

///========================================= Lua Hooking ==========================================
void PeakFreaksLevel::HookToLuaState(lua_State *pLuaState)
{
    /* PeakLev_Create()
       Creates a new PeakFreaksLevel and gives it to the MapManager. */
    lua_register(pLuaState, "PeakLev_Create", &Hook_PeakLev_Create);

    /* PeakLev_ParseSLF(sSLFPath)
       Takes an active PeakFreaksLevel and parses the given SLF path's data into a usable level. */
    lua_register(pLuaState, "PeakLev_ParseSLF", &Hook_PeakLev_ParseSLF);

    /* PeakLev_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the PeakFreaksLevel class. */
    lua_register(pLuaState, "PeakLev_GetProperty", &Hook_PeakLev_GetProperty);

    /* PeakLev_SetProperty("Set Tutorial")
       PeakLev_SetProperty("Set Trigger Script", sPath)
       PeakLev_SetProperty("Spawn Player")
       PeakLev_SetProperty("Set Player Position", fX, fY)
       Sets the property in the PeakFreaksLevel class. */
    lua_register(pLuaState, "PeakLev_SetProperty", &Hook_PeakLev_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_PeakLev_Create(lua_State *L)
{
    //PeakLev_Create()
    PeakFreaksLevel *nLevel = new PeakFreaksLevel();
    MapManager::Fetch()->ReceiveLevel(nLevel);
    return 0;
}
int Hook_PeakLev_ParseSLF(lua_State *L)
{
    ///--[Argument Check]
    //PeakLev_ParseSLF(sSLFPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PeakLev_ParseSLF");

    ///--[Active Object Check]
    //--Get and check.
    PeakFreaksLevel *rCheckLevel = PeakFreaksLevel::Fetch();
    if(!rCheckLevel) return LuaTypeError("PeakLev_ParseSLF", L);

    ///--[Constants]
    //--Size setters.
    TileLayer::cxSizePerTile = 16.0f;
    SugarLumpManager::xTileSizeX = 16;
    SugarLumpManager::xTileSizeY = 16;

    ///--[Execution]
    //--Open the SLF file. PeakFreaksLevels must *always* be opened in memory mode.
    bool tOldFlag = VirtualFile::xUseRAMLoading;
    VirtualFile::xUseRAMLoading = true;
    SugarLumpManager *rSLM = SugarLumpManager::Fetch();
    rSLM->Open(lua_tostring(L, 1));

    //--Instruction.
    rCheckLevel->ParseFile(rSLM);

    ///--[Finish Up]
    //--Purge all remaining SLF data.
    rSLM->Close();

    //--Reset flag.
    VirtualFile::xUseRAMLoading = tOldFlag;
    return 0;
}
int Hook_PeakLev_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //PeakLev_GetProperty("Dummy") (1 Integer)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PeakLev_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("PeakLev_GetProperty", L);

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("PeakLev_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_PeakLev_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //PeakLev_SetProperty("Set Tutorial")
    //PeakLev_SetProperty("Activate Tutorial A")
    //PeakLev_SetProperty("Activate Tutorial B")
    //PeakLev_SetProperty("Set Trigger Script", sPath)
    //PeakLev_SetProperty("Spawn Player")
    //PeakLev_SetProperty("Spawn Rival")
    //PeakLev_SetProperty("Set Player Position", fX, fY)
    //PeakLev_SetProperty("Reconstitute")
    //PeakLev_SetProperty("Spawn Powerups")

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PeakLev_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    PeakFreaksLevel *rActiveLevel = PeakFreaksLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("PeakLev_SetProperty", L);

    ///--[System]
    //--Marks this as a tutorial level, which changes generation.
    if(!strcasecmp(rSwitchType, "Set Tutorial") && tArgs == 1)
    {
        rActiveLevel->SetTutorial();
        rActiveLevel->ClearOpening();
    }
    //--Tutorial A: Movement.
    else if(!strcasecmp(rSwitchType, "Activate Tutorial A") && tArgs == 1)
    {
        rActiveLevel->ActivateTutorialA();
    }
    //--Tutorial B: Crushing.
    else if(!strcasecmp(rSwitchType, "Activate Tutorial B") && tArgs == 1)
    {
        rActiveLevel->ActivateTutorialB();
    }
    //--Sets the trigger script to fire when a trigger is tripped.
    else if(!strcasecmp(rSwitchType, "Set Trigger Script") && tArgs == 2)
    {
        rActiveLevel->SetTriggerScript(lua_tostring(L, 2));
    }
    //--Causes player to spawn if they haven't already.
    else if(!strcasecmp(rSwitchType, "Spawn Player") && tArgs == 1)
    {
        rActiveLevel->SpawnPlayer();
    }
    //--Causes the rival to spawn if they haven't already.
    else if(!strcasecmp(rSwitchType, "Spawn Rival") && tArgs == 1)
    {
        rActiveLevel->SpawnRival();
    }
    //--Sets the player's position.
    else if(!strcasecmp(rSwitchType, "Set Player Position") && tArgs == 3)
    {
        rActiveLevel->PositionPlayer(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Reconstitute's a randomly generated level.
    else if(!strcasecmp(rSwitchType, "Reconstitute") && tArgs == 1)
    {
        rActiveLevel->Reconstitute();
    }
    //--Spawn powerups after reconstituting the level.
    else if(!strcasecmp(rSwitchType, "Spawn Powerups") && tArgs == 1)
    {
        rActiveLevel->SpawnPowerups();
    }
    //--Error case.
    else
    {
        LuaPropertyError("PeakLev_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
