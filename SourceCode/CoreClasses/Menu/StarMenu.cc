//--Base
#include "StarMenu.h"

//--Classes
#include "StarButton.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

///========================================== System ==============================================
StarMenu::StarMenu()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_MENU_ROOT;

    ///--[StarMenu]
    //--System
    mIsPendingClose = false;

    //--Buttons
    mButtonList = new StarLinkedList(true);
}
StarMenu::~StarMenu()
{
    delete mButtonList;
}

///===================================== Property Queries =========================================
bool StarMenu::HasPendingClose()
{
    return mIsPendingClose;
}

///======================================== Manipulators ==========================================
void StarMenu::Clear()
{
    mButtonList->ClearList();
}
void StarMenu::SetClosingFlag(bool pFlag)
{
    mIsPendingClose = pFlag;
}
void StarMenu::RegisterButton(StarButton *pButton)
{
    if(!pButton) return;
    mButtonList->AddElement(pButton->GetName(), pButton, StarButton::DeleteThis);
}

///======================================== Core Methods ==========================================
///==================================== Private Core Methods ======================================
///=========================================== Update =============================================
///========================================== File I/O ============================================
///========================================== Drawing =============================================
///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
