///======================================= MonoUISkills ===========================================
//--UI object that encapsulates the Monoceros Skills menu.

#pragma once

///========================================= Includes =============================================
#include "AdvUISkills.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///======================================== Translation ===========================================
///--[MonoUIBase_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUISKILLS_STRINGS_TOTAL 21
class MonoUISkills_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUISKILLS_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextSkills;
            char *mTextHelpMenu;
            char *mTextCharactersSkill;
            char *mTextHelp[7];
            char *mTextControlPurchaseSkills;
            char *mTextControlShowHelp;
            char *mTextControlScroll10;
            char *mTextControlEditProfiles;
            char *mTextControlShowSkillDetails;
            char *mTextControlInspectSkills;
            char *mTextControlPrevChar;
            char *mTextControlNextChar;
            char *mTextControlMemorizeSkill;
            char *mTextControlDeleteProfile;
            char *mTextControlCancel;
        }str;
    };

    //--Functions
    MonoUISkills_Strings();
    virtual ~MonoUISkills_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUISkills : public MonoUICommon, virtual public AdvUISkills
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxMonoHelpStrings = 7;
    static const int cxMono_MaxDescLines = 8;

    //--Scrollbar.
    static const int cxMonoScroll_JobBuf  =  3;
    static const int cxMonoScroll_JobPage = 11;

    //--Sizes

    ///--[Variables]
    StarLinkedList *mCurrentSkillsList; //AdvCombatAbility *, reference

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFontDoubleHeader;
        StarFont *rFontHeader;
        StarFont *rFontHelp;
        StarFont *rFontMainline;
        StarFont *rFontControl;
        StarFont *rFontDescription;

        //--Images for this UI
        StarBitmap *rArrowLft;
        StarBitmap *rArrowRgt;
        StarBitmap *rFrameDescription;
        StarBitmap *rFrameSkills;
        StarBitmap *rOverlayCharNameBack;
        StarBitmap *rOverlaySkillBack;
        StarBitmap *rOverlaySkillHighlight;
        StarBitmap *rOverlayTitleShade;
        StarBitmap *rScrollbarBack;
        StarBitmap *rScrollbarScroller;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUISkills();
    virtual ~MonoUISkills();
    virtual void Construct();

    //--Public Variables
    static MonoUISkills_Strings *xrStringData;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();
    void ClearSkillsList();
    void AddSkillReference(const char *pName);

    //--Core Methods
    void PopulateSkillsBySelectedCharacter();
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    void RenderSkillsListing(float pAlpha);
    virtual void RenderSkillsDescription(float pAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_MonoUISkills_SetProperty(lua_State *L);
