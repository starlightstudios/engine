//--Base
#include "CarnUIInventory.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureMenu.h"
#include "CarnationCombatEntity.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"
#include "StarlightString.h"
#include "StarUIPopup.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "ControlManager.h"
#include "OptionsManager.h"

///========================================== System ==============================================
CarnUIInventory::CarnUIInventory()
{
    ///--[ =========== Variable Initialization ========== ]
    ///--[ ======= RootObject ======= ]
    ///--[System]
    mType = POINTER_TYPE_CARNUIINVENTORY;

    ///--[ ====== StarUIPiece ======= ]
    ///--[System]
    strcpy(mSubtype, "CINV");

    ///--[Visiblity]
    mVisibilityTimerMax = 1;

    ///--[Images]
    ///--[ ====== AdvUICommon ======= ]
    ///--[System]
    ///--[Colors]
    ///--[ ==== CarnUIInventory ===== ]
    ///--[System]
    ///--[Popup]
    mIsPopupActive = false;
    mPopupItemIndex = -1;
    mPopup = new StarUIPopup();

    ///--[Confirmation]
    mConfirmCode = CARNUI_INV_NONE;
    mConfirmItemSlot = -1;
    mConfirmTimer = 0;

    ///--[Description]
    mDescription = new StarLinkedList(true);

    ///--[Images]
    memset(&CarnImages, 0, sizeof(CarnImages));

    ///--[ ================ Construction ================ ]
    ///--[Images]
    //--Add the CarnImages structure to the verify list.
    AppendVerifyPack("CarnImages", &CarnImages, sizeof(CarnImages));

    ///--[Hitboxes]
    //--Item hitboxes. Always cxCarnScrollPage on a page.
    float cItemX = 274.0f;
    float cItemY = 202.0f;
    float cItemW = 306.0f;
    float cItemH =  27.0f;
    float cVSpacing = 30.0f;
    for(int i = 0; i < cxCarnScrollPage; i ++)
    {
        RegisterHitboxWH("X", cItemX, cItemY + (cVSpacing * i), cItemW, cItemH);
    }

    //--Scrollbar hitboxes.
    RegisterHitboxWH("ScrollUp", 563.0f, 185.0f, 36.0f,  58.0f);
    RegisterHitboxWH("ScrollBd", 563.0f, 242.0f, 36.0f, 336.0f);
    RegisterHitboxWH("ScrollDn", 563.0f, 578.0f, 36.0f,  58.0f);

    //--Confirm/Cancel hitboxes.
    RegisterHitboxWH("Confirm", 433.0f, 339.0f, 228.0f,  61.0f);
    RegisterHitboxWH("Cancel",  704.0f, 339.0f, 228.0f,  61.0f);

    ///--[Scrollbars]
    ScrollbarPack *nScrollbarPack = (ScrollbarPack *)starmemoryalloc(sizeof(ScrollbarPack));
    nScrollbarPack->Initialize();
    nScrollbarPack->rSkipPtr = &mItemSkip;
    nScrollbarPack->mPerPage = cxCarnScrollPage;
    nScrollbarPack->mMaxSkip = 0;
    nScrollbarPack->mIndexUp = cxScrollMin;
    nScrollbarPack->mIndexBd = cxScrollMin+1;
    nScrollbarPack->mIndexDn = cxScrollMin+2;
    mScrollbarList->AddElement("Scrollbar", nScrollbarPack, &FreeThis);

    //--Set scrollbar as mousewheel target.
    rWheelScrollbar = nScrollbarPack;
}
CarnUIInventory::~CarnUIInventory()
{
    delete mPopup;
    delete mDescription;
}
void CarnUIInventory::Construct()
{
    ///--[Documentation]
    //--Orders all image structures to resolve their images, then makes sure they did so.
    if(mImagesReady) return;

    //--Subroutines handle resolving image pointers. These can overloaded.
    ResolveSeriesInto("Carnation Inventory UI",           &CarnImages,       sizeof(CarnImages));
    ResolveSeriesInto("Carnation Common UI",              &CarnImagesCommon, sizeof(CarnImagesCommon));
    ResolveSeriesInto("Adventure Inventory UI Carnation", &AdvImages,        sizeof(AdvImages));
    ResolveSeriesInto("Adventure Help UI",                &HelpImages,       sizeof(HelpImages));

    //--Run a verification routine. This does not print diagnostics if it fails, the caller should check that
    //  all UI pieces resolved their images and print diagnostics if needed.
    mImagesReady = VerifyImages();

    //--Pass popup menu the images.
    mPopup->SetVisTimerMax(cxPopupTimerMax);
    mPopup->SetStencilCode(100);
    mPopup->SetBuffers(4.0f, 4.0f);
    mPopup->SetFontP(CarnImages.rFont_Popup);
    mPopup->SetFrameImageP(CarnImages.rFrame_Popup);
}

///===================================== Property Queries =========================================
bool CarnUIInventory::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_CARNUIINVENTORY) return true;
    if(pType == POINTER_TYPE_STARUIPIECE)     return true;
    return (pType == mType);
}
int CarnUIInventory::GetAlchemizeValue(AdventureItem *pItem)
{
    ///--[Documentation]
    //--The player can alchemize an item to delete it and get some cash for it. This gives 25% of the
    //  item's value plus 25% of the value of any "gems" stored in it.
    if(!pItem) return 0;

    //--Get base value.
    int tBaseValue = pItem->GetValue();

    //--Check gems.
    int tGemSlots = pItem->GetGemSlots();
    for(int i = 0; i < tGemSlots; i ++)
    {
        AdventureItem *rGem = pItem->GetGemInSlot(i);
        if(rGem) tBaseValue = tBaseValue + rGem->GetValue();
    }

    //--Round.
    int tAlchemizeValue = tBaseValue * 0.25f;
    return tAlchemizeValue;
}

///======================================= Manipulators ===========================================
void CarnUIInventory::TakeForeground()
{
    ///--[Documentation]
    //--The rough equivalent of "Show", the object assumes it has been opened. Resets timers and resolves
    //  anything that needs to be done at runtime, such as control strings.

    ///--[Variables]
    //--Reset.
    mItemCursor = -1;
    delete mDescription;
    mDescription = NULL;

    //--Adjust the scrollbar size to match the inventory size.
    mItemSkip = 0;
    RecomputeMaxSkip();
}
void CarnUIInventory::RecomputeMaxSkip()
{
    ///--[Documentation]
    //--Determine what the maximum skip is for the item list, and also update the scrollbar with that
    //  information. This can be called externally, since switching from Equipment to Items can cause
    //  a desync when pacts are removed after the inventory has computed the item skip value.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetItemList();
    int tMaxSkip = rItemList->GetListSize() - cxCarnScrollPage;
    if(tMaxSkip < 0) tMaxSkip = 0;

    //--If the current skip is above that, reset it.
    if(mItemSkip > tMaxSkip) mItemSkip = tMaxSkip;

    //--Send this to the scrollbar.
    ScrollbarPack *rScrollbarPack = (ScrollbarPack *)mScrollbarList->GetElementByName("Scrollbar");
    if(rScrollbarPack) rScrollbarPack->mMaxSkip = tMaxSkip;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
bool CarnUIInventory::UpdateForeground(bool pCannotHandleUpdate)
{
    ///--[ ===== Documentation and Setup ====== ]
    //--Main update, should handle input, timers, and computations. If pCannotHandleUpdate is true,
    //  the object is not in the foreground, and should call UpdateBackground() and exit.
    if(pCannotHandleUpdate) { UpdateBackground(); return false; }

    ///--[Timers]
    //--By default, the visiblity timer runs to its max and then stops.
    if(mVisibilityTimer < mVisibilityTimerMax) mVisibilityTimer ++;
    StandardTimer(mConfirmTimer, 0, cxConfirmTimerMax, (mConfirmCode != CARNUI_INV_NONE));

    ///--[ =========== Confirmation =========== ]
    //--In confirmation mode, click the confirm or cancel buttons. That's all!
    if(mConfirmCode > CARNUI_INV_NONE)
    {
        //--Update mouse info.
        GetMouseInfo();
        RunHitboxCheck();

        //--Left click:
        if(ControlManager::Fetch()->IsFirstPress("MouseLft"))
        {
            //--Check if the mouse is over any of the hitboxes of items.
            HitboxReplyPack *rReplyPack = (HitboxReplyPack *)mHitboxReplyList->PushIterator();
            while(rReplyPack)
            {
                //--Fast-access pointers.
                int tIndex = rReplyPack->mIndex;

                //--Confirm action. The function HandleConfirmation() should clear the confirm code.
                if(tIndex == cxConfirm)
                {
                    HandleConfirmation();
                }
                //--Cancel action.
                else if(tIndex == cxCancel)
                {
                    mConfirmCode = CARNUI_INV_NONE;
                }

                //--Next.
                rReplyPack = (HitboxReplyPack *)mHitboxReplyList->AutoIterate();
            }
        }

        //--Stop update.
        return true;
    }

    ///--[ ========== Mouse Routines ========== ]
    //--Get mouse information.
    GetMouseInfo();
    RunHitboxCheck();

    //--Popup mode.
    if(mIsPopupActive)
    {
        ExecutePopupAction(mPopup->UpdateActive());
        return true;
    }
    else
    {
        mPopup->UpdateInactive();
    }

    //--Scrollbars update before anything else.
    if(UpdateScrollbars()) return true;

    //--Run handlers.
    if(HandleMouseOver()) return true;
    if(HandleMouseLeft()) return true;
    if(HandleMouseRight()) return true;

    //--Mark as handling the update.
    return true;
}
void CarnUIInventory::UpdateBackground()
{
    ///--[Documentation]
    //--Background update, typically just runs timers until the object finishes hiding, then does nothing.
    //  Depending on how the source UI works, objects may be calling this update every tick, allowing
    //  background processing, or only call when that source UI is shown.
    //--Main UIs typically don't call this directly, but call UpdateForeground() with pCannotHandleUpdate
    //  as true. This is dependent on the implementation of the calling UI. If the caller is itself
    //  a backgroundable object it may call this for all subcomponents.
    if(mVisibilityTimer > 0) mVisibilityTimer --;
    mPopup->UpdateInactive();
    StandardTimer(mConfirmTimer, 0, cxConfirmTimerMax, false);
}
bool CarnUIInventory::HandleMouseOver()
{
    ///--[Documentation]
    //--Handles the mouse moving over objects. Returns true if it should block the update.

    //--If the mouse did not move, do nothing.
    if(!mMouseMoved) return false;

    ///--[Execution]
    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetItemList();

    //--Variables.
    int tOldItemCursor = mItemCursor;

    //--Check if the mouse is over any of the hitboxes of items.
    int tReply = CheckRepliesWithin(cxItemBoxMin, cxItemBoxMax);
    if(tReply != -1)
    {
        mItemCursor = tReply - cxItemBoxMin + mItemSkip;
    }

    //--If the item cursor was out of range:
    if(mItemCursor >= rItemList->GetListSize()) mItemCursor = tOldItemCursor;

    //--Scrollbar. This will undo item cursor changes since the hovering is over the bar,
    //  not the item.
    tReply = CheckRepliesWithin(cxScrollMin, cxScrollMax);
    if(tReply != -1 && rItemList->GetListSize() > cxCarnScrollPage)
    {
        mItemCursor = tOldItemCursor;
        mHitboxReplyList->PopIterator();
    }

    //--Check if the item cursor changed. If it did, play a sound.
    if(tOldItemCursor != mItemCursor && mItemCursor != -1)
    {
        //--Get description.
        AdventureItem *rHighlightItem = (AdventureItem *)rItemList->GetElementBySlot(mItemCursor);
        if(rHighlightItem)
        {
            delete mDescription;
            mDescription = CreateDescriptionListFromString(rHighlightItem->GetDescription(), CarnImages.rFont_Statistic, 440.0f);
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Mouse overs never handle the update.
    return false;
}
bool CarnUIInventory::HandleMouseLeft()
{
    ///--[Documentation]
    //--Handles the mouse left-clicking. Returns true if it should block the update.

    //--If the mouse did not left click, do nothing.
    if(!ControlManager::Fetch()->IsFirstPress("MouseLft")) return false;

    //--Check if the click was over the common mode-change tabs. If so, stop.
    if(CommonTabClickUpdate()) return true;

    //--Check if the user left-clicked while over an item. If so, check if a popup can be activated.
    if(mItemCursor != -1) BuildPopupMenu(mItemCursor);

    //--Handled the update.
    return true;
}
bool CarnUIInventory::HandleMouseRight()
{
    ///--[Documentation]
    //--Handles the mouse right-clicking. Returns true if it should block the update.

    //--If the mouse did not left click, do nothing.
    if(!ControlManager::Fetch()->IsFirstPress("MouseRgt")) return false;

    //--Right-clicking always exits the UI.
    FlagExit();

    //--Handled the update.
    return true;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CarnUIInventory::RenderForeground(float pVisAlpha)
{
    ///--[Documentation]
    //--Rendering when the object is the front object. The caller may optionally modify the alpha,
    //  which may be handled as a fade mixing or as a translation. See HandlePositioning().
    //--This is the standard rendering cycle. It does error checks, calls RenderPieces(), and then
    //  cleans up.

    ///--[Visibility Checks]
    //--Images failed to resolve. Do nothing.
    if(!mImagesReady) return;

    //--Caller set the visibility at zero, do nothing.
    if(pVisAlpha <= 0.0f) return;

    //--Render nothing if everything is offscreen.
    if(mVisibilityTimer < 1) return;

    //--Re-resolve if we should fade or translate. The option can theoretically change at any time.
    mUseFade = OptionsManager::Fetch()->GetOptionB("UI Transitions By Fade");

    ///--[Call Pieces Rendering]
    //--Compute how on-screen this object is by calculating the alpha from the vis timer. Multiply
    //  with the provided alpha so the host doesn't fade differently from sub-components.
    float cLocalAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, (float)mVisibilityTimerMax) * pVisAlpha;

    //--Call subroutine. Can be overriden.
    RenderPieces(cLocalAlpha);

    ///--[Clean]
    //--Reset color mixer.
    StarlightColor::ClearMixer();
}
void CarnUIInventory::RenderPieces(float pVisAlpha)
{
    ///--[Documentation]
    //--This UI renders everything as a single block. The inventory is a list of items, which can be examined for more
    //  detail or, using popup menus, used as items in some cases.

    ///--[Base]
    CarnImages.rFrame_Base->Draw();

    ///--[Common]
    RenderTopTabs(mCodeBackward);
    RenderHexes();

    ///--[Item Listing]
    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    StarLinkedList *rItemList = rInventory->GetItemList();

    //--Constants.
    float cIconX = 274.0f;
    float cItemX = 300.0f;
    float cItemR = 582.0f;
    float cItemY = 202.0f;
    float cVSpacing = 30.0f;

    //--Render.
    int i = 0;
    int tRenders = 0;
    AdventureItem *rItem = (AdventureItem *)rItemList->PushIterator();
    while(rItem)
    {
        //--Don't render below the skip.
        if(i < mItemSkip)
        {
            i ++;
            rItem = (AdventureItem *)rItemList->AutoIterate();
            continue;
        }

        //--Underlay.
        CarnImages.rOverlay_ItemSlot->Draw(0.0f, (tRenders * cVSpacing));

        //--Count how many gems are in an item.
        int tGemCount = 0;
        int tGemSlots = rItem->GetGemSlots();
        for(int p = 0; p < tGemSlots; p ++)
        {
            if(rItem->GetGemInSlot(p) != NULL) tGemCount ++;
        }

        //--Icon.
        StarBitmap *rIcon = rItem->GetIconImage();
        if(rIcon)
        {
            rIcon->Draw(cIconX, cItemY + 4.0f + (tRenders * cVSpacing));
        }

        //--Render name.
        if(tGemCount < 1)
        {
            CarnImages.rFont_Statistic->DrawText(cItemX, cItemY + (tRenders * cVSpacing), 0, 1.0f, rItem->GetDisplayName());
        }
        else
        {
            CarnImages.rFont_Statistic->DrawTextArgs(cItemX, cItemY + (tRenders * cVSpacing), 0, 1.0f, "%s (+%i)", rItem->GetDisplayName(), tGemCount);
        }

        //--If quantity is over 1, render quantity.
        if(rItem->GetStackSize() > 1)
        {
            CarnImages.rFont_Statistic->DrawTextArgs(cItemR, cItemY + (tRenders * cVSpacing), SUGARFONT_RIGHTALIGN_X, 1.0f, "x%i", rItem->GetStackSize());
        }

        //--Stop at list edge.
        tRenders ++;
        if(tRenders >= cxCarnScrollPage)
        {
            rItemList->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rItem = (AdventureItem *)rItemList->AutoIterate();
    }

    //--Scrollbar.
    if(rItemList->GetListSize() >= cxCarnScrollPage)
    {
        StandardRenderScrollbar(mItemSkip, cxCarnScrollPage, rItemList->GetListSize() - cxCarnScrollPage, 245.0f, 330.0f, false, AdvImages.rScrollbar_Scroller, AdvImages.rScrollbar_Static);
    }

    ///--[Item Details]
    //--Resolve highlighted item. If there isn't one, do nothing.
    AdventureItem *rHighlightItem = (AdventureItem *)rItemList->GetElementBySlot(mItemCursor);
    if(rHighlightItem)
    {
        //--Constants.
        float cTitleX = 709.0f;
        float cTitleY = 101.0f;
        float cDescX  = 709.0f;
        float cDescY  = 147.0f;
        float cDescVSpacing = 30.0f;

        //--Item name.
        CarnImages.rFont_Header->DrawText(cTitleX, cTitleY, 0, 1.0f, rHighlightItem->GetDisplayName());

        //--Description.
        RenderDescription(cDescX, cDescY, cDescVSpacing, mDescription, CarnImages.rFont_Statistic);
    }

    ///--[Popup Menu]
    //--Shows if the popup timer is nonzero.
    mPopup->Render();

    ///--[Confirmation Mode]
    //--Darken the UI.
    float cConfirmPct = (float)mConfirmTimer / (float)cxConfirmTimerMax;
    StarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 192 * cConfirmPct));
    if(cConfirmPct >= 0.50f && mConfirmCode != CARNUI_INV_NONE)
    {
        //--Item information.
        AdventureInventory *rInventory = AdventureInventory::Fetch();
        StarLinkedList *rItemList = rInventory->GetItemList();
        AdventureItem *rItem = (AdventureItem *)rItemList->GetElementBySlot(mConfirmItemSlot);
        if(!rItem) return;

        //--Frame.
        CarnImages.rFrame_Confirm->Draw();

        //--Confirm and Cancel buttons.
        CarnImages.rFrame_Button->Draw(433.0f, 339.0f);
        CarnImages.rFrame_Button->Draw(704.0f, 339.0f);

        //--Question text depends on the mode.
        if(mConfirmCode == CARNUI_INV_ALCHEMIZE)
        {
            CarnImages.rFont_Confirm->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 200, SUGARFONT_AUTOCENTER_X, 1.0f, "Really alchemize %s?", rItem->GetDisplayName());
            CarnImages.rFont_Confirm->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 230, SUGARFONT_AUTOCENTER_X, 1.0f, "You will receive %i hexes.", GetAlchemizeValue(rItem));
        }
        else if(mConfirmCode == CARNUI_INV_DISCARD)
        {
            CarnImages.rFont_Confirm->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, 200, SUGARFONT_AUTOCENTER_X, 1.0f, "Really discard %s?", rItem->GetDisplayName());
        }

        //--Text.
        CarnImages.rFont_Confirm->DrawTextArgs(433.0f + 114.0f, 339.0f + 30.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Confirm");
        CarnImages.rFont_Confirm->DrawTextArgs(704.0f + 114.0f, 339.0f + 30.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Cancel");
    }
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
///======================================== Lua Hooking ===========================================
void CarnUIInventory::HookToLuaState(lua_State *pLuaState)
{
    /* [System]
       CarnUIInventory_SetProperty("Recompute Max Scroll")
       Sets the requested property in the CarnUIInventory. */
    lua_register(pLuaState, "CarnUIInventory_SetProperty", &Hook_CarnUIInventory_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CarnUIInventory_SetProperty(lua_State *L)
{
    ///--[ ========= Argument Listing ========= ]
    ///--[Static]
    ///--[Dynamic]
    //--[System]
    //CarnUIInventory_SetProperty("Recompute Max Scroll")

    ///--[ =========== Arg Resolve ============ ]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CarnUIInventory_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[ =========== Static Types =========== ]
    ///--[ ========== Dynamic Types =========== ]
    ///--[Resolve Object]
    //--Dynamic. Get the object in question.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("CarnUIInventory_SetProperty", L);

    //--Get the sub-object.
    CarnUIInventory *rUIObject = dynamic_cast<CarnUIInventory *>(rMenu->RetrieveUI("Inventory", POINTER_TYPE_CARNUIINVENTORY, NULL, "CarnUIInventory_SetProperty"));
    if(!rUIObject) return LuaTypeError("CarnUIInventory_SetProperty", L);

    ///--[System]
    //--If true, the first class slot will not be editable. Used to force characters into certain classes for story reasons.
    if(!strcasecmp(rSwitchType, "Recompute Max Scroll") && tArgs >= 1)
    {
        rUIObject->RecomputeMaxSkip();
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("CarnUIInventory_SetProperty", rSwitchType, tArgs);
    }

    //--Success.
    return 0;
}

