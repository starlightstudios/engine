///--[AdvCombatAbility]
//--Represents an ability to be used in combat. Is held by an AdvCombatEntity.
//--Note about mRequiresTarget:
//  Almost all abilities should require targets, even if the ability only ever targets one
//  entity. This is to allow the player to subconsciously confirm the target.
//  The exceptions are system abilities like Retreat or Surrender, which use confirmation
//  windows and thus don't need targets. Abilities default to needing targets.
//  Abilities that do not need targets *never* call the target paint script.

#pragma once

///--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "AdvCombatDefStruct.h"

///--[Local Structures]
//--[Local Definitions]
#define ADVCA_HAS_NO_INTERNAL 0
#define ADVCA_HAS_INTERNAL 1
#define ADVCA_HAS_NOT_RESOLVED 2

///--[Classes]
class AdvCombatAbility : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mDisplayName;
    char *mScriptPath;
    bool mScriptResponses[ACA_SCRIPT_CODE_TOTAL];
    bool mCannotBeEquipped;
    bool mIsEquipped;
    int mHasInternalVersion;
    int mLastCallNumber;
    bool mRunIfNotEquipped;
    bool mOnlyRunIfUnlocked;

    //--Usage
    int mMPCost;
    int mCPCost; //Only used for animations
    bool mIsUsableNow;
    bool mRequiresTarget;
    bool mOpensConfirmation;
    int mCooldown;

    //--Confirmation
    char *mConfirmationText;

    //--Display
    int mDescriptionLinesTotal;
    StarlightString **mDescriptionLines;
    int mSimplifiedDescriptionLinesTotal;
    StarlightString **mSimplifiedDescriptionLines;
    StarBitmap *rIconBack;
    StarBitmap *rIconFrame;
    StarBitmap *rIcon;
    StarBitmap *rCPIcon;
    StarBitmap *rChargesIcon;

    //--Storage
    bool mIsUnlocked;
    int mJPCost;
    AdvCombatEntity *rOwningEntity;
    StarLinkedList *mTagList; //int *, master

    //--Special:
    char *mUnlockExecScript;

    protected:

    public:
    //--System
    AdvCombatAbility();
    virtual ~AdvCombatAbility();

    //--Private Statics
    static char xSoundEffectQueue[STD_MAX_LETTERS];
    static int xSoundEffectPriority;
    static int xLastCallNumber;

    //--Property Queries
    const char *GetInternalName();
    const char *GetDisplayName();
    const char *GetScriptPath();
    bool IsUsableNow();
    bool NeedsTargetCluster();
    bool ActivatesConfirmation();
    int GetCooldown();
    char *GetConfirmationText();
    int GetDescriptionLinesTotal();
    StarlightString *GetDescriptionLine(int pSlot);
    int GetSimplifiedDescriptionLinesTotal();
    StarlightString *GetSimplifiedDescriptionLine(int pSlot);
    StarBitmap *GetIconBack();
    StarBitmap *GetIconFrame();
    StarBitmap *GetIcon();
    StarBitmap *GetCPIcon();
    bool IsUnlocked();
    int GetJPCost();
    int GetCPCost();
    int GetMPCost();
    bool IsEquipped();
    bool CanBeEquipped();
    int GetTagCount(const char *pTag);
    int HasInternalVersion();
    int GetLastCallNumber();
    const char *GetUnlockExecScript();
    bool RunsIfNotEquipped();
    bool RunsOnlyIfUnlocked();

    //--Manipulators
    void SetOwner(AdvCombatEntity *pOwner);
    void SetInternalName(const char *pName);
    void SetDisplayName(const char *pName);
    void SetScriptPath(const char *pPath);
    void SetScriptResponse(int pIndex, bool pFlag);
    void SetUsable(bool pIsUsable);
    void SetEquippable(bool pIsEquippable);
    void SetNeedsTarget(bool pNeedsTarget);
    void SetActivatesConfirmation(bool pFlag, const char *pConfirmationString);
    void SetDescription(const char *pDescription);
    void AllocateDescriptionImages(int pImages);
    void SetDescriptionImage(int pSlot, float pYOffset, const char *pDLPath);
    void CrossreferenceDescriptionImages();
    void SetSimplifiedDescription(const char *pDescription);
    void AllocateSimplifiedDescriptionImages(int pImages);
    void SetSimplifiedDescriptionImage(int pSlot, float pYOffset, const char *pDLPath);
    void CrossreferenceSimplifiedDescriptionImages();
    void SetIconBack(const char *pDLPath);
    void SetIconFrame(const char *pDLPath);
    void SetIcon(const char *pDLPath);
    void SetCPIcon(const char *pDLPath);
    void SetChargesIcon(const char *pDLPath);
    void SetUnlocked(bool pFlag);
    void SetJPCost(int pCost);
    void SetCPCost(int pCost);
    void SetMPCost(int pCost);
    void SetEquipped(bool pFlag);
    void SetCooldown(int pTurns);
    void AddTag(const char *pTag);
    void RemoveTag(const char *pTag);
    void SetInternalVersionFlag(int pFlag);
    void SetUnlockExecScript(const char *pScript);
    void SetRunIfNotEquipped(bool pFlag);
    void SetOnlyRunIfUnlocked(bool pFlag);

    //--Core Methods
    void CallCode(int pCode);
    void EnqueueAsEvent(int pPriority);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void RenderAt(float pX, float pY);
    void RenderAtCombat(float pX, float pY, float pAlpha);

    //--Pointer Routing
    AdvCombatEntity *GetOwner();

    //--Static Functions
    static void HandleCombatActionSounds();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatAbility_GetProperty(lua_State *L);
int Hook_AdvCombatAbility_SetProperty(lua_State *L);

