///===================================== CarnUIFileSelect =========================================
//--Description

#pragma once

///========================================= Includes =============================================
#include "AdvUIFileSelect.h"
#include "CarnUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///========================================== Classes =============================================
class CarnUIFileSelect : public CarnUICommon, virtual public AdvUIFileSelect
{
    protected:
    ///--[Constants]
    //--Scrollbar
    static const int cxCarnEntriesPerPage = 9;      //Number of save files visible per page before a scrollbar is needed.

    //--Hitboxes
    static const int cxHitbox_ScrollUp   =  0;
    static const int cxHitbox_ScrollMd   =  1;
    static const int cxHitbox_ScrollDn   =  2;
    static const int cxHitbox_SavefileLo =  3;
    static const int cxHitbox_SavefileHi = 11;
    static const int cxHitbox_NewGame    = 12;
    static const int cxHitbox_Ovewrite   = 13;
    static const int cxHitbox_EditNote   = 14;

    ///--[System]
    bool mShowTutorialIndicator;
    bool mAllowNewSaves;

    ///--[Cursor]
    int mSelectedSaveFile;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_Button;
        StarFont *rFont_File;
        StarFont *rFont_CharInfo;

        //--Images
        StarBitmap *rFrame_Base;
        StarBitmap *rFrame_Button;
        StarBitmap *rMask_SaveData;
        StarBitmap *rOverlay_Divider;
        StarBitmap *rOverlay_PortraitFrame;
        StarBitmap *rScrollbar_Scroller;
        StarBitmap *rScrollbar_Static;
        StarBitmap *rUnderlay_Headers;
    }CarnImages;

    protected:

    public:
    //--System
    CarnUIFileSelect();
    virtual ~CarnUIFileSelect();
    virtual void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual bool UpdateNotes();
    virtual void UpdateBackground();
    void HandleSaveLeftClick();

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderFileBlock(int pIndex, float pLft, float pTop, bool pIsHighlighted, float pOpacityPct);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


