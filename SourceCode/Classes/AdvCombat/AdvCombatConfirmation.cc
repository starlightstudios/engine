//--Base
#include "AdvCombat.h"

//--Classes

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "ControlManager.h"

///========================================== System ==============================================
void AdvCombat::ActivateConfirmationWindow()
{
    mIsShowingConfirmationWindow = true;
}
void AdvCombat::DeactivateConfirmationWindow()
{
    mIsShowingConfirmationWindow = false;
}

///===================================== Property Queries =========================================
StarlightString *AdvCombat::GetConfirmationString()
{
    return mConfirmationString;
}

///======================================== Manipulators ==========================================
void AdvCombat::SetConfirmationText(const char *pString)
{
    //--Used when setting the string to use no special images. If you need advanced control, get the
    //  string with GetConfirmationString() and do the setup there.
    mConfirmationString->SetString(pString);
}
void AdvCombat::SetConfirmationFunc(ConfirmationFnPtr pFuncPtr)
{
    rConfirmationFunction = pFuncPtr;
}

///======================================== Core Methods ==========================================
void AdvCombat::ConfirmExecuteAbility()
{
    //--Common function handler for most confirmation cases. Usually, the program is requesting
    //  permission to execute an ability. The ability is stored in the class, and this just orders
    //  the AdvCombat to execute it.
    AdvCombat::Fetch()->ExecuteActiveAbility();
}

///=========================================== Update =============================================
void AdvCombat::UpdateConfirmation()
{
    //--Handles updates during confirmation mode. This is explicitly a player-only activity, enemies
    //  never bother with the confirmation window (and it'd be weird if they did).
    //--This is not called unless confirmation mode is active.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate:
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--In all cases, stop confirmation mode.
        mIsShowingConfirmationWindow = false;

        //--Execute the confirmation function pointer, if it exists.
        if(rConfirmationFunction) rConfirmationFunction();
    }
    //--Cancel:
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Set the flag to exit confirmation mode.
        mIsShowingConfirmationWindow = false;
    }
}

///========================================== Drawing =============================================
void AdvCombat::RenderConfirmation()
{
    //--Renders the confirmation dialogue over the screen.
    if(!Images.mIsReady || mConfirmationTimer < 1) return;

    //--Compute alpha.
    float cPct = EasingFunction::QuadraticInOut(mConfirmationTimer, ADVCOMBAT_CONFIRMATION_TICKS);

    //--Grey the screen out a bit.
    float cBacking = cPct * 0.80f;
    StarBitmap::DrawFullBlack(cBacking);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPct);

    //--Render the confirmation string in the middle of the screen.
    float cXPosition = VIRTUAL_CANVAS_X * 0.50f;
    float cYPosition = VIRTUAL_CANVAS_Y * 0.40f;
    mConfirmationString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY | SUGARFONT_NOCOLOR, 1.0f, Images.Data.rConfirmationFontBig);

    //--Render the accept string.
    cXPosition = VIRTUAL_CANVAS_X * 0.40f;
    cYPosition = VIRTUAL_CANVAS_Y * 0.50f;
    mAcceptString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY | SUGARFONT_NOCOLOR, 1.0f, Images.Data.rConfirmationFontSmall);

    //--Render the cancel string.
    cXPosition = VIRTUAL_CANVAS_X * 0.60f;
    cYPosition = VIRTUAL_CANVAS_Y * 0.50f;
    mCancelString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY | SUGARFONT_NOCOLOR, 1.0f, Images.Data.rConfirmationFontSmall);

    //--Clean.
    StarlightColor::ClearMixer();
}
