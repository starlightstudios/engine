//--[PlayerPony]
//--This is you, the Player.  Everything directly related to the Player and his/her avatar is
//  in this class. It's also a RootEntity, but is handled specially and never deleted.
//--The player does not necessarily need to render in the world, but it's a RootEntity so the EM
//  can handle it generically. It always updates and renders first.
//--I don't care if the derived game doesn't have a single pony in it, this class is staying as
//  PlayerPony because Star^3 must always remember its history.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootEntity.h"

//--[Local Structures]
//--[Local Definitions]
//--[Class]
class PlayerPony : public RootEntity
{
    private:
    //--System
    //--Controls
    StarLinkedList *mLockoutList;

    //--Mouse Handling
    int mClickBeginX, mClickBeginY;

    //--Special Context Options
    StarLinkedList *mContextOptionList;

    public:
    //--System
    PlayerPony();
    virtual ~PlayerPony();
    void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void ResetToGameStart();
    void RegisterContextOption(const char *pPath);

    //--Private Core Methods
    //--Core Methods
    void HandleCamera(float &sLft, float &sTop);
    void WipeContextCommands();
    void Reset(const char *pResetString);

    //--Control manipulators (PlayerControls.cc)
    bool AreControlsLocked();
    void AddToLockList(const char *pLockName);
    void DelFromLockList(const char *pLockName);
    void DelFromLockListPattern(const char *pPattern);
    void ClearLockList();

    //--Update routines
    virtual void Update();

    //--Drawing routines
    virtual void Render();

    //--Pointer Routing
    StarLinkedList *GetContextCommandList();

    //--Static Functions
    static PlayerPony *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};
