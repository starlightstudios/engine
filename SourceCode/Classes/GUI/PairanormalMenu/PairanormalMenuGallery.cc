//--Base
#include "PairanormalMenu.h"

//--Classes
#include "PairanormalLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

///--[Manipulators]
void PairanormalMenu::AllocateGallerySpace(int pSlots)
{
    //--Deallocate.
    for(int i = 0; i < mGalleryPathsTotal; i ++) free(mGalleryPaths[i]);
    free(mGalleryPaths);

    //--Reset.
    mCurrentGalleryPage = 0;
    mGalleryPathsTotal = 0;
    mGalleryPaths = NULL;
    if(pSlots < 1) return;

    //--Allocate.
    mGalleryPathsTotal = pSlots;
    mGalleryPaths = (char **)malloc(sizeof(char *) * mGalleryPathsTotal);

    //--Clear to Default.
    memset(mGalleryPaths, 0, sizeof(char *) * mGalleryPathsTotal);
}
void PairanormalMenu::SetGalleryImage(int pSlot, const char *pPath)
{
    //--Error check.
    if(pSlot < 0 || pSlot >= mGalleryPathsTotal) return;
    ResetString(mGalleryPaths[pSlot], pPath);
}

///--[Core Methods]
void PairanormalMenu::RefreshImagesByPage(int pPage)
{
    //--Page cannot go below 0.
    if(pPage < 0) return;

    //--Check if there would be no images on this page.
    if(pPage * GALLERY_VISIBLE_IMAGES > mGalleryPathsTotal) return;

    //--Clear.
    mCurrentGalleryPage = pPage;
    memset(rGalleryImages, 0, sizeof(void *) * GALLERY_VISIBLE_IMAGES);

    //--Figure out which set of GALLERY_VISIBLE_IMAGES needs to be displayed. NULL is a valid pointer.
    int cStart = pPage * GALLERY_VISIBLE_IMAGES;
    for(int i = cStart; i < cStart + GALLERY_VISIBLE_IMAGES; i ++)
    {
        //--Range check.
        if(i < 0 || i >= mGalleryPathsTotal) continue;

        //--Extract and store the bitmap.
        rGalleryImages[i - cStart] = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(mGalleryPaths[i]);
    }
}

///--[Update]
void PairanormalMenu::UpdateGallery()
{
    ///--[Documentation and Setup]
    //--Updates the gallery controls.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timer]
    if(mGalleryFadeTimer < PM_FADE_TICKS)
    {
        mGalleryFadeTimer ++;
    }

    ///--[Mouse Position]
    //--Get mouse location.
    float tMouseZ;
    rControlManager->GetMouseCoordsF(mMouseX, mMouseY, tMouseZ);

    ///--[Mouse Clicking]
    //--Player clicks the mouse at a location on screen.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        ///--[Normal Case]
        if(mFocusOnImage == -1)
        {
            //--Go back to the main menu.
            if(mGalleryBtnDim[PMBTN_GAL_BACK].IsPointWithin(mMouseX, mMouseY))
            {
                mCurrentMode = PM_MODE_MAINMENU;
                AudioManager::Fetch()->PlaySound("UI|Select");
            }
            //--Settings.
            else if(mGalleryBtnDim[PMBTN_GAL_SETTINGS].IsPointWithin(mMouseX, mMouseY))
            {

            }
            //--Page Decrement.
            else if(mGalleryBtnDim[PMBTN_GAL_PAGEDOWN].IsPointWithin(mMouseX, mMouseY))
            {
                if(mCurrentGalleryPage > 0) RefreshImagesByPage(mCurrentGalleryPage - 1);
                AudioManager::Fetch()->PlaySound("UI|Select");
            }
            //--Page Increment.
            else if(mGalleryBtnDim[PMBTN_GAL_PAGEUP].IsPointWithin(mMouseX, mMouseY))
            {
                RefreshImagesByPage(mCurrentGalleryPage + 1);
                AudioManager::Fetch()->PlaySound("UI|Select");
            }
            //--For-loop to check the picture buttons. They have the same functionality.
            else
            {
                for(int i = PMBTN_GAL_PICTURE_0; i < PMBTN_GAL_PICTURE_8 + 1; i ++)
                {
                    if(mGalleryBtnDim[i].IsPointWithin(mMouseX, mMouseY))
                    {
                        mFocusOnImage = i - PMBTN_GAL_PICTURE_0;
                        if(rGalleryImages[mFocusOnImage] == NULL) mFocusOnImage = -1;
                        AudioManager::Fetch()->PlaySound("UI|Select");
                    }
                }
            }
        }
        ///--[Image Focus]
        else
        {
            //--Button clears the focus.
            if(mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].IsPointWithin(mMouseX, mMouseY))
            {
                mFocusOnImage = -1;
                AudioManager::Fetch()->PlaySound("UI|Select");
            }
        }
    }
}

///--[Render]
void PairanormalMenu::RenderGallery()
{
    ///--[Documentation and Setup]
    //--Renders the gallery.
    if(!Images.mIsReady) return;

    //--Alpha.
    float cAlpha = (float)mGalleryFadeTimer / (float)PM_FADE_TICKS;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Render the background.
    if(rGalleryBackground)
    {
        //--Determine scale and offset.
        float cUseScale = BG_SCALE;
        float cYOffset = BG_OFFSET_Y;
        if(PairanormalLevel::xIsLowResMode)
        {
            cUseScale = cUseScale * 2.0f;
            cYOffset = cYOffset * 0.5f;
        }

        //--Render.
        glScalef(cUseScale, cUseScale, 1.0f);
        rGalleryBackground->Draw(0, BG_OFFSET_Y);
        glScalef(1.0f / cUseScale, 1.0f / cUseScale, 1.0f);
    }

    //--Darken overlay.
    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 0.50f * cAlpha);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);

    //--Static Parts
    Images.Data.rGalleryBacking->Draw();
    Images.Data.rBoxBlack->Draw(411, 26);
    Images.Data.rHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 40.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gallery");

    //--Render a backing box for each picture.
    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f * cAlpha);
    for(int i = PMBTN_GAL_PICTURE_0; i < PMBTN_GAL_PICTURE_8 + 1; i ++)
    {
        //--If the image exists, render it here.
        if(rGalleryImages[i - PMBTN_GAL_PICTURE_0])
        {
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);
            Images.Data.rImageBacking->Draw(mGalleryBtnDim[i].mLft - 21.0f, mGalleryBtnDim[i].mTop - 21.0f);
            rGalleryImages[i - PMBTN_GAL_PICTURE_0]->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(0.0f, 1.0f); glVertex2f(mGalleryBtnDim[i].mLft, mGalleryBtnDim[i].mTop);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(mGalleryBtnDim[i].mRgt, mGalleryBtnDim[i].mTop);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(mGalleryBtnDim[i].mRgt, mGalleryBtnDim[i].mBot);
                glTexCoord2f(0.0f, 0.0f); glVertex2f(mGalleryBtnDim[i].mLft, mGalleryBtnDim[i].mBot);
            glEnd();
            StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f * cAlpha);
        }
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);
    }

    //--When not focusing on an image, show the page changer buttons.
    if(mFocusOnImage == -1)
    {
        //--Render.
        Images.Data.rBtnIncrement->Draw(mGalleryBtnDim[PMBTN_GAL_PAGEUP].mLft   - 15.0f, mGalleryBtnDim[PMBTN_GAL_PAGEUP].mTop   - 10.0f);
        Images.Data.rBtnDecrement->Draw(mGalleryBtnDim[PMBTN_GAL_PAGEDOWN].mLft - 15.0f, mGalleryBtnDim[PMBTN_GAL_PAGEDOWN].mTop - 10.0f);

        //--Page Count.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);
        float tPosition = 460.0f - Images.Data.rHeadingFont->GetTextWidth("Page 77/77");
        Images.Data.rHeadingFont->DrawTextArgs(tPosition, 677.0f, 0, 1.0f, "Page %i/%i", mCurrentGalleryPage+1, (mGalleryPathsTotal / GALLERY_VISIBLE_IMAGES)+1);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);
    }
    //--When focusing on an image, show a check-mark to cancel focus mode.
    else
    {
        //--Backing.
        glDisable(GL_TEXTURE_2D);
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f * cAlpha);
        glBegin(GL_QUADS);
            glVertex2f(mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].mLft, mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].mTop);
            glVertex2f(mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].mRgt, mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].mTop);
            glVertex2f(mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].mRgt, mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].mBot);
            glVertex2f(mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].mLft, mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].mBot);
        glEnd();

        //--Check Mark.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);
        glBegin(GL_QUADS);
            glVertex2f(1052.0f, 680.0f);
            glVertex2f(1067.0f, 697.0f);
            glVertex2f(1062.0f, 701.0f);
            glVertex2f(1045.0f, 686.0f);

            glVertex2f(1080.0f, 667.0f);
            glVertex2f(1087.0f, 673.0f);
            glVertex2f(1067.0f, 697.0f);
            glVertex2f(1061.0f, 692.0f);
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }

    //--UI Buttons.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);
    Images.Data.rIconBack->Draw();
    //Images.Data.rIconClose->Draw();

    //--Clean.
    glEnable(GL_TEXTURE_2D);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);

    ///--[Image Focus]
    //--When focusing on a given image, it renders over the main board.
    if(mFocusOnImage >= 0 && mFocusOnImage < GALLERY_VISIBLE_IMAGES && rGalleryImages[mFocusOnImage])
    {
        //--Positions.
        float cLft = 287.0f;
        float cTop = 118.0f;
        float cWid = 791.0f;
        float cHei = 555.0f;
        float cRgt = cLft + cWid;
        float cBot = cTop + cHei;

        //--Render.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);
        rGalleryImages[mFocusOnImage]->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 1.0f); glVertex2f(cLft, cTop);
            glTexCoord2f(1.0f, 1.0f); glVertex2f(cRgt, cTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cBot);
            glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cBot);
        glEnd();
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}
