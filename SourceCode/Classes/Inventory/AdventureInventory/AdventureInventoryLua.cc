//--Base
#include "AdventureInventory.h"

//--Classes
#include "AdventureItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================== Lua Hooking ===========================================
void AdventureInventory::HookToLuaState(lua_State *pLuaState)
{
    /* AdInv_CreateItem(sName)
       Creates, registers, and pushes a new AdventureItem. This item is registered to the player's
       inventory by default. */
    lua_register(pLuaState, "AdInv_CreateItem", &Hook_AdInv_CreateItem);

    /* AdInv_GetProperty("Is Registering To Shop") (1 Boolean) (Static)
       AdInv_GetProperty("Name And Quantity From String", sString) (1 String, 1 Integer) (Static)
       AdInv_GetProperty("Unique ID Counter") (1 Integer)
       AdInv_GetProperty("Platina") (1 Integer)
       AdInv_GetProperty("Total Items Unequipped") (1 Integer)
       AdInv_GetProperty("Total Items Equipped") (1 Integer)
       AdInv_GetProperty("Item Count", sName) (1 Integer)
       AdInv_GetProperty("Is Item Equipped", sCharacter, sItemName) (1 Integer)
       AdInv_GetProperty("Is Any Item Equipped In", sCharacter, sSlotName) (1 Boolean)
       AdInv_GetProperty("Crafting Count", iSlot) (1 Integer)
       AdInv_GetProperty("Doctor Bag Charges") (1 Integer)
       AdInv_GetProperty("Doctor Bag Charges Max") (1 Integer)
       AdInv_GetProperty("Catalyst Count", iSlot) (1 Integer)
       AdInv_GetProperty("Catalyst Local Cur", iSlot) (1 Integer)
       AdInv_GetProperty("Catalyst Local Max", iSlot) (1 Integer)
       Sets the requested property in the adventure inventory. */
    lua_register(pLuaState, "AdInv_GetProperty", &Hook_AdInv_GetProperty);

    /* //--[Static]
       AdInv_SetProperty("Item Image Path", sPath) (Static)
       AdInv_SetProperty("Override Last Item Image", sPath) (Static)

       //--[System]
       AdInv_SetProperty("Gem Name Resolve Path", sPath)
       AdInv_SetProperty("Clear")
       AdInv_SetProperty("Stack Items")
       AdInv_SetProperty("Clear Buyback")

       //--[Doctor Bag]
       AdInv_SetProperty("Doctor Bag Charges", iAmount)
       AdInv_SetProperty("Doctor Bag Charges Max", iAmount)
       AdInv_SetProperty("Doctor Bag Charge Factor", fFactor)
       AdInv_SetProperty("Doctor Bag Potency", fPotency)

       //--[Catalysts]
       AdInv_SetProperty("Catalyst Count", iCatalystType, iAmount)
       AdInv_SetProperty("Catalyst Local Cur", iCatalystType, iAmount)
       AdInv_SetProperty("Catalyst Local Max", iCatalystType, iAmount)

       //--[Platina]
       AdInv_SetProperty("Add Platina", iAmount)
       AdInv_SetProperty("Remove Platina", iAmount)
       AdInv_SetProperty("Remove Item", sName, iQuantity)

       //--[Crafting]
       AdInv_SetProperty("Crafting Material", iSlot, iAmount)
       AdInv_SetProperty("Add Crafting Material", iSlot, iAmount)
       AdInv_SetProperty("Remove Crafting Material", iSlot, iAmount)

       //--[Gems]
       AdInv_SetProperty("Mark Last Item As Master Gem")
       AdInv_SetProperty("Merge Last Gem With Master")
       AdInv_SetProperty("Push Master Gem") (Pushes Activity Stack)
       AdInv_SetProperty("Clear Master Gem")
       AdInv_SetProperty("Mark Last Item As Socket Item")
       AdInv_SetProperty("Socket Last Item In Socket Item")
       AdInv_SetProperty("Clear Socket Item")
       Sets the requested property in the AdventureInventory. */
    lua_register(pLuaState, "AdInv_SetProperty", &Hook_AdInv_SetProperty);

    /* AdInv_PushItem(sItemName)
       Pushes the first instance of the item found in the inventory. This is rarely used, and should
       only be used if the item has a unique name. */
    lua_register(pLuaState, "AdInv_PushItem", &Hook_AdInv_PushItem);

    /* AdInv_PushItemI(iIndex)
       Pushes the item in the given slot. */
    lua_register(pLuaState, "AdInv_PushItemI", &Hook_AdInv_PushItemI);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_AdInv_CreateItem(lua_State *L)
{
    //AdInv_CreateItem(sName)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_CreateItem");

    //--Create.
    AdventureItem *nItem = new AdventureItem();
    nItem->SetName(lua_tostring(L, 1));

    //--Register.
    bool tItemExists = AdventureInventory::Fetch()->RegisterItem(nItem);
    if(!tItemExists)
    {
        fprintf(stderr, "WARNING: ITEM DELETED DUE TO STACKING. DO NOT MODIFY!\n");
        nItem = NULL;
    }

    //--Push as active object.
    DataLibrary::Fetch()->rActiveObject = nItem;
    return 0;
}
int Hook_AdInv_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AdInv_GetProperty("Is Registering To Shop") (1 Boolean) (Static)
    //AdInv_GetProperty("Name And Quantity From String", sString) (1 String, 1 Integer) (Static)

    //--[System]
    //AdInv_GetProperty("Unique ID Counter") (1 Integer)
    //AdInv_GetProperty("Internal Sort Flag") (1 Integer)
    //AdInv_GetProperty("Platina") (1 Integer)

    //--[Counts]
    //AdInv_GetProperty("Total Items Unequipped") (1 Integer)
    //AdInv_GetProperty("Total Items Equipped") (1 Integer)
    //AdInv_GetProperty("Item In Slot", iSlot) (1 String)
    //AdInv_GetProperty("Item Count", sName) (1 Integer)
    //AdInv_GetProperty("Item Count Plus Equip", sName) (1 Integer)
    //AdInv_GetProperty("Is Item Equipped", sCharacter, sItemName) (1 Integer)
    //AdInv_GetProperty("Is Any Item Equipped In", sCharacter, sSlotName) (1 Boolean)
    //AdInv_GetProperty("Crafting Count", iSlot) (1 Integer)
    //AdInv_GetProperty("Doctor Bag Charges") (1 Integer)
    //AdInv_GetProperty("Doctor Bag Charges Max") (1 Integer)
    //AdInv_GetProperty("Catalyst Count", iSlot) (1 Integer)
    //AdInv_GetProperty("Catalyst Local Cur", iSlot) (1 Integer)
    //AdInv_GetProperty("Catalyst Local Max", iSlot) (1 Integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_GetProperty");

    //--Switch type.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static Types]
    //--Returns true if the item is registering to a shop and not the inventory. Used during item
    //  creation only.
    if(!strcasecmp(rSwitchType, "Is Registering To Shop") && tArgs == 1)
    {
        lua_pushboolean(L, AdventureInventory::xIsRegisteringItemsToShop);
        return 1;
    }
    //--Given a string like "Dogs x20", returns "Dogs" and 20, splitting the string.
    if(!strcasecmp(rSwitchType, "Name And Quantity From String") && tArgs >= 2)
    {
        //--Call routine.
        int tQuantity = 1;
        char *nString = Subdivide::GetItemNameAndQuantity(lua_tostring(L, 2), tQuantity);

        //--Push.
        lua_pushstring(L, nString);
        lua_pushinteger(L, tQuantity);

        //--Clean.
        free(nString);
        return 2;
    }

    ///--[Dynamic Types]
    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Switching.
    int tReturns = 0;

    //--The current Unique ID counter for items. Used for savefiles.
    if(!strcasecmp(rSwitchType, "Unique ID Counter") && tArgs == 1)
    {
        lua_pushinteger(L, AdventureItem::xItemUniqueIDCounter);
        tReturns = 1;
    }
    //--Flag for current inventory sorting.
    else if(!strcasecmp(rSwitchType, "Internal Sort Flag") && tArgs == 1)
    {
        lua_pushinteger(L, rInventory->GetInternalSortFlag());
        tReturns = 1;
    }
    //--How much money the party has.
    else if(!strcasecmp(rSwitchType, "Platina") && tArgs == 1)
    {
        lua_pushinteger(L, rInventory->GetPlatina());
        tReturns = 1;
    }
    //--How many items are in the inventory but not equipped to characters.
    else if(!strcasecmp(rSwitchType, "Total Items Unequipped") && tArgs == 1)
    {
        StarLinkedList *rItemList = rInventory->GetItemList();
        lua_pushinteger(L, rItemList->GetListSize());
        tReturns = 1;
    }
    //--How many items are in the inventory, including those equipped to characters.
    else if(!strcasecmp(rSwitchType, "Total Items Equipped") && tArgs == 1)
    {
        rInventory->BuildExtendedItemList();
        StarLinkedList *rExtendedItemsList = rInventory->GetExtendedItemList();
        lua_pushinteger(L, rExtendedItemsList->GetListSize());
        tReturns = 1;
    }
    //--Name of the item in the given slot.
    else if(!strcasecmp(rSwitchType, "Item In Slot") && tArgs == 2)
    {
        StarLinkedList *rItemList = rInventory->GetItemList();
        lua_pushstring(L, rItemList->GetNameOfElementBySlot(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Returns how many times the given name is in the inventory. Content does not matter, just name.
    else if(!strcasecmp(rSwitchType, "Item Count") && tArgs == 2)
    {
        lua_pushinteger(L, rInventory->GetCountOf(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns how many times the given name is in the inventory, including those equipped on party members.
    else if(!strcasecmp(rSwitchType, "Item Count Plus Equip") && tArgs == 2)
    {
        lua_pushinteger(L, rInventory->GetCountOfEquip(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns 1 or more if the item is equipped on the character, 0 if not.
    else if(!strcasecmp(rSwitchType, "Is Item Equipped") && tArgs == 3)
    {
        lua_pushinteger(L, rInventory->IsItemEquippedBy(lua_tostring(L, 2), lua_tostring(L, 3)));
        tReturns = 1;
    }
    //--Returns true if any item is equipped in the named slot by the named character.
    else if(!strcasecmp(rSwitchType, "Is Any Item Equipped In") && tArgs == 3)
    {
        lua_pushboolean(L, rInventory->IsAnyItemEquippedByIn(lua_tostring(L, 2), lua_tostring(L, 3)));
        tReturns = 1;
    }
    //--How many crafting items of the given slot amount are present.
    else if(!strcasecmp(rSwitchType, "Crafting Count") && tArgs == 2)
    {
        lua_pushinteger(L, rInventory->GetCraftingCount(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Doctor Bag Charges
    else if(!strcasecmp(rSwitchType, "Doctor Bag Charges") && tArgs == 1)
    {
        lua_pushinteger(L, rInventory->GetDoctorBagCharges());
        tReturns = 1;
    }
    //--Doctor Bag Charges Max
    else if(!strcasecmp(rSwitchType, "Doctor Bag Charges Max") && tArgs == 1)
    {
        lua_pushinteger(L, rInventory->GetDoctorBagChargesMax());
        tReturns = 1;
    }
    //--Catalyst Count of a given type.
    else if(!strcasecmp(rSwitchType, "Catalyst Count") && tArgs == 2)
    {
        lua_pushinteger(L, rInventory->GetCatalystCount(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--How many catalysts of this type have been acquired so far in the chapter.
    else if(!strcasecmp(rSwitchType, "Catalyst Local Cur") && tArgs == 2)
    {
        lua_pushinteger(L, rInventory->GetCatalystLocalCur(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--How many catalysts of this type are available in this chapter.
    else if(!strcasecmp(rSwitchType, "Catalyst Local Max") && tArgs == 2)
    {
        lua_pushinteger(L, rInventory->GetCatalystLocalMax(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("AdItem_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_AdInv_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AdInv_SetProperty("Item Image Path", sPath) (Static)
    //AdInv_SetProperty("Override Last Item Image", sPath) (Static)

    //--[System]
    //AdInv_SetProperty("Gem Name Resolve Path", sPath)
    //AdInv_SetProperty("Clear")
    //AdInv_SetProperty("Clear Buyback")
    //AdInv_SetProperty("Stack Items")
    //AdInv_SetProperty("Set Internal Sort", iFlag)
    //AdInv_SetProperty("Mark Last Item Quantity", iQuantity)
    //AdInv_SetProperty("Push Item By Slot", iSlot)
    //AdInv_SetProperty("Remove Item By Slot", iSlot)
    //AdInv_SetProperty("Push Last Item")

    //--[Doctor Bag]
    //AdInv_SetProperty("Doctor Bag Charges", iAmount)
    //AdInv_SetProperty("Doctor Bag Charges Max", iAmount)
    //AdInv_SetProperty("Doctor Bag Charge Factor", fFactor)
    //AdInv_SetProperty("Doctor Bag Potency", fPotency)

    //--[Catalysts]
    //AdInv_SetProperty("Catalyst Count", iCatalystType, iAmount)
    //AdInv_SetProperty("Catalyst Local Cur", iCatalystType, iAmount)
    //AdInv_SetProperty("Catalyst Local Max", iCatalystType, iAmount)
    //AdInv_SetProperty("Resolve Catalysts By String", sString)

    //--[Platina]
    //AdInv_SetProperty("Add Platina", iAmount)
    //AdInv_SetProperty("Remove Platina", iAmount)
    //AdInv_SetProperty("Remove Item", sName, iQuantity)

    //--[Crafting]
    //AdInv_SetProperty("Crafting Material", iSlot, iAmount)
    //AdInv_SetProperty("Add Crafting Material", iSlot, iAmount)
    //AdInv_SetProperty("Remove Crafting Material", iSlot, iAmount)

    //--[Gems]
    //AdInv_SetProperty("Mark Last Item As Master Gem")
    //AdInv_SetProperty("Merge Last Gem With Master")
    //AdInv_SetProperty("Push Master Gem") (Pushes Activity Stack)
    //AdInv_SetProperty("Clear Master Gem")
    //AdInv_SetProperty("Mark Last Item As Socket Item")
    //AdInv_SetProperty("Socket Last Item In Socket Item")
    //AdInv_SetProperty("Clear Socket Item")
    //AdInv_SetProperty("Mark Last Item As Equipment")
    //AdInv_SetProperty("Clear Equipment Marker")

    ///--[Argument Check]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_SetProperty");

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--DLPath of the last name run through the image remapping script.
    if(!strcasecmp(rSwitchType, "Item Image Path") && tArgs == 2)
    {
        ResetString(AdventureInventory::xLastItemImagePath, lua_tostring(L, 2));
    }
    //--Used for chest pickup notifications, overrides the last item image. Some items, like adamantite,
    //  don't go through the item creation system and must have their image specified manually.
    else if(!strcasecmp(rSwitchType, "Override Last Item Image") && tArgs == 2)
    {
        void *rPtr = DataLibrary::Fetch()->GetEntry(lua_tostring(L, 2));
        AdventureInventory::xrLastRegisteredIcon = (StarBitmap *)rPtr;
    }
    ///--[System]
    //--Path to the file that resolves gem names when they change.
    else if(!strcasecmp(rSwitchType, "Gem Name Resolve Path") && tArgs == 2)
    {
        rInventory->SetGemNameResolvePath(lua_tostring(L, 2));
    }
    //--Clear the entire inventory. Do NOT use this while an item is on the activity stack!
    //  Implicitly clears the buyback list.
    else if(!strcasecmp(rSwitchType, "Clear") && tArgs == 1)
    {
        rInventory->Clear();
    }
    //--Clears just the buyback listing.
    else if(!strcasecmp(rSwitchType, "Clear Buyback") && tArgs == 1)
    {
        rInventory->ClearBuyback();
    }
    //--Re-sorts all stackable items. Call this after adding an item which can stack.
    else if(!strcasecmp(rSwitchType, "Stack Items") && tArgs == 1)
    {
        rInventory->StackItems();
    }
    //--Sets the internal sorting flag for the inventory.
    else if(!strcasecmp(rSwitchType, "Set Internal Sort") && tArgs == 2)
    {
        rInventory->SetInternalSort(lua_tointeger(L, 2));
    }
    //--Specifies quantity of the last registered item. Use only for saving/loading.
    else if(!strcasecmp(rSwitchType, "Mark Last Item Quantity") && tArgs == 2)
    {
        rInventory->MarkLastItemQuantity(lua_tointeger(L, 2));
    }
    //--Push the item in the given slot, if it exists. Can push NULL.
    else if(!strcasecmp(rSwitchType, "Push Item By Slot") && tArgs == 2)
    {
        StarLinkedList *rItemList = rInventory->GetItemList();
        void *rCheckPtr = rItemList->GetElementBySlot(lua_tointeger(L, 2));
        DataLibrary::Fetch()->PushActiveEntity(rCheckPtr);
    }
    //--Removes the item from the given slot. May destablize the activity stack.
    else if(!strcasecmp(rSwitchType, "Remove Item By Slot") && tArgs == 2)
    {
        StarLinkedList *rItemList = rInventory->GetItemList();
        rItemList->RemoveElementI(lua_tointeger(L, 2));
    }
    //--The last-registered item is pushed to the activity stack.
    else if(!strcasecmp(rSwitchType, "Push Last Item") && tArgs == 1)
    {
        DataLibrary::Fetch()->PushActiveEntity(rInventory->GetLastRegisteredItem());
    }
    ///--[Doctor Bag]
    //--How many charges the doctor bag has.
    else if(!strcasecmp(rSwitchType, "Doctor Bag Charges") && tArgs == 2)
    {
        rInventory->SetDoctorBagCharges(lua_tointeger(L, 2));
    }
    //--How many charges the doctor bag can contain.
    else if(!strcasecmp(rSwitchType, "Doctor Bag Charges Max") && tArgs == 2)
    {
        rInventory->SetDoctorBagMaxCharges(lua_tointeger(L, 2));
    }
    //--Percentage rate gain after battle.
    else if(!strcasecmp(rSwitchType, "Doctor Bag Charge Factor") && tArgs == 2)
    {
        rInventory->SetDoctorBagChargeRate(lua_tonumber(L, 2));
    }
    //--Percentage of HP healed per point of doctor bag.
    else if(!strcasecmp(rSwitchType, "Doctor Bag Potency") && tArgs == 2)
    {
        rInventory->SetDoctorBagPotency(lua_tonumber(L, 2));
    }
    ///--[Catalysts]
    //--Number of each Catalyst the player has.
    else if(!strcasecmp(rSwitchType, "Catalyst Count") && tArgs == 3)
    {
        rInventory->SetCatalystCount(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--How many the player has of those available in the current chapter.
    else if(!strcasecmp(rSwitchType, "Catalyst Local Cur") && tArgs == 3)
    {
        rInventory->SetCatalystLocalCur(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--How many of this type of catalyst are in the chapter total.
    else if(!strcasecmp(rSwitchType, "Catalyst Local Max") && tArgs == 3)
    {
        rInventory->SetCatalystLocalMax(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Uses pre-built lists held by the Map Manager to figure out how many catalysts are available in this chapter.
    else if(!strcasecmp(rSwitchType, "Resolve Catalysts By String") && tArgs == 2)
    {
        rInventory->SetCatalystCountsByGlobalString(lua_tostring(L, 2));
    }
    ///--[Platina]
    //--Add dosh.
    else if(!strcasecmp(rSwitchType, "Add Platina") && tArgs == 2)
    {
        rInventory->SetPlatina(rInventory->GetPlatina() + lua_tointeger(L, 2));
    }
    //--Remove dosh.
    else if(!strcasecmp(rSwitchType, "Remove Platina") && tArgs == 2)
    {
        rInventory->SetPlatina(rInventory->GetPlatina() - lua_tointeger(L, 2));
    }
    //--Removes an item.
    else if(!strcasecmp(rSwitchType, "Remove Item") && (tArgs == 2 || tArgs == 3))
    {
        //--Optional: A third argument allows specifying a removal quantity.
        int tRemoveCount = 1;
        if(tArgs == 3) tRemoveCount = lua_tointeger(L, 3);

        //--If the specified value is 0, do nothing:
        if(tRemoveCount == 0)
        {

        }
        //--If the value is -1, remove all of the entries.
        else if(tRemoveCount < 0)
        {
            while(rInventory->RemoveItem(lua_tostring(L, 2)))
            {
                ;
            }
        }
        //--Otherwise, remove the specific amount requested.
        else
        {
            for(int i = 0; i < tRemoveCount; i ++)
            {
                bool tCheck = rInventory->RemoveItem(lua_tostring(L, 2));
                if(!tCheck) break;
            }
        }
    }
    ///--[Crafting]
    //--Flatly sets the crafting count.
    else if(!strcasecmp(rSwitchType, "Crafting Material") && tArgs == 3)
    {
        rInventory->SetCraftingMaterial(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Adds crafting material.
    else if(!strcasecmp(rSwitchType, "Add Crafting Material") && tArgs == 3)
    {
        int tExisting = rInventory->GetCraftingCount(lua_tointeger(L, 2));
        rInventory->SetCraftingMaterial(lua_tointeger(L, 2), tExisting + lua_tointeger(L, 3));
    }
    //--Remove crafting material.
    else if(!strcasecmp(rSwitchType, "Remove Crafting Material") && tArgs == 3)
    {
        int tExisting = rInventory->GetCraftingCount(lua_tointeger(L, 2));
        rInventory->SetCraftingMaterial(lua_tointeger(L, 2), tExisting - lua_tointeger(L, 3));
    }
    ///--[Gems]
    //--Marks the last registered item as the master gem. Used for loading saves.
    else if(!strcasecmp(rSwitchType, "Mark Last Item As Master Gem") && tArgs == 1)
    {
        rInventory->MarkLastItemAsMasterGem();
    }
    //--Merges the last registered item with the master gem.
    else if(!strcasecmp(rSwitchType, "Merge Last Gem With Master") && tArgs == 1)
    {
        rInventory->MergeLastItemWithMasterGem();
    }
    //--Pushes the activity stack with the master gem. Can push null.
    else if(!strcasecmp(rSwitchType, "Push Master Gem") && tArgs == 1)
    {
        DataLibrary::Fetch()->PushActiveEntity(rInventory->GetMasterGem());
    }
    //--Clears the master gem pointer.
    else if(!strcasecmp(rSwitchType, "Clear Master Gem") && tArgs == 1)
    {
        rInventory->ClearMasterGem();
    }
    //--Marks the last registered item as the item which gems will socket into later.
    else if(!strcasecmp(rSwitchType, "Mark Last Item As Socket Item") && tArgs == 1)
    {
        rInventory->MarkLastItemAsSocketItem();
    }
    //--Sockets the last registered item into the marked socket item.
    else if(!strcasecmp(rSwitchType, "Socket Last Item In Socket Item") && tArgs == 1)
    {
        rInventory->SocketLastItemInSocketItem();
    }
    //--Clears the socketing item.
    else if(!strcasecmp(rSwitchType, "Clear Socket Item") && tArgs == 1)
    {
        rInventory->ClearSocketItem();
    }
    //--Marks the last created item as the one a character will equip next. Used for loading the game.
    else if(!strcasecmp(rSwitchType, "Mark Last Item As Equipment") && tArgs == 1)
    {
        rInventory->MarkLastItemAsEquipItem();
    }
    //--Clears the last item equipment flag.
    else if(!strcasecmp(rSwitchType, "Clear Equipment Marker") && tArgs == 1)
    {
        rInventory->ClearLastEquipItem();
    }
    ///--[Error]
    else
    {
        LuaPropertyError("AdInv_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
int Hook_AdInv_PushItem(lua_State *L)
{
    //AdInv_PushItem(sItemName)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_PushItem");

    //--Set as active.
    rDataLibrary->rActiveObject = AdventureInventory::Fetch()->GetItem(lua_tostring(L, 1));
    return 0;
}
int Hook_AdInv_PushItemI(lua_State *L)
{
    //AdInv_PushItemI(iIndex)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_PushItemI");

    //--Set as active.
    StarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
    void *rItem = rItemList->GetElementBySlot(lua_tointeger(L, 1));
    rDataLibrary->rActiveObject = rItem;
    return 0;
}
