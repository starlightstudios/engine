-- |[ ================================= fnMarkAutorunAsFound() ================================= ]|
--Called by the autorun file if the autorun's executor exists and can run. Because mods/games may
-- not necessarily have their data present, games may be marked to autorun but can't. This makes
-- sure we don't try to boot into a game we can't play.
function fnMarkAutorunAsFound(psName)
    
    -- |[Argument Check]|
    if(psName             == nil) then return end
    if(gzaAutorunHandlers == nil) then return end
    
    -- |[Scan]|
    for i = 1, #gzaAutorunHandlers, 1 do
        if(gzaAutorunHandlers[i][1] == psName) then
            gzaAutorunHandlers[i][3] = true
            return
        end
    end
end