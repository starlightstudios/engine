//--Base
#include "AdventureInventory.h"

//--Classes
#include "AdventureItem.h"

//--CoreClasses
#include "StarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

void AdventureInventory::DisassembleGem(AdventureItem *pPtr)
{
    ///--[Documentation]
    //--Given a gem, removes all of its sub-gems and registers them into the inventory. Also rengisters them
    //  to the gems list to save time.
    if(!pPtr) return;

    //--Iterate.
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        //--Get the subgem.
        AdventureItem *rSubgem = pPtr->RemoveGemFromSlot(i);
        if(!rSubgem) continue;

        //--Register it to the inventory.
        RegisterItem(rSubgem);
        mGemList->AddElementAsTail("Null", rSubgem);
    }

    //--Compute statistics.
    pPtr->ComputeStatistics();

    //--Reset the name and icon to their original values.
    pPtr->SetName(pPtr->GetGemBaseName());
    pPtr->SetIconImageP(pPtr->GetGemBaseImage());
    pPtr->SetDescription(pPtr->GetGemBaseDescription());
    pPtr->ProcessDescription();
}
int AdventureInventory::ComputeGemUpgradePlatinaCost(int pTier)
{
    ///--[Documentation]
    //--Computes and returns the cost, in adamantite, to upgrade a gem at a given tier.
    if(pTier < 0) return 0;
    if(pTier >= 5) return 0;

    //--Cost isn't linear.
    if(pTier == 0) return   50;
    if(pTier == 1) return  150;
    if(pTier == 2) return  500;
    if(pTier == 3) return 2000;
    if(pTier == 4) return 5000;
    return 0;
}
int AdventureInventory::ComputeMonocerosGemUpgradePlatinaCost(int pTier)
{
    ///--[Documentation]
    //--Computes and returns the cost, in adamantite, to upgrade a gem at a given tier. This version is used
    //  by the Monoceros code as the game has a very different cost curve.
    if(pTier < 0) return 0;
    if(pTier >= 5) return 0;

    //--Cost isn't linear.
    if(pTier == 0) return  10;
    if(pTier == 1) return  70;
    if(pTier == 2) return 300;
    if(pTier == 3) return 500;
    if(pTier == 4) return 800;
    return 0;
}
int AdventureInventory::ComputeGemUpgradeCost(int pTier, int pAdamantiteType)
{
    ///--[Documentation]
    //--Computes and returns the cost, in adamantite, to upgrade a gem at a given tier.
    if(pTier < 0) return 0;
    if(pTier >= 5) return 0;

    //--Rank 0 gems:
    if(pTier == 0)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 3;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 1;
        return 0;
    }
    //--Rank 1 gems:
    else if(pTier == 1)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 3;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 1;
        if(pAdamantiteType == CRAFT_ADAMANTITE_SHARD)  return 1;
        return 0;
    }
    //--Rank 2 gems:
    else if(pTier == 2)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 5;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 2;
        if(pAdamantiteType == CRAFT_ADAMANTITE_SHARD)  return 1;
        if(pAdamantiteType == CRAFT_ADAMANTITE_PIECE)  return 1;
        return 0;
    }
    //--Rank 3 gems:
    else if(pTier == 3)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 5;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 1;
        if(pAdamantiteType == CRAFT_ADAMANTITE_SHARD)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_PIECE)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_CHUNK)  return 1;
        return 0;
    }
    //--Rank 4 gems:
    else if(pTier == 4)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 5;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 1;
        if(pAdamantiteType == CRAFT_ADAMANTITE_SHARD)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_PIECE)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_CHUNK)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_ORE)    return 1;
        return 0;
    }

    //--Out of range.
    return 0;
}
