-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "Nowhere"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[ ======================== System ======================== ]|
	--Music.
	AL_SetProperty("Music", "Null")
	
	--Chest path.
	--DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)
	
	--Create the default character.
	TA_Create("Septima")
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Position", 13, 4)
		TA_SetProperty("Facing", gci_Face_South)
		fnSetCharacterGraphics("Root/Images/Sprites/Septima/", true)
	DL_PopActiveObject()
	
	--Order the player to take control of the party leader.
	AL_SetProperty("Player Actor ID", giPartyLeaderID)
	
	--Map Setup
	fnResolveMapLocation("Nowhere")
	

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

    -- |[Clear Field Abilities]|
    AL_SetProperty("Can Edit Field Abilities", false)
    AdvCombat_SetProperty("Set Field Ability", 0, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 1, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 2, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 3, "NULL")
    AdvCombat_SetProperty("Set Field Ability", 4, "NULL")
end
