-- |[ ====================================== Achievements ====================================== ]|
--Wipe any lingering achievements.
if(gsAchievementSet ~= "Runes of Pandemonium") then return end
AM_SetPropertyJournal("Clear Achievements")