//--Base
#include "PairanormalDialogue.h"

//--Classes
#include "PairanormalLevel.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "DisplayManager.h"
#include "OptionsManager.h"

//--[Property Queries]
float PairanormalDialogue::GetRenderDoubleOffset()
{
    //--Returning 0.0f means no double render should occur.
    if(mGlitchTimer >= mGlitchRunTimer || !(mGlitchFlags & DIAGLITCH_LINES)) return 0.0f;

    //--Compute as a percentage and multiply by screen size factors.
    float tPercent = (float)mGlitchTimer / (float)mGlitchRunTimer;
    float tFlip = 1.0f;

    //--Block 8.
    if(tPercent < 0.08f)
    {
        tPercent = 0.08;
        tFlip = -1.0f;
    }
    else if(tPercent < 0.10f)
    {
        tFlip = -1.0f;
    }
    //--Block 19.
    else if(tPercent < 0.19)
    {
        tPercent = 0.19;
    }
    else if(tPercent < 0.22)
    {
    }
    //--Block 31
    else if(tPercent < 0.31)
    {
        tPercent = 0.31;
        tFlip = -1.0f;
    }
    else if(tPercent < 0.34)
    {
        tFlip = -1.0f;
    }
    //--Block 50
    else if(tPercent < 0.50)
    {
        tPercent = 0.50;
    }
    else if(tPercent < 0.52)
    {
    }
    //--Block 65
    else if(tPercent < 0.65)
    {
        tPercent = 0.65;
    }
    else if(tPercent < 0.67)
    {
    }
    //--Block 79
    else if(tPercent < 0.79)
    {
        tPercent = 0.79;
        tFlip = -1.0f;
    }
    else if(tPercent < 0.82)
    {
        tFlip = -1.0f;
    }
    //--Block 93
    else if(tPercent < 0.93)
    {
        tPercent = 0.93;
    }
    else if(tPercent < 0.96)
    {
    }
    //--All others.
    else
    {
        tPercent = 1.0f;
    }

    return mGlitchVHSStart + (mGlitchVHSRange * tPercent * tFlip);
}
SugarBitmap *PairanormalDialogue::GetCreepyOverlay()
{
    //--Can legally return NULL if no overlay is in use.
    if(mGlitchTimer >= mGlitchRunTimer || !(mGlitchFlags & DIAGLITCH_CREEPY)) return NULL;

    //--Range check.
    if(mCreepySlot < 0 || mCreepySlot >= CREEPY_OVERLAYS_TOTAL) return NULL;
    return Images.Data.rCreepyOverlay[mCreepySlot];
}
bool PairanormalDialogue::IsBackgroundInverted()
{
    if(mGlitchTimer >= mGlitchRunTimer || !(mGlitchFlags & DIAGLITCH_BGCOLINV)) return false;
    return true;
}

//--[Core Methods]
void PairanormalDialogue::RollGlitchEffects()
{
    //--[Documentation and Setup]
    //--Rolls what special effects will appear during this glitch. Glitches always have at least one
    //  base effect and then a number of other effects. It is possible for a glitch to have exactly
    //  one effect if none of the sub-effects roll.
    mGlitchFlags = 0x00;

    //--[Base Effects]
    //--At least one base effect will always take place.
    int tBaseRoll = rand() % 100;

    //--Inversion only.
    if(tBaseRoll < 25)
    {
        mGlitchFlags |= DIAGLITCH_COLINV;
    }
    //--Inversion and Scanline.
    else if(tBaseRoll < 75)
    {
        mGlitchFlags |= DIAGLITCH_COLINV;
        mGlitchFlags |= DIAGLITCH_LINES;
    }
    //--Scanline only.
    else
    {
        mGlitchFlags |= DIAGLITCH_LINES;
    }

    //--25% chance to have a creepy overlay. Becomes 100% if scanlines did not roll.
    if(rand() % 100 < 25 || !(mGlitchFlags & DIAGLITCH_LINES))
    {
        mGlitchFlags |= DIAGLITCH_CREEPY;
    }

    //--[Extra Effects]
    //--Background color inversion. 33% chance of taking place.
    if(rand() % 100 < 33)
    {
        mGlitchFlags |= DIAGLITCH_BGCOLINV;
    }
    //--Second roll. Gets an extra roll if the characters had their colors inverted.
    else if(mGlitchFlags & DIAGLITCH_COLINV && rand() % 100 < 33)
    {
        mGlitchFlags |= DIAGLITCH_BGCOLINV;
    }

    //--[Property Rolls]
    //--These always roll and describe how the glitches behave. Not all of the properties are used.
    mGlitchVHSStart = 0.0f;
    mGlitchVHSRange = (VIRTUAL_CANVAS_Y * 0.70f) + (VIRTUAL_CANVAS_Y * GetRandomPercent() * 0.20f);

    //--50% chance of the scanline effect going up instead of down.
    if(rand() % 100 < 50) mGlitchVHSRange = mGlitchVHSRange * -1.0f;

    //--Roll the creepy overlay.
    mCreepySlot = rand() % CREEPY_OVERLAYS_TOTAL;

    //--[Mandatory Stuff]
    //--If the OptionsManager doesn't allow flashing images, the screen shakes instead.
    if(!OptionsManager::Fetch()->GetOptionB("Pairanormal Flashing Images"))
    {
        PairanormalLevel::Fetch()->ShakeScreen();
    }
}
