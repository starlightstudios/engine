-- |[ =============================== fnSetCutsceneDiagnostics() =============================== ]|
--Scans available paths, returns lowest index of a usable path. Returns 0 if no paths match.
-- The path checks for the existince of the launching file, but not the rest of the datafiles.
function fnSetCutsceneDiagnostics(piLevel)
    
    -- |[Argument Check]|
    if(piLevel == nil) then return end
    
    -- |[Set]|
    Cutscene_SetProperty("Diagnostic Level", piLevel)
    gi_Diagnostic_Cutscene_Flag = piLevel
end