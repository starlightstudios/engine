///======================================= MonoUIOptions ===========================================
//--UI Object that encapsulates the Monoceros Options Menu.

#pragma once

///========================================= Includes =============================================
#include "AdvUIOptions.h"
#include "MonoUICommon.h"
#include "MonoUIOptionsStrings.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
//--Gameplay Codes
#define MONOMENU_OPTIONS_GAMEPLAY_TOURIST_MODE 0
#define MONOMENU_OPTIONS_GAMEPLAY_HASTEN 1
#define MONOMENU_OPTIONS_GAMEPLAY_MAXAUTOSAVES 2
#define MONOMENU_OPTIONS_GAMEPLAY_MEMORYCURSOR 3
#define MONOMENU_OPTIONS_GAMEPLAY_ALLOWASSETSTREAMING 4
#define MONOMENU_OPTIONS_GAMEPLAY_USERAMLOADING 5
#define MONOMENU_OPTIONS_GAMEPLAY_UNLOADACTORSAFTERIDALOGUE 6
#define MONOMENU_OPTIONS_GAMEPLAY_USEFADINGUI 7
#define MONOMENU_OPTIONS_GAMEPLAY_RESOLUTION 8
#define MONOMENU_OPTIONS_GAMEPLAY_CENSOR_I 9
#define MONOMENU_OPTIONS_GAMEPLAY_CENSOR_A 10
#define MONOMENU_OPTIONS_GAMEPLAY_CENSOR_Y 11
#define MONOMENU_OPTIONS_GAMEPLAY_CENSOR_E 12
#define MONOMENU_OPTIONS_GAMEPLAY_TOTAL 13

//--Audio Codes
#define MONOMENU_OPTIONS_MUSIC_VOLUME 0
#define MONOMENU_OPTIONS_SOUND_VOLUME 1
#define MONOMENU_OPTIONS_IZANA_VOLUME 2
#define MONOMENU_OPTIONS_CAELYN_VOLUME 3
#define MONOMENU_OPTIONS_CYRANO_VOLUME 4
#define MONOMENU_OPTIONS_SAVEVALUESINSAVEFILE 5
#define MONOMENU_OPTIONS_AUDIO_TOTAL 6

//--Controls Codes
#define MONOMENU_OPTIONS_CONTROLS_ACTIVATE 0
#define MONOMENU_OPTIONS_CONTROLS_CANCEL 1
#define MONOMENU_OPTIONS_CONTROLS_RUN 2
#define MONOMENU_OPTIONS_CONTROLS_UP 3
#define MONOMENU_OPTIONS_CONTROLS_RIGHT 4
#define MONOMENU_OPTIONS_CONTROLS_DOWN 5
#define MONOMENU_OPTIONS_CONTROLS_LEFT 6
#define MONOMENU_OPTIONS_CONTROLS_SHOULDERLEFT 7
#define MONOMENU_OPTIONS_CONTROLS_SHOULDERRIGHT 8
#define MONOMENU_OPTIONS_CONTROLS_ABILITY_1 9
#define MONOMENU_OPTIONS_CONTROLS_ABILITY_2 10
#define MONOMENU_OPTIONS_CONTROLS_ABILITY_3 11
#define MONOMENU_OPTIONS_CONTROLS_ABILITY_4 12
#define MONOMENU_OPTIONS_CONTROLS_ABILITY_5 13
#define MONOMENU_OPTIONS_CONTROLS_EMERGENCY_ESCAPE 14
#define MONOMENU_OPTIONS_CONTROLS_TOTAL 15

//--Debug Control Codes
#define MONOMENU_OPTIONS_DEBUG_OPENMENU 0
#define MONOMENU_OPTIONS_DEBUG_TOGGLEFULLSCREEN 1
#define MONOMENU_OPTIONS_DEBUG_REBUILDKERNING 2
#define MONOMENU_OPTIONS_DEBUG_REBUILDSHADERS 3
#define MONOMENU_OPTIONS_DEBUG_DISABLE_OUTLINE 4
#define MONOMENU_OPTIONS_DEBUG_DISABLE_UNDERWATER 5
#define MONOMENU_OPTIONS_DEBUG_DISABLE_LIGHTING 6
#define MONOMENU_OPTIONS_DEBUG_TOTAL 7

///========================================== Classes =============================================
class MonoUIOptions : public MonoUICommon, virtual public AdvUIOptions
{
    protected:
    ///--[Constants]
    //--Array Sizes
    static const int cxMonoHelpStrings = 6;

    ///--[System]
    bool mEnableMouse;
    int mPreviousMouseX;
    int mPreviousMouseY;
    int mResolutionsTotal;
    char **mResolutionStrings;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFontDoubleHeading;
        StarFont *rFontHeading;
        StarFont *rFontMainline;
        StarFont *rFontControl;
        StarFont *rHelpFont;

        //--Images for this UI
        StarBitmap *rArrowLft;
        StarBitmap *rArrowRgt;
        StarBitmap *rButtonDefaults;
        StarBitmap *rButtonDefaultsSel;
        StarBitmap *rButtonOkay;
        StarBitmap *rButtonOkaySel;
        StarBitmap *rFrameConfirm;
        StarBitmap *rFrameDescription;
        StarBitmap *rFrameHelp;
        StarBitmap *rFrameOptions;
        StarBitmap *rFrameTab;
        StarBitmap *rFrameTabSelected;
        StarBitmap *rOverlaySelection;
        StarBitmap *rOverlayTitleShade;
        StarBitmap *rSliderIndicator;
        StarBitmap *rSliderBack;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUIOptions();
    virtual ~MonoUIOptions();
    virtual void Construct();
    virtual void ConstructTitle();

    //--Public Variables
    static MonoUIOptions_Strings *xrStringData;

    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    virtual void TakeForeground();

    //--Core Methods
    virtual void RefreshMenuHelp();
    virtual void ReresolveOptionStrings();
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);
    virtual void UpdateBackground();
    virtual void HandleActivatePress(int pMode, int pCursor);
    virtual void HandleRunPress(int pMode, int pCursor);
    virtual void HandleLeftPress(int pMode, int pCursor);
    virtual void HandleRightPress(int pMode, int pCursor);

    //--File I/O
    //--Drawing
    virtual void RenderPieces(float pVisAlpha);
    virtual void RenderOptionsGameplay(float pAlpha);
    virtual void RenderOptionsAudio(float pAlpha);
    virtual void RenderOptionsControls(float pAlpha);
    virtual void RenderOptionsDebug(float pAlpha);
    virtual void RenderBoolean(const char *pName, float pY, bool pValue);
    virtual void RenderInteger(const char *pName, float pY, int pValue, int pMaximum);
    virtual void RenderFloat(const char *pName, float pY, float pValue);
    virtual void RenderControl(const char *pName, float pY, const char *pControlName);
    virtual void RenderDetails();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
