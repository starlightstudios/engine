//--Base
#include "TilemapActor.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

///========================================== System ==============================================
///===================================== Property Queries =========================================
///======================================= Manipulators ===========================================
void TilemapActor::SetIdleModeActive(bool pFlag)
{
    //--Modifying the flag always scrambles the timer.
    mHasIdleAnimation = pFlag;
    RandomizeIdleTimer();
}
void TilemapActor::AllocateIdleFrames(int pTotal)
{
    //--Deallocate.
    free(rIdleImages);

    //--Zero.
    mIdleImagesTotal = 0;
    rIdleImages = NULL;

    //--Range check.
    if(pTotal < 1) return;

    //--Allocate, clear.
    SetMemoryData(__FILE__, __LINE__);
    mIdleImagesTotal = pTotal;
    rIdleImages = (StarBitmap **)starmemoryalloc(sizeof(StarBitmap *) * mIdleImagesTotal);
    memset(rIdleImages, 0, sizeof(StarBitmap *) * mIdleImagesTotal);
}
void TilemapActor::SetIdleFrame(int pSlot, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mIdleImagesTotal) return;
    rIdleImages[pSlot] = (StarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void TilemapActor::SetIdleTPF(int pValue)
{
    if(pValue < 1) return;
    mIdleTPF = pValue;
}
void TilemapActor::SetIdleLoopFrame(int pFrame)
{
    mIdleLoopsToFrame = pFrame;
}
void TilemapActor::RandomizeIdleTimer()
{
    //--Randomizes when the next idle animation will begin. Used to make sure all party members
    //  don't play theirs at the same time.
    mIdleTimerCountdown = (rand() % 600) + 300;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void TilemapActor::IdleUpdate()
{
    ///--[Documentation]
    //--Handles updates if the actor has idle animations enabled. This will override the active
    //  frame if the conditions are met.
    if(!mHasIdleAnimation) return;

    //--Decrement this timer. If it reaches zero, we start idle animations. This timer gets randomized
    //  whenever the character moves.
    if(mIdleTimerCountdown > 0) mIdleTimerCountdown --;
    if(mIdleTimerCountdown > 0) return;

    //--If the timer is exactly zero, initialize the animation.
    if(mIdleTimerCountdown == 0)
    {
        //--Set to -1 so this only happens once.
        mIdleTimerCountdown = -1;

        //--Set the timer to -, so it increment to 0.
        mIdleAnimTimer = -1;
    }

    //--Increment the timer.
    mIdleAnimTimer ++;

    //--Check if we've run over the last possible frame. If so, handle the "ending" case.
    if(mIdleAnimTimer >= mIdleImagesTotal * mIdleTPF)
    {
        //--If we have a loop-to frame, set the timer to the start of that frame.
        if(mIdleLoopsToFrame > -1)
        {
            mIdleAnimTimer = mIdleLoopsToFrame * mIdleTPF;
        }
        //--No loop-to frame, so re-randomize the idle timer.
        else
        {
            mIdleAnimTimer = -1;
            RandomizeIdleTimer();
        }
    }
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
///======================================= Pointer Routing ========================================
///===================================== Static Functions =========================================
///========================================= Lua Hooking ==========================================
///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
