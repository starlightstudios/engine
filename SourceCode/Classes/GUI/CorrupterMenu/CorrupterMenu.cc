//--Base
#include "CorrupterMenu.h"

//--Classes
#include "VisualLevel.h"

//--CoreClasses
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

///========================================== System ==============================================
CorrupterMenu::CorrupterMenu()
{
    ///--[RootObject]
    //--System
    mType = POINTER_TYPE_CORRUPTERMENU;

    ///--[CorrupterMenu]
    //--System
    mIsVisible = false;
    mCurrentMode = CM_MODE_MINIONS;

    //--(Status Mode)
    //--Mode
    mIsUnlockingPerk = false;
    memset(mPerkUnlockLines, 0, sizeof(char) * CM_PERK_UNLOCK_LINES * CM_PERK_UNLOCK_CHARS);

    //--Position Constants
    cPerkStartX = 0.0f;
    cPerkUnlockStartX = 0.0f;
    cPerkStartY = 0.0f;
    cPerkBtnWid = 0.0f;
    cPerkBtnHei = 0.0f;

    //--Button Highlighting
    mPerkHighlightedBtn = -1;

    //--(Minions Mode)
    //--Column Position Constants
    cXPosIcon = CM_HEADER_INDENT;
    cXPosName = cXPosIcon + 32.0f;
    cXPosHP = cXPosName + 160.0f;
    cXPosWP = cXPosHP + 128.0f;
    cXPosLocation = cXPosWP + 128.0f;
    cXPosOrders = cXPosLocation + 256.0f;

    //--Standing Orders Button
    mIsOrdersButtonHighlighted = false;
    mOrdersBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);

    //--Storage
    mMinionActiveSlot = -1;
    mMonsterListing = new StarLinkedList(true);

    //--(Summons Mode)
    //--Selection
    mHighlightedSummonIndex = -1;

    //--Script
    mSummonExecutionScript = NULL;

    //--Confirmation
    mShowConfirmDialog = false;
    mIsDialogNotEnoughMP = false;

    //--(Magic Mode)
    //--Selection
    mMustRefreshMagicLegality = false;
    mHighlightedMagicIndex = -1;

    //--Positioning
    cMagicListingLft = 0.0f;
    cMagicListingWid = 0.0f;
    cMagicListingTop = 0.0f;
    cDescriptionListingLft = 0.0f;
    cDescriptionListingWid = 0.0f;
    cDescriptionListingTop = 0.0f;

    //--Casting Button
    mMagicCastBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);

    //--(Transform Mode)
    //--Selection
    mHighlightedTransformIndex = -1;

    //--Positioning
    mTransformListingDim.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mTransformPortraitDim.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mTransformDescriptionDim.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mTransformActivateBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);

    //--(Unrolled Magic Menu)
    //--Selection
    mHighlightedUnrolledSpell = -1;
    mLastUnrolledSize = 0;

    //--(Universal)
    //--Header
    mHighlightedHeaderBtn = 0;
    mHeaderStatusBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mHeaderMinionsBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mHeaderSummonBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mHeaderMagicBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mHeaderTransformBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mHeaderCloseBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);

    //--Confirmation Dialog
    mConfirmBody.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mConfirmYesBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
    mConfirmCancelBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);

    //--Colors
    mNormalTextCol.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    mUnavailableCol.SetRGBAF(0.6f, 0.0f, 0.0f, 1.0f);
    mHighlightCol.SetRGBAF(0.50f, 0.05f, 0.40f, 1.0f);

    //--Images
    memset(&Images, 0, sizeof(Images));

    ///--[Construction]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Images.Data.rBorderCard  = (StarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/3DUI/CardBorder");
    Images.Data.rUIFont      = rDataLibrary->GetFont("Corrupter Menu Main");
    Images.Data.rUIFontSmall = rDataLibrary->GetFont("Corrupter Menu Small");
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(StarBitmap *));

    //--[Header Buttons]
    //--Set their positions. They may reposition if widescreen is used.
    if(!Images.mIsReady) return;

    //--Sizes.
    float cHdrIndent = CM_HEADER_INDENT;
    float cBtnIndent = CM_BTN_INDENT;
    float cTxtHeight = CM_BTN_TEXT_SIZE;
    float cBtnHeight = cTxtHeight + (cBtnIndent * 2.0f);

    //--Header.
    mHeaderStatusBtn.SetWH(cHdrIndent, cHdrIndent, Images.Data.rUIFont->GetTextWidth("Status") + (cBtnIndent * 2.0f), cBtnHeight);
    mHeaderMinionsBtn.SetWH(mHeaderStatusBtn.mRgt + cBtnIndent, cHdrIndent, Images.Data.rUIFont->GetTextWidth("Minions") + (cBtnIndent * 2.0f), cBtnHeight);
    mHeaderSummonBtn.SetWH(mHeaderMinionsBtn.mRgt + cBtnIndent, cHdrIndent, Images.Data.rUIFont->GetTextWidth("Summon") + (cBtnIndent * 2.0f), cBtnHeight);
    mHeaderMagicBtn.SetWH(mHeaderSummonBtn.mRgt + cBtnIndent, cHdrIndent, Images.Data.rUIFont->GetTextWidth("Magic") + (cBtnIndent * 2.0f), cBtnHeight);
    mHeaderTransformBtn.SetWH(mHeaderMagicBtn.mRgt + cBtnIndent, cHdrIndent, Images.Data.rUIFont->GetTextWidth("Transform") + (cBtnIndent * 2.0f), cBtnHeight);
    mHeaderCloseBtn.SetWH(VIRTUAL_CANVAS_X - Images.Data.rUIFont->GetTextWidth("Close") - cBtnIndent * 2.0f - cHdrIndent, cHdrIndent, Images.Data.rUIFont->GetTextWidth("Close") + (cBtnIndent * 2.0f), cBtnHeight);

    //--Magic Mode.
    cMagicListingLft = CM_HEADER_INDENT;
    cMagicListingWid = 240.0f;
    cMagicListingTop = mHeaderStatusBtn.mBot + CM_BTN_INDENT;
    cDescriptionListingLft = cMagicListingLft + cMagicListingWid + 32.0f;
    cDescriptionListingWid = VIRTUAL_CANVAS_X - cDescriptionListingLft - CM_HEADER_INDENT;
    cDescriptionListingTop = mHeaderStatusBtn.mBot + CM_BTN_INDENT;

    //--Minions Mode.
    mOrdersBtn.SetWH(CM_HEADER_INDENT, VIRTUAL_CANVAS_Y - CM_HEADER_INDENT - cBtnHeight, Images.Data.rUIFont->GetTextWidth("Current Standing Orders: Destroy") + (cBtnIndent * 2.0f), cBtnHeight);

    //--Confirmation Dialog.
    float cConfirmWid = VIRTUAL_CANVAS_X * 0.65f;
    float cConfirmHei = VIRTUAL_CANVAS_Y * 0.50f;
    mConfirmBody.SetWH((VIRTUAL_CANVAS_X - cConfirmWid) * 0.5f, (VIRTUAL_CANVAS_Y - cConfirmHei) * 0.5f, cConfirmWid, cConfirmHei);

    //--Buttons.
    float cTexWidYes = Images.Data.rUIFont->GetTextWidth("Confirm") + (cBtnIndent * 2.0f);
    float cTexWidNo  = Images.Data.rUIFont->GetTextWidth("Cancel")  + (cBtnIndent * 2.0f);
    mConfirmYesBtn.SetWH(mConfirmBody.mLft + CM_HEADER_INDENT, mConfirmBody.mBot - CM_HEADER_INDENT - cBtnHeight, cTexWidYes, cBtnHeight);
    mConfirmCancelBtn.SetWH(mConfirmBody.mRgt - CM_HEADER_INDENT - (cTexWidNo * 1.0f), mConfirmBody.mBot - CM_HEADER_INDENT - cBtnHeight, cTexWidNo, cBtnHeight);

    //--Status mode.
    cPerkStartX = mHeaderSummonBtn.mRgt + 240.0f;
    cPerkUnlockStartX = cPerkStartX;
    cPerkStartY = mHeaderStatusBtn.mBot + CM_BTN_INDENT;
    cPerkBtnWid = 280.0f;
    cPerkBtnHei = CM_BTN_TEXT_SIZE + (cBtnIndent * 2.0f);

    //--Transformation menu. The height values of these structures represent the size of each text line.
    mTransformListingDim.SetWH(CM_HEADER_INDENT, mHeaderStatusBtn.mBot + CM_BTN_INDENT, 240.0f, cTxtHeight);

    //--Position of the description is bottom-aligned.
    float cDescriptionTop = VIRTUAL_CANVAS_Y - (CM_HEADER_INDENT * 2.0f) - (CM_BTN_TEXT_SIZE * 5.0f) - (cBtnHeight);
    mTransformDescriptionDim.Set(mTransformListingDim.mRgt + 32.0f, cDescriptionTop, 1.0f, cDescriptionTop + cTxtHeight);

    //--Portrait for transformation ends right above the description.
    mTransformPortraitDim.Set(mTransformListingDim.mRgt + 32.0f, mHeaderStatusBtn.mBot + CM_BTN_INDENT, VIRTUAL_CANVAS_X - CM_HEADER_INDENT, cDescriptionTop - 8.0f);
}
CorrupterMenu::~CorrupterMenu()
{
    delete mMonsterListing;
    free(mSummonExecutionScript);
}

//--[Public Statics]
//--Stores what the standing orders are. Can be reset from the Lua code.
char *CorrupterMenu::xStandingOrders = InitializeString("Follow");

//--Stores a list of everything the player can summon, as SummonPacks.
StarLinkedList *CorrupterMenu::xSummonPackList = new StarLinkedList(true);

//--How much MP the player has.
int CorrupterMenu::xPlayerMP = 0;

///===================================== Property Queries =========================================
bool CorrupterMenu::IsVisible()
{
    return mIsVisible;
}

///======================================= Manipulators ===========================================
void CorrupterMenu::SetVisibility(bool pIsVisible)
{
    if(!mIsVisible && pIsVisible)
    {
        mMustRefreshMagicLegality = true;
    }
    mIsVisible = pIsVisible;
}

///======================================= Core Methods ===========================================
///=================================== Private Core Methods =======================================
///========================================== Update ==============================================
void CorrupterMenu::Update()
{
    //--Handle the header, which is always present on the menus. It only allows switching between
    //  modes, and can block the update from continuing.
    if(UpdateHeader()) return;

    //--Switching function, passes the update to whatever mode is active.
    if(mCurrentMode == CM_MODE_STATUS)
    {
        UpdateStatusMode();
    }
    //--Minions mode.
    else if(mCurrentMode == CM_MODE_MINIONS)
    {
        UpdateMinionsMode();
    }
    //--Summon mode.
    else if(mCurrentMode == CM_MODE_SUMMON)
    {
        UpdateSummonMode();
    }
    //--Magic mode.
    else if(mCurrentMode == CM_MODE_MAGIC)
    {
        UpdateMagicMode();
    }
    //--Transform mode.
    else if(mCurrentMode == CM_MODE_TRANSFORM)
    {
        UpdateTransformMode();
    }
}
bool CorrupterMenu::UpdateHeader()
{
    //--Standardized update for the header. Returns true if it handled mouse input, false otherwise.
    ControlManager *rControlManager = ControlManager::Fetch();
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Status Button.
    if(IsPointWithin2DReal(tMouseX, tMouseY, mHeaderStatusBtn))
    {
        //--SFX.
        if(mHighlightedHeaderBtn != CM_MODE_STATUS)
        {
            mHighlightedHeaderBtn = CM_MODE_STATUS;
            //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Left-click.
        if(rControlManager->IsFirstPress("MouseLft") && !IsStatusMode())
        {
            //AudioManager::Fetch()->PlaySound("Menu|Select");
            SetToStatusMode();
            return true;
        }
    }

    //--Minions Button.
    if(IsPointWithin2DReal(tMouseX, tMouseY, mHeaderMinionsBtn))
    {
        //--SFX.
        if(mHighlightedHeaderBtn != CM_MODE_MINIONS)
        {
            mHighlightedHeaderBtn = CM_MODE_MINIONS;
            //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Left-click.
        if(rControlManager->IsFirstPress("MouseLft") && !IsMinionsMode())
        {
            //AudioManager::Fetch()->PlaySound("Menu|Select");
            SetToMinionsMode();
            return true;
        }
    }

    //--Summon button.
    if(IsPointWithin2DReal(tMouseX, tMouseY, mHeaderSummonBtn))
    {
        //--SFX.
        if(mHighlightedHeaderBtn != CM_MODE_SUMMON)
        {
            mHighlightedHeaderBtn = CM_MODE_SUMMON;
            //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Left-click.
        if(rControlManager->IsFirstPress("MouseLft") && !IsSummonMode())
        {
            //AudioManager::Fetch()->PlaySound("Menu|Select");
            SetToSummonMode();
            return true;
        }
    }

    //--Magic button.
    if(IsPointWithin2DReal(tMouseX, tMouseY, mHeaderMagicBtn))
    {
        //--SFX.
        if(mHighlightedHeaderBtn != CM_MODE_MAGIC)
        {
            mHighlightedHeaderBtn = CM_MODE_MAGIC;
            //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Left-click.
        if(rControlManager->IsFirstPress("MouseLft") && !IsMagicMode())
        {
            //AudioManager::Fetch()->PlaySound("Menu|Select");
            SetToMagicMode();
            return true;
        }
    }

    //--Transform button.
    if(IsPointWithin2DReal(tMouseX, tMouseY, mHeaderTransformBtn))
    {
        //--SFX.
        if(mHighlightedHeaderBtn != CM_MODE_TRANSFORM)
        {
            mHighlightedHeaderBtn = CM_MODE_TRANSFORM;
            //AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Left-click.
        if(rControlManager->IsFirstPress("MouseLft") && !IsTransformMode())
        {
            //AudioManager::Fetch()->PlaySound("Menu|Select");
            SetToTransformMode();
            return true;
        }
    }

    //--Close button. Hides this menu.
    if(IsPointWithin2DReal(tMouseX, tMouseY, mHeaderCloseBtn))
    {
        //--SFX.
        if(mHighlightedHeaderBtn != -1)
        {
            mHighlightedHeaderBtn = -1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Left-click.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //AudioManager::Fetch()->PlaySound("Menu|Select");
            SetVisibility(false);
            return true;
        }
    }

    //--All checks failed.
    return false;
}

///========================================= File I/O =============================================
///========================================== Drawing =============================================
void CorrupterMenu::Render()
{
    //--Render a border across the whole screen.
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, 0.0f, 0.0f, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y, 0x01FF);

    //--Header.
    RenderHeader();

    //--Render status mode.
    if(mCurrentMode == CM_MODE_STATUS)
    {
        RenderStatusMode();
    }
    //--Render the contents of Minions mode.
    else if(mCurrentMode == CM_MODE_MINIONS)
    {
        RenderMinionsMode();
    }
    //--Summon mode.
    else if(mCurrentMode == CM_MODE_SUMMON)
    {
        RenderSummonMode();
    }
    //--Magic mode.
    else if(mCurrentMode == CM_MODE_MAGIC)
    {
        RenderMagicMode();
    }
    //--Transform mode.
    else if(mCurrentMode == CM_MODE_TRANSFORM)
    {
        RenderTransformMode();
    }
}
void CorrupterMenu::RenderHeader()
{
    //--Renders the header at the top-left of the window, which has buttons allowing the switching of modes.
    //  The positions of the header buttons are built during class setup.
    if(!Images.mIsReady) return;

    //--Sizes.
    float cBtnIndent = CM_BTN_INDENT;

    //--Colors.
    static const StarlightColor cxSelected = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    static const StarlightColor cxUnselected = StarlightColor::MapRGBAF(0.5f, 0.5f, 0.5f, 1.0f);

    //--Render the currently selected icon first as white.
    memcpy(&VisualLevel::xTextColor, &cxSelected, sizeof(StarlightColor));
    if(mCurrentMode == CM_MODE_STATUS)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderStatusBtn, cBtnIndent, "Status");
    }
    else if(mCurrentMode == CM_MODE_MINIONS)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderMinionsBtn, cBtnIndent, "Minions");
    }
    else if(mCurrentMode == CM_MODE_SUMMON)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderSummonBtn, cBtnIndent, "Summon");
    }
    else if(mCurrentMode == CM_MODE_MAGIC)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderMagicBtn, cBtnIndent, "Magic");
    }
    else if(mCurrentMode == CM_MODE_TRANSFORM)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderTransformBtn, cBtnIndent, "Transform");
    }

    //--All other buttons render as grey.
    memcpy(&VisualLevel::xTextColor, &cxUnselected, sizeof(StarlightColor));
    if(mCurrentMode != CM_MODE_STATUS)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderStatusBtn, cBtnIndent, "Status");
    }
    if(mCurrentMode != CM_MODE_MINIONS)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderMinionsBtn, cBtnIndent, "Minions");
    }
    if(mCurrentMode != CM_MODE_SUMMON)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderSummonBtn, cBtnIndent, "Summon");
    }
    if(mCurrentMode != CM_MODE_MAGIC)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderMagicBtn, cBtnIndent, "Magic");
    }
    if(mCurrentMode != CM_MODE_TRANSFORM)
    {
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderTransformBtn, cBtnIndent, "Transform");
    }

    //--MP render.
    Images.Data.rUIFont->DrawTextArgs(mHeaderTransformBtn.mRgt + CM_BTN_INDENT, mHeaderTransformBtn.mTop + CM_BTN_INDENT, 0, 1.0f, "Your MP: %i", xPlayerMP);

    //--Close button. Always renders white.
    memcpy(&VisualLevel::xTextColor, &cxSelected, sizeof(StarlightColor));
    VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mHeaderCloseBtn, cBtnIndent, "Close");
}

///====================================== Pointer Routing =========================================
///===================================== Static Functions =========================================
CorrupterMenu *CorrupterMenu::Fetch()
{
    PandemoniumLevel *rLevel = PandemoniumLevel::Fetch();
    if(!rLevel) return NULL;
    return rLevel->GetCorrupterMenu();
}

///======================================== Lua Hooking ===========================================
void CorrupterMenu::HookToLuaState(lua_State *pLuaState)
{
    /* CM_RegisterSummonPack("CLEAR")
       CM_RegisterSummonPack(sName, iMPCost, iPrototypeSlot, sIconPath, sDescription1, ..., sDescription5)
       Creates a new SummonPack, used in CorrupterMode to summon minions at the cost of MP. The description
       lines are optional, none need to be passed in.
       Pass "CLEAR" with no other arguments to clear the summon pack list. */
    lua_register(pLuaState, "CM_RegisterSummonPack", &Hook_CM_RegisterSummonPack);

    /* CM_GetProperty("MP") (1 integer)
       CM_GetProperty("Standing Orders") (1 string)
       Returns the requested property from the CorrupterMenu. A PandemoniumLevel of any type must exist
       or this call will fail and return 0. */
    lua_register(pLuaState, "CM_GetProperty", &Hook_CM_GetProperty);

    /* CM_SetProperty("MP", iValue)
       CM_SetProperty("Summon Script", sPath)
       Sets the requested property in the CorrupterMenu. A PandemoniumLevel of any type must exist
       or this call will fail. */
    lua_register(pLuaState, "CM_SetProperty", &Hook_CM_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_CM_RegisterSummonPack(lua_State *L)
{
    //CM_RegisterSummonPack("CLEAR")
    //CM_RegisterSummonPack(sName, iHP, iMP, iMPCost, iPrototypeSlot, sIconPath, sRenderPath, sDescription1, ..., sDescription5)
    int tArgs = lua_gettop(L);

    //--Single-argument case.
    if(tArgs == 1)
    {
        CorrupterMenu::xSummonPackList->ClearList();
        return 0;
    }

    //--Otherwise, check for the normal case. At least 4 args are required, more can be passed in.
    if(tArgs < 7) return LuaArgError("CM_RegisterSummonPack");

    //--Create.
    SummonPack *nPack = SummonPack::Create();
    ResetString(nPack->mSummonName, lua_tostring(L, 1));
    nPack->mHP = lua_tointeger(L, 2);
    nPack->mWP = lua_tointeger(L, 3);
    nPack->mManaCost = lua_tointeger(L, 4);
    nPack->mPrototypeSlot = lua_tointeger(L, 5);
    nPack->rIconImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(lua_tostring(L, 6));
    nPack->rRenderImg = (StarBitmap *)DataLibrary::Fetch()->GetEntry(lua_tostring(L, 7));

    //--Add the description lines, if present.
    for(int i = 8; i < tArgs + 1; i ++)
    {
        nPack->SetLine(i-8, lua_tostring(L, i));
    }

    //--Register.
    CorrupterMenu::xSummonPackList->AddElement(lua_tostring(L, 1), nPack, SummonPack::DeleteThis);
    return 0;
}
int Hook_CM_GetProperty(lua_State *L)
{
    //CM_GetProperty("MP") (1 integer)
    //CM_GetProperty("Standing Orders") (1 string)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CM_GetProperty");

    //--Switch type.
    const char *rSwitchType = lua_tostring(L, 1);

    //--MP Value. Can be set statically.
    if(!strcasecmp(rSwitchType, "MP"))
    {
        lua_pushinteger(L, CorrupterMenu::xPlayerMP);
        return 1;
    }
    //--Current standing orders.
    if(!strcasecmp(rSwitchType, "Standing Orders"))
    {
        lua_pushstring(L, CorrupterMenu::xStandingOrders);
        return 1;
    }
    //--Unknown type.
    else
    {
        return LuaPropertyError("CM_GetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_CM_SetProperty(lua_State *L)
{
    //CM_SetProperty("MP", iValue)
    //CM_SetProperty("Summon Script", sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("CM_SetProperty");

    //--Check that the menu exists.
    CorrupterMenu *rCorrupterMenu = CorrupterMenu::Fetch();
    if(!rCorrupterMenu) return LuaTypeError("CM_SetProperty", L);

    //--Switch type.
    const char *rSwitchType = lua_tostring(L, 1);

    //--MP Value. Can be set statically.
    if(!strcasecmp(rSwitchType, "MP") && tArgs == 2)
    {
        CorrupterMenu::xPlayerMP = lua_tointeger(L, 2);
    }
    //--Which script to use when summoning someone.
    else if(!strcasecmp(rSwitchType, "Summon Script") && tArgs == 2)
    {
        rCorrupterMenu->SetSummonExecScript(lua_tostring(L, 2));
    }
    //--Unknown type.
    else
    {
        return LuaPropertyError("CM_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
