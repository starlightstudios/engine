///======================================== MonoUIQuit ============================================
//--Quit UI for Monoceros. Just changes rendering properties.

#pragma once

///========================================= Includes =============================================
#include "AdvUIQuit.h"
#include "MonoUICommon.h"

///===================================== Local Structures =========================================
///===================================== Local Definitions ========================================
///======================================== Translation ===========================================
///--[MonoUIBase_Strings]
//--Contains translatable strings, built at startup. Only one static copy needs to exist.
#include "TranslationManager.h"
#define MONOUIQUIT_STRINGS_TOTAL 3
class MonoUIQuit_Strings : public TranslationModule
{
    //--Methods
    public:
    union
    {
        struct
        {
            char *mPtr[MONOUIQUIT_STRINGS_TOTAL];
        }mem;
        struct
        {
            char *mTextQuitToTitle;
            char *mTextCancel;
            char *mTextToTitle;
        }str;
    };

    //--Functions
    MonoUIQuit_Strings();
    virtual ~MonoUIQuit_Strings();
    virtual int GetSize();
    virtual void SetString(int pSlot, const char *pString);
};

///========================================== Classes =============================================
class MonoUIQuit : public MonoUICommon, virtual public AdvUIQuit
{
    protected:
    ///--[System]
    //--Menu Values
    static const int cxMonoMenuVal_Cancel  = 0;
    static const int cxMonoMenuVal_ToTitle = 1;

    ///--[Images]
    struct
    {
        //--Fonts
        StarFont *rFont_DoubleHeading;
        StarFont *rFont_Heading;
        StarFont *rFont_Mainline;

        //--Images
        StarBitmap *rButton_Cancel;
        StarBitmap *rButton_CancelSel;
        StarBitmap *rButton_ToTitle;
        StarBitmap *rButton_ToTitleSel;
        StarBitmap *rFrame_Main;
    }MonoImages;

    protected:

    public:
    //--System
    MonoUIQuit();
    virtual ~MonoUIQuit();
    virtual void Construct();

    //--Public Variables
    static MonoUIQuit_Strings *xrStringData;

    //--Property Queries
    //--Manipulators
    //--Core Methods
    virtual void RecomputeCursorPositions();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual bool UpdateForeground(bool pCannotHandleUpdate);

    //--File I/O
    //--Drawing
    void RenderPieces(float pVisAlpha);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    //static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions


