//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "CorrupterMenu.h"

//--CoreClasses
#include "StarBitmap.h"
#include "StarFont.h"
#include "StarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers

//--Manipulators
void PandemoniumLevel::AppendToConsole(const char *pString)
{
    //--Append to console with the default text size.
    AppendToConsole(pString, cFontSize, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
}
void PandemoniumLevel::AppendToConsole(const char *pString, float pTextSize, StarlightColor pColor)
{
    //--Appends the given string to the end of the console. Note that the 'end' is actually
    //  the 'head' of the console since iterators are faster from the front. The console is
    //  therefore backwards in memory. Sue me.
    //--A copy of the string is appended. If the original was heap-allocated, the caller is
    //  still responsible for it.
    //--Font size should be in pixels.
    if(!pString || !Images.mIsReady) return;

    //--If the text size was zero, set it to the default.
    if(pTextSize < Images.Data.rConsoleFont->GetTextHeight()) pTextSize = Images.Data.rConsoleFont->GetTextHeight();

    //--Create the string pack.
    SetMemoryData(__FILE__, __LINE__);
    ConsoleString *nStringPack = (ConsoleString *)starmemoryalloc(sizeof(ConsoleString));
    nStringPack->Initialize();
    nStringPack->mHasBreak = xNextLineHasBreak;
    nStringPack->mTextSize = pTextSize;
    memcpy(&nStringPack->mColor, &pColor, sizeof(StarlightColor));

    //--If the color is black, switch it to white. Bitmap fonts have automatic outlines.
    if(pColor.r == 0.0f && pColor.g == 0.0f && pColor.b == 0.0f)
    {
        nStringPack->mColor.SetRGBAF(0.8f, 0.8f, 0.8f, nStringPack->mColor.a);
    }

    //--Get the length of the string. If necessary, cut the string up and paste it multiple times.
    //  The font must exist for this test, otherwise the system console is emulating and we don't
    //  need to run this subroutine.
    float cFontScale = 1.0f;
    //const float cBufferLen = 15.0f;
    float tLength = 950.0f;

    //--Get the length of the string.
    float tStringLen = Images.Data.rConsoleFont->GetTextWidth(pString) * cFontScale;
    if(tStringLen > tLength)
    {
        //--Setup.
        int tCursor = 0;
        int tRunningCursor = 0;

        //--Move characters into a new buffer until the string length is hit.
        char tBuffer[STD_MAX_LETTERS];
        while(tCursor < (STD_MAX_LETTERS-1) && tRunningCursor < (int)strlen(pString))
        {
            //--Append the letter.
            tBuffer[tCursor+0] = pString[tRunningCursor];
            tBuffer[tCursor+1] = '\0';

            //--Check the length.
            float tNewLength = Images.Data.rConsoleFont->GetTextWidth(tBuffer) * cFontScale;

            //--If it's too long, we're done.
            if(tNewLength > tLength) break;

            //--Otherwise, go up one.
            tCursor ++;
            tRunningCursor ++;
        }

        //--Now scan backwards until we hit a space. This is the cutoff point.
        int tCutoffLetter = -1;
        for(int i = (int)strlen(tBuffer) - 1; i >= 0; i --)
        {
            if(tBuffer[i] == ' ')
            {
                tCutoffLetter = i+1;
                break;
            }
        }

        //--If the cutoff letter is -1, then no spaces were found. Just hard-break on the letter.
        if(tCutoffLetter == -1)
        {
            //--Flags.
            xNextLineHasBreak = false;

            //--Add the buffer.
            nStringPack->mString = InitializeString("%s", tBuffer);
            mConsoleStrings->AddElementAsHead("X", nStringPack, &ConsoleString::DeleteThis);

            //--Append the rest of the string as its own string.
            int tStartLetter = (int)strlen(nStringPack->mString);
            AppendToConsole(&pString[tStartLetter], pTextSize, pColor);
        }
        //--Otherwise, cutoff on the space.
        else
        {
            //--Flags.
            xNextLineHasBreak = false;

            //--Cut the buffer.
            tBuffer[tCutoffLetter] = '\0';

            //--Add the buffer.
            nStringPack->mString = InitializeString("%s", tBuffer);
            mConsoleStrings->AddElementAsHead("X", nStringPack, &ConsoleString::DeleteThis);

            //--Append the rest of the string as its own string.
            int tStartLetter = (int)strlen(nStringPack->mString);
            AppendToConsole(&pString[tStartLetter], pTextSize, pColor);
        }
    }
    //--If the string wasn't wider than the console, just append it.
    else
    {
        //--Flags.
        xNextLineHasBreak = false;

        //--Append it.
        nStringPack->mString = InitializeString("%s", pString);
        mConsoleStrings->AddElementAsHead("X", nStringPack, &ConsoleString::DeleteThis);
    }
}
void PandemoniumLevel::AppendToConsoleStatic(const char *pString)
{
    //--Static version of AppendToConsole, fetches the level for you. If no level is found, fails
    //  quietly and safely.
    PandemoniumLevel *rLevel = PandemoniumLevel::Fetch();
    if(rLevel) rLevel->AppendToConsole(pString);
}
void PandemoniumLevel::AppendToConsoleStatic(const char *pString, float pTextSize, StarlightColor pColor)
{
    //--Static version of AppendToConsole, fetches the level for you. If no level is found, fails
    //  quietly and safely.
    PandemoniumLevel *rLevel = PandemoniumLevel::Fetch();
    if(rLevel) rLevel->AppendToConsole(pString, pTextSize, pColor);
}

//--Rendering
void PandemoniumLevel::RenderConsolePane()
{
    //--Console pane in the bottom left. Shows the console, which is a text readout of what's
    //  going on. You can't type into it.
    if(!Images.mIsReady) return;

    //--Position setup.
    glTranslatef(ConsolePane.cLft, ConsolePane.cTop, 0.0f);

    //--Fast-access. Keeps things aligned.
    float cWid = ConsolePane.cWid;
    float cHei = ConsolePane.cHei;
    float cInd = ConsolePane.cIndent;

    //--[Border]
    //--Subroutine handles it.
    RenderBorder(0.0f, 0.0f, cWid, cHei, cInd, cBorderColor, cInnerColor);

    //--Render as much of the console as we can get away with. The most recent entry is at the
    //  bottom of the console.
    float cFontScale = 1.0f;
    float tStepRate = (Images.Data.rConsoleFont->GetTextHeight()+2.0f) * -1.3f * cFontScale;
    float tCursor = ConsolePane.cHei - ConsolePane.cIndent + tStepRate;

    //--Console renders black, but the color can be overridden.
    glColor3f(0.0f, 0.0f, 0.0f);

    //--If this is present, then print "Press any key to continue".
    if(mIsConsoleWaitingOnKeypress)
    {
        glColor3f(1.0f, 0.0f, 0.0f);
        Images.Data.rConsoleFont->DrawText(ConsolePane.cIndent, tCursor, 0, cFontScale, "Press any key to continue.");
        glColor3f(0.0f, 0.0f, 0.0f);
        tCursor = tCursor + tStepRate;
    }

    //--Loop through strings. Stop if no string is found, or if we go over the edge.
    int i = 0;
    ConsoleString *rString = (ConsoleString *)mConsoleStrings->PushIterator();
    while(rString)
    {
        //--If a string consists of exactly one space, it's a padding line. Render it at half-size, and
        //  don't render it at all if this is the 0th line.
        if(rString->mString[0] == ' ' && rString->mString[1] == '\0')
        {
            if(i > 0) tCursor = tCursor + (tStepRate * 0.25f);
            i ++;
            rString = (ConsoleString *)mConsoleStrings->AutoIterate();
            continue;
        }

        //--Render.
        rString->mColor.SetAsMixer();
        Images.Data.rConsoleFont->DrawText(ConsolePane.cIndent, tCursor, 0, cFontScale, rString->mString);

        //--Ending case check.
        tCursor = tCursor - (rString->mTextSize + 2.0f);
        if(tCursor < ConsolePane.cIndent - (tStepRate * 0.7f))
        {
            mConsoleStrings->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rString = (ConsoleString *)mConsoleStrings->AutoIterate();
    }

    //--Final clean up.
    glTranslatef(-ConsolePane.cLft, -ConsolePane.cTop, 0.0f);
    glColor3f(1.0f, 1.0f, 1.0f);

    //--Render the unrolled magic menu.
    if(mMagicMenuPopulationScript)
    {
        mCorrupterMenu->RenderUnrolledMenu(mUnrolledMenuDim);
    }
}
