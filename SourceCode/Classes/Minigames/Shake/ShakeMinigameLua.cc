//--Base
#include "ShakeMinigame.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

///======================================== Lua Hooking ===========================================
void ShakeMinigame::HookToLuaState(lua_State *pLuaState)
{
    /* ShakeMinigame_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "ShakeMinigame_GetProperty", &Hook_ShakeMinigame_GetProperty);

    /* ShakeMinigame_SetProperty("Set Grabbing Enemy", psName)
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "ShakeMinigame_SetProperty", &Hook_ShakeMinigame_SetProperty);
}

///================================================================================================
///                                      Hooking Functions                                       ==
///================================================================================================
int Hook_ShakeMinigame_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //ShakeMinigame_GetProperty("Dummy") (1 Integer)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("ShakeMinigame_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    ShakeMinigame *rMinigame = ShakeMinigame::Fetch();
    if(!rMinigame) return LuaTypeError("ShakeMinigame_GetProperty", L);

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("ShakeMinigame_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_ShakeMinigame_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //ShakeMinigame_SetProperty("Set Grabbing Enemy", psName)

    ///--[Arguments]
    //--Must have at least one argument to be valid.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("ShakeMinigame_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    ShakeMinigame *rMinigame = ShakeMinigame::Fetch();
    if(!rMinigame) return LuaTypeError("ShakeMinigame_SetProperty", L);

    ///--[System]
    //--Enemy that is grabbing the player.
    if(!strcasecmp(rSwitchType, "Set Grabbing Enemy") && tArgs == 2)
    {
        rMinigame->SetGrabbingActorS(lua_tostring(L, 2));
    }
    //--Error case.
    else
    {
        LuaPropertyError("ShakeMinigame_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
