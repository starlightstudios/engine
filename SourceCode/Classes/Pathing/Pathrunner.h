///======================================== Pathrunner ============================================
//--Path-running algorithm in a class package. Can run all at once or partially, and nominally supports
//  threading as all of its variables are located internally once set up.

#pragma once

///========================================== Includes ============================================
#include "Definitions.h"
#include "Structures.h"

///===================================== Local Structures =========================================
typedef struct PathrunnerPack
{
    int mPulseVal;
    float mDistToLine;
    float mDistToTarget;
    int mX;
    int mY;
    int mPrevDir;
}PathrunnerPack;

///===================================== Local Definitions ========================================
#define ADVP_UNUSED 0x7FFF
#define ADVP_WALK_DIST 8
#define ADVP_NORMAL_ITERATIONS 200

///========================================== Classes =============================================
class Pathrunner
{
    private:
    protected:
    ///--[System]
    bool mFailure;
    bool mHasAnyPath;
    bool mIsInitialized;
    int mWalkDist;
    int mIterationsToRun;

    ///--[Collision Storage]
    int mXSize;
    int mYSize;
    int16_t **mPathArray;

    ///--[Flags]
    int mStartX;
    int mStartY;
    int mEndX;
    int mEndY;
    float mShortestDistance;
    float mOriginalDistance;

    ///--[Path Runner]
    bool mAllowGridAligning;
    int mPulseTotal;
    int mCollisionDepth;
    TiledLevel *rActiveLevel;
    StarLinkedList *mPathRunnerList; //PathrunnerPack *, master

    public:
    //--System
    Pathrunner();
    virtual ~Pathrunner();

    //--Public Variables
    static StarLinkedList *xStaticPathList; //PathrunnerPack *, master

    //--Property Queries
    bool IsInitialized();
    bool IsFailed();
    bool HasPulsesRemaining();
    int GetPulsesRemaining();

    //--Manipulators
    void SetCollisionDepth(int pDepth);
    void SetIterationsPerCycle(int pIterations);
    void SetWalkDistance(int pDistance);

    //--Core Methods
    virtual bool IsMoveShortest(int pX, int pY, int pPulseVal);
    virtual bool IsMoveValid(int pX, int pY, int pPulseVal, int pDirection);
    static void InsertPackageOnList(PathrunnerPack *pPackage, StarLinkedList *pList, float pStartDistance);

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual StarLinkedList *RunPathingIntoList();
    virtual void Initialize(int pStartX, int pStartY, int pEndX, int pEndY);
    virtual void RunIterator(int pPulseCap, bool pStopOnReachingTarget);
    virtual StarLinkedList *ResolvePath();

    //--File I/O
    void WritePathToDisk(const char *pPath);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Path_GetPathLength(lua_State *L);
int Hook_Path_GetPathDirection(lua_State *L);


